# Luminosity and beamline monitoring using VPRetinaCluster counters

  > **The repository is still in building.**
  

In this repository we provide the code for the luminosity and beamline monitoring using VPRetinaCluster counters.

  

Clusters counters are defined in the following regions:

  

![Alt text](./counters.png?raw=true)

  

Where the inner and outer counters store the following type of counters:

-  <span  style="color:red;">**Inner counters**</span> : average cluster per event, divided per BxType;

-  **Outer counters** : average cluster per event, average fraction of empty events (log0 method) divided per bxType; fraction of empty events per each bunch-crossing (per-bxid-lumi).

 
Repository is organized in this way:

```
project
│   README.md
│
└───beamline
│   └───MC_data	
│   └───src
│   └───plots
│   
└───lumi
│   └───lumi_calibration
|   │   └───scr
│   └───lumi_estimate
│   └───lumi_per_bxid
```
In the beamline folder we have all the code necessary to do the beamline analysis
In the lumi folder we have three subfolders:
- one is relative to the calibration in a VdM scan
- one stores the code for an estimate of the luminosity during a Physics Run
- The luminosity per bxid provide information about the luminosity per bunch crossing
  
## Beamline
Contains Monte Carlo simulation and code used to generate calibration files for the pca. Also contains roba to test the pca on real data. 
## Lumi
### Lumi calibration
Contains data and python scripts used for the VdM scan. The code is under building and will be mantained for future VdM scans. Further details can be found inside the folder. Actually work only for September '23 data due to different formattings. Plan to fix it in the future.
### Lumi estimate
Contains python script for the estimated of luminosity in a physics run given a calibration file produced using the scripts in the ```lumi calibration``` foler. For now it is tested with pp-reference run of september 2023 with the VdM calibration of september 2023.
### Lumi per bxid
Daniele ha fatto cose (che voi umani nemmeno...)

## Dataset description

### Vdm_10_11_2022
- 23% of counters missing
- bxtype counters available only for outer counters
- background still low
### Vdm_07_09_2023
- 5% of counters missing
- bxtype available for outer and inner counters
  
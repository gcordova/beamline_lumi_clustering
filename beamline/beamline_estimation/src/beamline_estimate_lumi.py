import os
from os.path import exists
import sys
from functools import reduce
from pathlib import Path
import numpy as np
import pandas as pd
from pandas.errors import EmptyDataError
import matplotlib.pyplot as plt
import yaml
import seaborn as sns
import colorcet as cc
from scipy.stats import trim_mean
import datetime as dt


# Function to transform original data (x) to pattern space (p)
def data_to_pca(x, config_dictionary):
    """
    Transform original data x to pattern space p using PCA.

    Args:
        x (numpy.ndarray): Input data.
        config_dictionary (dict): Dictionary containing configuration parameters.

    Returns:
        float: Transformed value in pattern space.
    """
    # Extract configuration parameters
    eigen_vectors = config_dictionary["gEigenVectors"]
    n_variables = config_dictionary["gNVariables"]
    mean_values = config_dictionary["gMeanValues"]
    sigma_values = config_dictionary["gSigmaValues"]
    p = 0
    for j in range(n_variables):
        p += (x[j])* eigen_vectors[j * n_variables]#-mean_values[j]) * eigen_vectors[j * n_variables] / sigma_values[j]
    return p

def transform_data(df_x_counters, config_dictionary):
    """
    Transform data using Principal Component Analysis (PCA).

    Args:
        df_x_counters (pd.DataFrame): Dataframe with counters data.
        config_dictionary (dict): Dictionary containing configuration parameters.

    Returns:
        pd.Series: Transformed data.
    """
    coef = config_dictionary["calib_coeff"]

    p_c = []
    for i in range(len(df_x_counters.index)):
        x = df_x_counters.iloc[i].to_numpy()
        a = data_to_pca(x, config_dictionary)
        p_c.append(a)

    pos = p_c  # + np.full(len(df_x_counters.index), 0.1 / coef)
    return pos

def main():
    # Load configuration
    stream = open("../config/config.yaml", 'r', encoding="utf-8")
    config = yaml.safe_load(stream) 
    stream = open("../config/pca_x_veloC_all.yaml", 'r', encoding="utf-8")
    config_pca_xC = yaml.safe_load(stream)

    stream = open("../config/pca_y_veloC_all.yaml", 'r', encoding="utf-8")
    config_pca_yC = yaml.safe_load(stream)

    stream = open("../config/pca_x_veloA_all.yaml", 'r', encoding="utf-8")
    config_pca_xA = yaml.safe_load(stream)

    stream = open("../config/pca_y_veloA_all.yaml", 'r', encoding="utf-8")
    config_pca_yA = yaml.safe_load(stream)

    stream = open("../config/pca_z_veloA_all.yaml", 'r', encoding="utf-8")
    config_pca_zA = yaml.safe_load(stream)

    stream = open("../config/pca_z_veloC_all.yaml", 'r', encoding="utf-8")
    config_pca_zC = yaml.safe_load(stream)

    stream = open("../config/pca_x_config.yaml", 'r', encoding="utf-8")
    config_pca_x = yaml.safe_load(stream)

    stream = open("../config/pca_y_config.yaml", 'r', encoding="utf-8")
    config_pca_y = yaml.safe_load(stream)

    # Load the data
    df = pd.read_csv("../../../data_vdm_2024/input_data/data_merged.csv") #df = pd.read_csv("../../../data_vdm_2022/output_data/df_counters_wo_offset.csv")
    df["TS"] = pd.to_datetime(df["TS"], format='%Y-%m-%d %H:%M:%S')
    df = df.set_index("TS")
    df_lumi = pd.read_csv('../../../data_vdm_2024/output_data/lumi.csv') #lumi9476.csv
    df_sigma_vis = pd.read_csv('../../../data_vdm_2024/output_data/sigma_vis.csv')
    #replace the "_outer" with "_A" and "_inner" with "_B" in the column colName
    df_sigma_vis["colName"] = df_sigma_vis["colName"].str.replace('_outer', '_A').str.replace('_inner', '_B')
    df_sigma_vis.set_index("colName", inplace=True)
    
    df_lumi["TS"] = pd.to_datetime(df_lumi["TS"], format='%Y-%m-%d %H:%M:%S')
    df_lumi = df_lumi.set_index("TS")
    df_lumi = df_lumi*1e3
    df_lumi_outer = df_lumi.filter(regex='_outer')
    df_lumi_inner = df_lumi.filter(regex='_inner')
    counters_col_all = [col for col in df.columns if col.startswith("M")] 
    counters_colC = config["veloC_all"]
    counters_colA = config["veloA_all"]
    all_counters = config_pca_x["columns"]
    #aggiungi il prefisso "lumi_" a tutti gli elementi di all_counters
    all_counters = ["lumi_" + s for s in all_counters]
    #df_counters_all = df[counters_col_all]
    counters_colA = [col for col in counters_colA]
    df_lumi_all = df_lumi.rename(columns=lambda x: x.replace('_outer', '_A').replace('_inner', '_B'))
    col_a = [col for col in df_lumi_all.columns if col.endswith("_A")]
    col_b = [col for col in df_lumi_all.columns if col.endswith("_B")]
    coll_bunches = 22
    lhc_freq = 0.0112455
    
    # calculate trimmed mean of lumi
    trim = df_lumi.apply(lambda x: trim_mean(x, 0.15), axis=1)
    upper_interval_outer = trim + 0.5*trim
    lower_interval_outer = trim - 0.5*trim
    upper_interval_inner = trim + 0.5*trim
    lower_interval_inner = trim - 0.5*trim
    df_counters_all = pd.DataFrame(index=df_lumi.index)

    for col in all_counters:
        if col.endswith("_A"):
            if col not in col_a:
                df_lumi_all[col] = trim
                df_counters_all[col.replace("lumi_", "")] = df_lumi_all[col]*df_sigma_vis.loc[col.replace("lumi_", "")]["calibration"]/(coll_bunches*lhc_freq)
                print("Added column: ", col)
            else:
                df_lumi_all[col] = np.where((df_lumi_all[col] > upper_interval_outer) | (df_lumi_all[col] < lower_interval_outer), trim, df_lumi_all[col])
                df_counters_all[col.replace("lumi_", "")] = df_lumi_all[col]*df_sigma_vis.loc[col.replace("lumi_", "")]["calibration"]/(coll_bunches*lhc_freq)
        elif col.endswith("_B"):
            if col not in col_b:
                df_lumi_all[col] = trim
                df_counters_all[col.replace("lumi_", "")] = df_lumi_all[col]*df_sigma_vis.loc[col.replace("lumi_", "")]["calibration"]/(coll_bunches*lhc_freq)
                print("Added column: ", col)
            else:
                df_lumi_all[col] = np.where((df_lumi_all[col] > upper_interval_inner) | (df_lumi_all[col] < lower_interval_inner), trim, df_lumi_all[col])
                df_counters_all[col.replace("lumi_", "")] = df_lumi_all[col]*df_sigma_vis.loc[col.replace("lumi_", "")]["calibration"]/(coll_bunches*lhc_freq)

    df_lumi_all = df_lumi_all.reindex(sorted(df_lumi_all.columns), axis=1)
    df_counters_all = df_counters_all.rename(columns=lambda x: x.replace('_A', '_outer').replace('_B', '_inner'))
    norm = df_counters_all.mean(axis=1)
    norm_C = df_counters_all[counters_colC].mean(axis=1)
    norm_A = df_counters_all[counters_colA].mean(axis=1)
    
    #keep only the entries in df_counters_all with norm > threshold
    df_counters_all = df_counters_all[norm > 5]
    norm = norm[norm>5]
    norm_C = norm_C[norm_C>5]
    norm_A = norm_A[norm_A>5]

    df_countersC = df_counters_all[counters_colC]
    df_countersA = df_counters_all[counters_colA]
    
    df_position = pd.DataFrame(index=df_counters_all.index)
    df_counters_all["1_comp_x"] = transform_data(df_counters_all, config_pca_x)
    df_counters_all["1_comp_y"] = transform_data(df_counters_all, config_pca_y)
    df_countersC["1_comp_x"] = transform_data(df_countersC, config_pca_xC)
    df_countersC["1_comp_y"] = transform_data(df_countersC, config_pca_yC)
    df_countersA["1_comp_x"] = transform_data(df_countersA, config_pca_xA)
    df_countersA["1_comp_y"] = transform_data(df_countersA, config_pca_yA)
    df_countersA["1_comp_z"] = transform_data(df_countersA, config_pca_zA)
    df_countersC["1_comp_z"] = transform_data(df_countersC, config_pca_zC)
    z1=-(df_countersC["1_comp_z"]/norm)
    z2=-(df_countersA["1_comp_z"]/norm)
    df_position["x_estimate"] = (df_counters_all["1_comp_x"]/norm)/1.06022+0.36561/1.06022#/1.20491+0.23720/1.20491#/0.00583+0.00115/0.00583#/1.21069+0.23825/1.21069#/0.00349+0.00083/0.00349#/0.03147+0.008927/0.03147#/0.01051+0.002731/0.01051#/0.01084+0.002818/0.01084#+0.2182)*0.9662-0.0144#/0.02954+ 0.003657/0.02954 #/(0.2759)+0.03415/0.2759#/df_lumi['lumi'] * 8.*0.0112450/(63.4*0.2759)+0.03415/0.2759
    df_position["y_estimate"] = -(df_counters_all["1_comp_y"]/norm)/1.03352+0.06567/1.03352#/1.09919+0.02553/1.09919#/0.00532+0.00012/0.00532#/1.10336+0.02564/1.10336#/0.00314-0.00003/0.00314#/0.02985+0.0001929/0.02985#/0.009755+0.0004956#/0.01006+0.0005095/0.01006#*34.73+0.01834+0.004832#/0.02786  +0.0001357/0.02786 #/(0.2592)+0.001214/0.2759#/df_lumi['lumi']
    df_position["xVeloC"] = -(df_countersC["1_comp_x"]/norm_C)/0.76857+11.05906/0.76857#/0.88906+11.04425/0.88906#/0.00413+0.05334/0.00413#/0.99212+11.15395/0.99212#/0.00241+0.05205/0.00241#/0.02301+0.318/0.02301#/0.007341+0.1023/0.007341#/0.008613+0.106/0.008613#*46.62+13.33#*45.83+13.11#*4.321+11.38
    df_position["yVeloC"] = (df_countersC["1_comp_y"]/norm_C)/0.79435+0.11227/0.79435#0.86935+0.10603/0.86935#/0.00421+0.00051/0.00421#/0.87235+0.10638/0.87235#/0.00292+0.00025/0.00292#/0.02293+0.002895/0.02293#/0.007368+0.0008725/0.007368#/0.007609+0.0008996/0.007609#*43.91+0.1226#*43.91+0.1228#*8.631-0.1176
    df_position["xVeloA"] = (df_countersA["1_comp_x"]/norm_A)/0.74602-10.45299/0.74602#/0.81536-10.58366/0.81536#/0.00414-0.05132/0.00414#/0.71712-10.57377/0.71712#/0.00254-0.05057/0.00254#/0.02169-0.3026/0.02169#/0.007176-0.09767/0.007176#/0.006864-0.1003/0.006864#*43.13-11.91#*47.01-12.89#*4.283-10.83
    df_position["yVeloA"] = -(df_countersA["1_comp_y"]/norm_A)/0.66325+0.00070/0.66325#0.68642-0.03456/0.68642#/0.00332-0.00017/0.00332#/0.68934-0.03475/0.68934#/0.00159-0.00004/0.00159#/0.01911+0.0003813/0.01911#/0.006114+0.00009598/0.006114#/0.0063+0.0000999/0.0063#*55.46-0.05156#*55.41-0.04321#*10.38-0.1218
    df_position["zVeloA"] = 7525*z2+956.5 #7779*z2+983.3#-4.977e07*z2**3-1.904e07*z2**2-2.43e06*z2-1.021e05
    df_position["zVeloC"] = 5872*z1+704.1 #5983*z1+715.6  #-2.0471e07*(z1**3) -7.439e06*(z1**2) -8.963e05*z1- 3.551e04#7260*z1+919.8#-4.015e06*z1**3-1.62e06*z1**2-2.09e06*z1-8688
    df_position["1_comp_x"] = df_counters_all["1_comp_x"]/norm
    df_position["1_comp_y"] = df_counters_all["1_comp_y"]/norm
    df_position["1_comp_Ax"] = df_countersA["1_comp_x"]/norm_A
    df_position["1_comp_Ay"] = df_countersA["1_comp_y"]/norm_A
    df_position["1_comp_Cx"] = df_countersC["1_comp_x"]/norm_C
    df_position["1_comp_Cy"] = df_countersC["1_comp_y"]/norm_C
    df_position["1_comp_Az"] = df_countersA["1_comp_z"]/norm_A
    df_position["1_comp_Cz"] = df_countersC["1_comp_z"]/norm_A
    df_position["norm"] = norm

    #plot the x estimated position
    plt.figure(figsize=(20, 10))
    df_position["x_estimate"].plot()
    plt.title("x estimated position")
    plt.xlabel("Time")
    plt.ylabel("Position")
    plt.grid()
    #plt.show()
    


    df_position.to_csv("../../beamline_calibration/calibration_dataset/vdm_2024_lumi_sub_calib_bis.csv")


if __name__ == "__main__":
    main()
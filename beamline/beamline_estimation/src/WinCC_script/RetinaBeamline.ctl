// $License: NOLICENSE
//--------------------------------------------------------------------------------
/**
  @file $relPath
  @copyright $copyright
  @author velo_user
*/

//--------------------------------------------------------------------------------
// Libraries used (#uses)
#uses "fwFSM/fwFsm.ctl"

//--------------------------------------------------------------------------------
// Variables and Constants
int BADREAD = 12237598; //value to indicate that the read is bad
float coef = 0.5; //coefficient to calculate the range of the outliers
int poolPeriod = 3; //seconds to pool the data

//--------------------------------------------------------------------------------
/**
*/

main(mapping event)
{
  startThread( "BeamPositionEstimator" );

}

void BeamPositionEstimator()

{

  time t; //time variable: current time
  dyn_float value_counters_inner; //array of values of the inner counters
  dyn_float value_counters_outer; //array of values of the outer counters
  dyn_bool mask_inner; //array of flags to indicate if the value is valid for the inner counters
  dyn_bool mask_outer; //array of flags to indicate if the value is valid for the outer counters

    //dps names for positions
  dyn_string x_pca_dps_name = makeDynString("VEECS:VPRetina.Beamline.PosX");
  dyn_string y_pca_dps_name = makeDynString("VEECS:VPRetina.Beamline.PosY");
  dyn_string z_pca_dps_name = makeDynString("VEECS:VPRetina.Beamline.PosZ");
  dyn_string xA_pca_dps_name = makeDynString("VEECS:VPRetina.Beamline.VeloAPosX");
  dyn_string yA_pca_dps_name = makeDynString("VEECS:VPRetina.Beamline.VeloAPosY");
  dyn_string zA_pca_dps_name = makeDynString("VEECS:VPRetina.Beamline.VeloAPosZ");
  dyn_string xC_pca_dps_name = makeDynString("VEECS:VPRetina.Beamline.VeloCPosX");
  dyn_string yC_pca_dps_name = makeDynString("VEECS:VPRetina.Beamline.VeloCPosY");
  dyn_string zC_pca_dps_name = makeDynString("VEECS:VPRetina.Beamline.VeloCPosZ");
  //VELO states
  string state_A, state_C;

  string module = "";
  while(true){
    dpSetWait("VEECS:VPRetina.Luminosity.Infos.ControlManagers.VEECS.RetinaBeamline", true);
    t = getCurrentTime();
    //get states of VELO A and C
    fwCU_getState("VELOA", state_A);
    fwCU_getState("VELOC", state_C);
    // if VELO is running and it is time to pool the data
    if( second(t)%poolPeriod==0 && milliSecond(t)>500 && (state_A=="RUNNING" && state_C=="RUNNING") ){ //
        //loop over all counters
      for(int i = 0; i < 52; i++){ //52 modules
        if(i<10) module = "0" + (string)(i); //add 0 to the left of the number
        else module = (string)(i); //module number

        for( int j = 0; j <= 1 ; j++){ 
          //float tmp_value ;
          dyn_string regs_counts_avg_outer, regs_counts_avg_inner ; // names of dps
          dyn_float tmp_values_outer ;  // values of outer dps
          dyn_float tmp_values_inner ; // values of inner dps
          float tmp_value_outer_to_PCA ; // value of outer counter to pass to PCA
          float tmp_value_inner_to_PCA ;    // value of inner counter to pass to PCA
          bool tmp_mask_outer_to_PCA = false; // flag to check if the value is valid
          bool tmp_mask_inner_to_PCA = false; // flag to check if the value is valid

            //arrays of names: get the dps names for the counters of the current module  and bxtype
          dynAppend(regs_counts_avg_outer, dpNames("V*DAQTELL40:Velo_TELL40_M" + module + "_*_" + (string)(j) + ".luminosity_rates.lumi_bb_avg_rate") );
          dynAppend(regs_counts_avg_outer, dpNames("V*DAQTELL40:Velo_TELL40_M" + module + "_*_" + (string)(j) + ".luminosity_rates.lumi_be_avg_rate") );
          dynAppend(regs_counts_avg_outer, dpNames("V*DAQTELL40:Velo_TELL40_M" + module + "_*_" + (string)(j) + ".luminosity_rates.lumi_eb_avg_rate") );
          dynAppend(regs_counts_avg_outer, dpNames("V*DAQTELL40:Velo_TELL40_M" + module + "_*_" + (string)(j) + ".luminosity_rates.lumi_ee_avg_rate") );

          dynAppend(regs_counts_avg_inner, dpNames("V*DAQTELL40:Velo_TELL40_M" + module + "_*_" + (string)(j) + ".luminosity_rates.lumi_bb_avg_inner_rate") );
          dynAppend(regs_counts_avg_inner, dpNames("V*DAQTELL40:Velo_TELL40_M" + module + "_*_" + (string)(j) + ".luminosity_rates.lumi_be_avg_inner_rate") );
          dynAppend(regs_counts_avg_inner, dpNames("V*DAQTELL40:Velo_TELL40_M" + module + "_*_" + (string)(j) + ".luminosity_rates.lumi_eb_avg_inner_rate") );
          dynAppend(regs_counts_avg_inner, dpNames("V*DAQTELL40:Velo_TELL40_M" + module + "_*_" + (string)(j) + ".luminosity_rates.lumi_ee_avg_inner_rate") );

        //get the values of the counters given the array of names above
          dpGet( regs_counts_avg_outer, tmp_values_outer );
          dpGet( regs_counts_avg_inner, tmp_values_inner );

//            subtract backgrounds and set valid flags

            //check that values per bxtype are less than 1 and that the sum of the values is greater than 0
            if ( tmp_values_outer[1] < 1  && tmp_values_outer[2] < 1 && tmp_values_outer[3] < 1 && tmp_values_outer[4] < 1 && (tmp_values_outer[1] - tmp_values_outer[2] - tmp_values_outer[3] + tmp_values_outer[4]) > 0  ){
                // bb - be - eb + ee
                tmp_value_outer_to_PCA = (tmp_values_outer[1] - tmp_values_outer[2] - tmp_values_outer[3] + tmp_values_outer[4]);
                tmp_mask_outer_to_PCA = true;
            }
            else{
              tmp_value_outer_to_PCA = BADREAD*1. ;
            }

            if ( tmp_values_inner[1] < 1  && tmp_values_inner[2] < 1 && tmp_values_inner[3] < 1 && tmp_values_inner[4] < 1 && (tmp_values_inner[1] - tmp_values_inner[2] - tmp_values_inner[3] + tmp_values_inner[4]) > 0  ){
                tmp_value_inner_to_PCA = (tmp_values_inner[1] - tmp_values_inner[2] - tmp_values_inner[3] + tmp_values_inner[4]);
                tmp_mask_inner_to_PCA = true;
            }
            else{
              tmp_value_inner_to_PCA = BADREAD*1. ;
            }
          //append the values to the arrays
          dynAppend(value_counters_outer, tmp_value_outer_to_PCA);
          dynAppend(value_counters_inner, tmp_value_inner_to_PCA);
          dynAppend(mask_outer, tmp_mask_outer_to_PCA);
          dynAppend(mask_inner, tmp_mask_inner_to_PCA);
        }
      }
    // substitute the outliers with the median of the valid values
    value_counters_inner = SubstituteOutliers(value_counters_inner, mask_inner,coef);
    value_counters_outer = SubstituteOutliers(value_counters_outer, mask_outer,coef);
    // calculate the normalisation factor
    float value_norm = CalculateNormalisationFactor(value_counters_outer, value_counters_inner);
      
      //calculate scalar product of the PCA weights and the values of the counters
      float x_pca = ApplyPcaComponent(value_counters_inner, value_counters_outer, mask_inner, mask_outer, pca_weight_x);
      float y_pca = ApplyPcaComponent(value_counters_inner, value_counters_outer, mask_inner, mask_outer, pca_weight_y);
      float z_pca = ApplyPcaComponent(value_counters_inner, value_counters_outer, mask_inner, mask_outer, pca_weight_z);

        //calibrate the values
      float x_calib = 999.; if(value_norm >0.001 && x_pca !=999.) x_calib = x_pca/(value_norm*1.09177) + 0.30965/1.09177;
      float y_calib = 999.; if(value_norm >0.001 && y_pca !=999. ) y_calib = -y_pca/(value_norm*1.03565) + 0.06694/1.03565;
      //float z_calib = 0.; if(value_mu) z_calib = -4.544e4/5.5 * pow(z_pca, 3.) + 1.245e5/5.5 * pow(z_pca,2) -1.144e5/5.5*z_pca + 3.526e4;

        //split the values of the counters into inner and outer counters for VELO A and C
      dyn_float value_counters_inner_A, value_counters_outer_A, value_counters_inner_C, value_counters_outer_C;
      dyn_float mask_inner_A, mask_outer_A, mask_inner_C, mask_outer_C;

      //Reminder: the order of the counters is: Outer-Down,   Inner-Down,   Outer-Left,   Inner-Left,   Outer-Upper,   Inner-Upper,   Outer-Right,   Inner-Right
      //                                        O00_0,        I00_0,        O00_1,        I00_1,        O01_0,         I01_0,         O01_1,         I01_1
      // VELO C are Down and Left, VELO A are Upper and Right
      // first 2 counters are VELO C, next 2 are VELO A, next 2 are VELO C, next 2 are VELO A, etc.
      for(int i=0; i<104; i++){
        if(i%4 < 2){ //VELO A
          dynAppend(value_counters_inner_C, value_counters_inner[i+1] );
          dynAppend(value_counters_outer_C, value_counters_outer[i+1] );
          dynAppend(mask_inner_C, mask_inner[i+1] );
          dynAppend(mask_outer_C, mask_outer[i+1] );
        }
        else{ //VELO C
          dynAppend(value_counters_inner_A, value_counters_inner[i+1] );
          dynAppend(value_counters_outer_A, value_counters_outer[i+1] );
          dynAppend(mask_inner_A, mask_inner[i+1] );
          dynAppend(mask_outer_A, mask_outer[i+1] );
        }
      }

      // VELO_A
      float x_A_pca = ApplyPcaComponent(value_counters_inner_A, value_counters_outer_A, mask_inner_A, mask_outer_A, pca_weight_A_x);
      float y_A_pca = ApplyPcaComponent(value_counters_inner_A, value_counters_outer_A, mask_inner_A, mask_outer_A, pca_weight_A_y);
      float z_A_pca = ApplyPcaComponent(value_counters_inner_A, value_counters_outer_A, mask_inner_A, mask_outer_A, pca_weight_A_z);

      float x_A_calib = 999.; if(value_norm>0.001 && x_A_pca !=999.) x_A_calib = x_A_pca/(value_norm*0.76674) -10.50362/0.76674; //x_A_pca*44.2870/value_mu -11.6342; //x_A_pca*43.13/value_mu -11.91
      float y_A_calib = 999.; if(value_norm>0.001 && y_A_pca !=999.) y_A_calib = -y_A_pca/(value_norm*0.66291) +0.01324/0.66291; //-y_A_pca*43.9753/value_mu +0.1272; //-y_A_pca*55.46/value_mu -0.05156;
      float z_A_calib = 999.; if(value_norm>0.001 && z_A_pca !=999.) z_A_calib = -z_A_pca*7525.0/value_norm + 956.5; //-23008090*pow(z_A_pca/value_mu, 3.) + 8408082*pow(z_A_pca/value_mu,2.) - 1030313*(z_A_pca/value_mu) + 42358 ; //-z_A_pca*7525.0/value_mu + 956.5;

      //VELO_C
      float x_C_pca = ApplyPcaComponent(value_counters_inner_C, value_counters_outer_C, mask_inner_C, mask_outer_C, pca_weight_C_x);
      float y_C_pca = ApplyPcaComponent(value_counters_inner_C, value_counters_outer_C, mask_inner_C, mask_outer_C, pca_weight_C_y);
      float z_C_pca = ApplyPcaComponent(value_counters_inner_C, value_counters_outer_C, mask_inner_C, mask_outer_C, pca_weight_C_z);

      float x_C_calib = 999.; if(value_norm>0.001 && x_C_pca !=999.) x_C_calib = -x_C_pca/(value_norm*0.78941) + 11.03146/0.78941; //-x_C_pca*43.0478/value_mu + 11.9876; //-x_C_pca*46.62/value_mu + 13.33;
      float y_C_calib = 999.; if(value_norm>0.001 && y_C_pca !=999.) y_C_calib = y_C_pca/(value_norm*0.79598) + 0.10051/0.79598; //y_C_pca*42.7716/value_mu - 0.1550; //y_C_pca*43.91/value_mu + 0.1226;
      float z_C_calib = 999.; if(value_norm>0.001 && z_C_pca !=999.) z_C_calib = -z_C_pca*5872.0/value_norm + 704.1; //-16950459*pow(z_C_pca/value_mu, 3.) + 5736989*pow(z_C_pca/value_mu,2.) - 652426*(z_C_pca/value_mu) + 24954; //-z_C_pca*5872.0/value_mu + 704.1;

     //write values to the dps only if scalar product is not 0
        if(x_calib < 900) dpSetWait(x_pca_dps_name, makeDynFloat(x_calib));
        if(y_calib < 900) dpSetWait(y_pca_dps_name, makeDynFloat(y_calib));
        if(z_A_calib <900 && z_C_calib <900 )dpSetWait(z_pca_dps_name, makeDynFloat(z_A_calib*0.5 + z_C_calib*0.5));

        if(x_A_calib < 900) dpSetWait(xA_pca_dps_name, makeDynFloat(x_A_calib));
        if(y_A_calib < 900) dpSetWait(yA_pca_dps_name, makeDynFloat(y_A_calib));
        if(z_A_calib < 900) dpSetWait(zA_pca_dps_name, makeDynFloat(z_A_calib));

        if(x_C_calib < 900) dpSetWait(xC_pca_dps_name, makeDynFloat(x_C_calib));
        if(y_C_calib < 900) dpSetWait(yC_pca_dps_name, makeDynFloat(y_C_calib));
        if(z_C_calib < 900) dpSetWait(zC_pca_dps_name, makeDynFloat(z_C_calib));
    
      dynClear(value_counters_inner);
      dynClear(mask_inner);
      dynClear(value_counters_outer);
      dynClear(mask_outer);
      dynClear(value_counters_inner_A);
      dynClear(mask_inner_A);
      dynClear(value_counters_outer_A);
      dynClear(mask_outer_A);
      dynClear(value_counters_inner_C);
      dynClear(mask_inner_C);
      dynClear(value_counters_outer_C);
      dynClear(mask_outer_C);
      delay(1);
    }
  }

}


/**
  @brief Substitutes the outliers in the values with the median of the valid values
    @param values: array of values
    @param mask: array of flags to indicate if the value is valid
    @param coeff: coefficient to calculate the range of the outliers
    @return array of values with the outliers substituted
*/
dyn_float SubstituteOutliers (dyn_float values, dyn_bool mask, float coeff=0.5)
{
  dyn_float values_to_median; //values to calculate the median
  float median; //median value
  dyn_float values_substituted; //output of the function
  for(int i=1; i<=dyn_len(values); i++){
    if(mask[i]) dynAppend(values_to_median, values[i]); //append only the values that are valid
  }
  dynSort(values_to_median); //sort the values
  //if the number of values is even, the median is the value shifted on the right tail
  if( dynlen(values_to_median) > 2 && dynlen(values_to_median)%2==0) median = values_to_median[(dynlen(values_to_median)+1)/2 ];
  //if the number of values is odd, the median is the middle value
  else if( dynlen(values_to_median) > 2 && dynlen(values_to_median)%2!=0) median = values_to_median[dynlen(values_to_median)/2 ];
  //if there are less than 3 values, the median is 0 and return original values
  else 
    return values;
  //loop over all values and substitute the outliers with the median
  for(int i=1; i<=dyn_len(values); i++){
    // if the value is valid and is not an outlier, append the value
    if( mask[i] && (values[i] < (median +median*coeff) && values[i] > (median - median*coeff)) ) dynAppend(values_substituted, values[i]);
    // if the value is an outlier, append the median
    else dynAppend(values_substituted, median);
  }
  return values_substituted;
}

/*
    @brief Calculates the normalisation factor (median of all values)
        @param values_outer: array of values of the outer counters
        @param values_inner: array of values of the inner counters
        @return normalisation factor
    */
float CalculateNormalisationFactor(dyn_float values_outer, dyn_float values_inner)
{
  float norm=0;
  int count=0;
  for(int i=1; i<=dyn_len(values_outer); i++){
      norm = norm + values_outer[i];
      count++;
  }
    for(int i=1; i<=dyn_len(values_inner); i++){
        norm = norm + values_inner[i];
        count++;
    }
  if(count>0) norm = norm/count;
  else norm = 999.;
  return norm;
}

/**
  @brief Applies the PCA component to the values
    @param values_inner: array of values of the inner counters
    @param values_outer: array of values of the outer counters
    @param mask_inner: array of flags to indicate if the value is valid for the inner counters
    @param mask_outer: array of flags to indicate if the value is valid for the outer counters
    @param pca_weight: array of weights for the PCA component
    @return value of the PCA component
*/
float ApplyPcaComponent( dyn_float values_inner, dyn_float values_outer,
                          dyn_bool mask_inner, dyn_bool mask_outer,
                          dyn_float pca_weight)
{
  float output_value=0;

  for(int i=1; i<=dyn_len(values_inner); i++){
        output_value = output_value + values_outer[i] * pca_weight[2*i-1];
        output_value = output_value + values_inner[i] * pca_weight[2*i];
  }
  if(output_value==0.) output_value=999.;
  return output_value;
}

//----------------------------------------------------------------------
// weights calculated by the PCA for the position estimation


dyn_float pca_weight_x =  //    D-O         D-I         L-O         L-I        U-O        U-I        R-O        R-I
              makeDynFloat( -0.0638669, -0.0724104, -0.0689771, -0.0720538, 0.0620647, 0.070775 , 0.070492 , 0.0722513,
                            -0.0663558, -0.0706874, -0.0676406, -0.0715464, 0.0636811, 0.0720731, 0.0598751, 0.071733,
                            -0.0634858, -0.0724326, -0.0670366, -0.0724193, 0.0686147, 0.0717152, 0.0603623, 0.0718222,
                            -0.0663176, -0.072269,  -0.0657722, -0.0715803, 0.067675,  0.0722447, 0.0618328, 0.0723284,
                            -0.0652662, -0.0718161, -0.0607939, -0.0724551, 0.0685236, 0.0723187, 0.068423 , 0.0721188,
                            -0.0581349, -0.0718357, -0.0658982, -0.0715679, 0.0683036, 0.0724683, 0.0658217, 0.0722666,
                            -0.0700306, -0.0724295, -0.0637532, -0.0720479, 0.0664292, 0.0724925, 0.0638324, 0.0722808,
                            -0.0679761, -0.0716465, -0.0677819, -0.0723706, 0.0678199, 0.0724501, 0.0694015, 0.0721715,
                            -0.0640777, -0.0726076, -0.0694379, -0.0721762, 0.0650932, 0.0720598, 0.0707936, 0.0725562,
                            -0.0656003, -0.0724686, -0.062024 , -0.0713875, 0.0672944, 0.0715798, 0.0636053, 0.0722904,
                            -0.0680227, -0.0721779, -0.0645714, -0.0720725, 0.0706317, 0.0714257, 0.0666112, 0.0721004,
                            -0.0703212, -0.0726307, -0.0642078, -0.0723443, 0.0688665, 0.0721125, 0.0670625, 0.0720413,
                            -0.0690617, -0.0720349, -0.0682886, -0.0726731, 0.0688358, 0.0727239, 0.0676816, 0.0724089,
                            -0.0656461, -0.0720927, -0.0668145, -0.0719975, 0.0646793, 0.0722137, 0.0678367, 0.0718522,
                            -0.0668684, -0.0719524, -0.0697869, -0.0725226, 0.0673839, 0.0721752, 0.065634 , 0.072159,
                            -0.0663626, -0.0716826, -0.0708157, -0.0720291, 0.0674574, 0.072242 , 0.0688001, 0.0726284,
                            -0.065709 , -0.0723256, -0.0667188, -0.0722531, 0.0700967, 0.0726074, 0.0703317, 0.0719603,
                            -0.0707397, -0.0725747, -0.0702601, -0.0723983, 0.0684923, 0.0713808, 0.0694037, 0.0721389,
                            -0.0675835, -0.0712141, -0.0570862, -0.0723892, 0.0685103, 0.0721235, 0.0685467, 0.0719353,
                            -0.0652589, -0.0721748, -0.0696293, -0.0725488, 0.0632193, 0.0723795, 0.0690258, 0.0717908,
                            -0.0627372, -0.0716313, -0.0648461, -0.0715184, 0.0678818, 0.0724158, 0.068548 , 0.0712309,
                            -0.0666779, -0.0723057, -0.0691048, -0.07211  , 0.0677408, 0.0720196, 0.062748 , 0.0721021,
                            -0.0573847, -0.0719005, -0.0651362, -0.0719738, 0.0689597, 0.072055 , 0.0655592, 0.0723024,
                            -0.0641126, -0.0722345, -0.0636145, -0.0721935, 0.069713 , 0.0722629, 0.065558 , 0.0721125,
                            -0.0680406, -0.0726212, -0.0649006, -0.071955 , 0.0702267, 0.0725469, 0.0623548, 0.0723803,
                            -0.0558737, -0.071789 , -0.0644377, -0.0722342, 0.0702946, 0.0712663, 0.0652728, 0.0718027  ) ;


dyn_float pca_weight_y =  //    D-O         D-I         L-O         L-I        U-O        U-I        R-O        R-I
              makeDynFloat( 0.069144,  0.0717332, -0.0668161, -0.071722,  -0.0695418, -0.0708843, 0.0692123, 0.0719064,
                            0.0696022, 0.0709812, -0.0681813, -0.0716257, -0.068551,  -0.0710496, 0.0628595, 0.0716524,
                            0.0654455, 0.0710881, -0.0665131, -0.0715574, -0.0652971, -0.0717988, 0.0680942, 0.0712038,
                            0.0675603, 0.0717744, -0.0668619, -0.0713967, -0.0692652, -0.0716441, 0.0628044, 0.0714546,
                            0.0670219, 0.0714354, -0.0640359, -0.0716962, -0.0711117, -0.0712432, 0.0699096, 0.0718773,
                            0.0675958, 0.0708243, -0.0692515, -0.0716089, -0.0701471, -0.0716489, 0.064256,  0.0717407,
                            0.0646531, 0.0715001, -0.0666986, -0.0715988, -0.0650658, -0.0712976, 0.0646519, 0.0716425,
                            0.0634028, 0.0715904, -0.0660959, -0.0712695, -0.0687427, -0.0716663, 0.06785,   0.0710373,
                            0.0669106, 0.0715215, -0.0672349, -0.0713826, -0.066385,  -0.0712625, 0.0679799, 0.0707658,
                            0.0663759, 0.0718407, -0.0698845, -0.07114,   -0.0653076, -0.071058,  0.0505685, 0.0711757,
                            0.0690136, 0.0714196, -0.0662444, -0.0719177, -0.0650733, -0.0708243, 0.0685348, 0.0719204,
                            0.0716107, 0.0700847, -0.0670113, -0.071264,  -0.0669855, -0.071356,  0.0684852, 0.071452,
                            0.0658217, 0.0714825, -0.066535,  -0.0711522, -0.0665529, -0.0709754, 0.0699124, 0.0716352,
                            0.0660866, 0.0715234, -0.0704289, -0.0715932, -0.0651963, -0.0708565, 0.0684096, 0.0712122,
                            0.0684239, 0.0711605, -0.0671232, -0.071812,  -0.069609,  -0.0706052, 0.0667164, 0.0710751,
                            0.0681939, 0.0714,    -0.0640376, -0.0710411, -0.0687854, -0.0718597, 0.0646325, 0.0713121,
                            0.0645234, 0.0716033, -0.0641534, -0.0717974, -0.0696705, -0.0716585, 0.0665323, 0.0711639,
                            0.0688995, 0.0718474, -0.0681559, -0.0701617, -0.0676433, -0.0717966, 0.0677434, 0.0714209,
                            0.0675432, 0.0715849, -0.0683762, -0.0720504, -0.0677902, -0.0712286, 0.0689215, 0.0716753,
                            0.0668095, 0.0713532, -0.067743,  -0.0714313, -0.0693675, -0.0712476, 0.0691629, 0.0709959,
                            0.065821,  0.0718276, -0.0693252, -0.0714671, -0.0682065, -0.07158,   0.0708809, 0.0707138,
                            0.0652364, 0.0718155, -0.0695499, -0.0717578, -0.0694193, -0.0713831, 0.0686811, 0.071665,
                            0.0642909, 0.0718694, -0.0694119, -0.070864,  -0.0609426, -0.0718711, 0.0680716, 0.0713778,
                            0.0681083, 0.0714634, -0.0651414, -0.0713929, -0.0650347, -0.0716382, 0.0674654, 0.0717217,
                            0.0672355, 0.07147,   -0.0663713, -0.0711325, -0.066391,  -0.0717559, 0.0652675, 0.0714562,
                            0.0647626, 0.0718042, -0.0699842, -0.0706799, -0.0701326, -0.0710403, 0.0694823, 0.0706458  ) ;

dyn_float pca_weight_z =  //    D-O         D-I         L-O         L-I        U-O        U-I        R-O        R-I
              makeDynFloat( -0.06027,   -0.0222242,  -0.0535349, -0.0334571, -0.0680095,   0.0166122,   -0.0818566,  0.0204924,
                            -0.0684569,  0.00692554, -0.0741795, -0.0285677, -0.0688959,  -0.000812046, -0.0762366, -0.0208494,
                            -0.0616584, -0.00479381, -0.0755695, -0.0454568, -0.0771221,  -0.00983096,  -0.0783792, -0.00170127,
                            -0.0809569, -0.0613626,  -0.0838443, -0.0488381, -0.0759434,  -0.0429684,   -0.0825557, -0.0366114,
                            -0.0846281, -0.0785785,  -0.0875384, -0.0863185, -0.0877198,  -0.0837238,   -0.0887173, -0.0795472,
                            -0.0834764, -0.0833656,  -0.0893851, -0.0853989, -0.0758605,  -0.083548,    -0.0815316, -0.0770764,
                            -0.0715701, -0.0843237,  -0.0783782, -0.0668086, -0.0521482,  -0.0695698,   -0.0623343, -0.0552326,
                            -0.039134,  -0.045677,   -0.0431483, -0.02602,   -0.00348517, -0.022887,    -0.010272,  -0.010486,
                            0.0369856,  0.00276055,  -0.00400146, 0.0271394, 0.056536,    0.0296476,    0.0350862,  0.0394792,
                            0.064912,   0.0485166,   0.0569732,   0.0619031, 0.0751801,   0.0645278,    0.0726702,  0.0684368,
                            0.078297,   0.0729413,   0.0831009,   0.0777933, 0.0854099,   0.0763766,    0.0830374,  0.0846953,
                            0.0864801,  0.0878537,   0.0891313,   0.0883131, 0.0896792,   0.0894939,    0.0875001,  0.0891527,
                            0.090315,   0.0888785,   0.0881602,   0.0871951, 0.0870005,   0.087064,     0.0913607,  0.0866941,
                            0.0891073,  0.0860895,   0.0913325,   0.0857078, 0.0875664,   0.0798298,    0.0888626,  0.0842899,
                            0.085922,   0.081855,    0.0893552,   0.084043,  0.084457,    0.0760667,    0.0839327,  0.0804195,
                            0.0881315,  0.0753617,   0.0874323,   0.0746942, 0.0846633,   0.0731637,    0.0885725,  0.0747372,
                            0.083715,   0.076907,    0.0856233,   0.0723727, 0.0850975,   0.0571222,    0.0832695,  0.0660476,
                            0.0809628,  0.0663543,   0.0822299,   0.061677,  0.0765456,   0.0302523,    0.0839431,  0.0682346,
                            0.0784651,  0.0648753,   0.0793004,   0.0696676, 0.0746532,   0.0309542,    0.0828459,  0.051515,
                            0.0729071,  0.0377914,   0.0720762,   0.0516142, 0.0818543,   0.000451018,  0.0787336,  0.0324495,
                            0.0685563,  0.0753281,   0.075635,    0.0589327, 0.0718486,   0.010092,     0.0778239,  0.0673505,
                            0.0806366,  0.0431641,   0.0642309,   0.0579912, 0.0550001,   0.0611206,    0.0716642,  0.077021,
                            0.0666805,  0.0597484,   0.081021,    0.0476666, 0.0513315,   0.0584107,    0.0722551,  0.0622901,
                            0.0388694,  0.0713824,   0.0740293,   0.0532679, 0.052818,    0.0719898,    0.0688262,  0.0797379,
                            0.0584487,  0.0717433,   0.075791,    0.0783576, 0.0719499,   0.0651012,    0.0618466,  0.0748173,
                            0.0475768,  0.0470468,   0.0648421,   0.074352,  0.0567949,   0.0240389,    0.0704377,  0.0656605 ) ;


dyn_float pca_weight_A_x = //    D-O         D-I         L-O         L-I
                makeDynFloat(0.0889885, 0.0993366, 0.0995079, 0.101059,
                      				0.0908397, 0.101053, 0.0844143, 0.100528,
                      				0.0972902, 0.10066, 0.0880313, 0.101617,
                      				0.0957115, 0.101686, 0.0882059, 0.102119,
                      				0.0959474, 0.101913, 0.0974326, 0.101778,
                      				0.0963322, 0.101287, 0.0931081, 0.101595,
                      				0.0934001, 0.102137, 0.0903939, 0.101385,
                      				0.0948218, 0.101872, 0.0989538, 0.101263,
                      				0.0923763, 0.101891, 0.0993553, 0.101694,
                      				0.0941836, 0.101464, 0.0893475, 0.101779,
                      				0.0991647, 0.101028, 0.0955753, 0.101166,
                      				0.0973422, 0.101389, 0.0943208, 0.101188,
                      				0.095416, 0.101624, 0.0954439, 0.101411,
                      				0.0927932, 0.101898, 0.0950364, 0.100864,
                      				0.0935768, 0.10141, 0.0926499, 0.101553,
                      				0.0950344, 0.101439, 0.0954077, 0.101566,
                      				0.099476, 0.102053, 0.0981825, 0.101697,
                      				0.0967222, 0.100637, 0.0976171, 0.101506,
                      				0.0939411, 0.101552, 0.0970171, 0.100732,
                      				0.0891103, 0.101834, 0.0985424, 0.101479,
                      				0.0960406, 0.101857, 0.0965236, 0.100296,
                      				0.0950674, 0.101269, 0.088919, 0.101702,
                      				0.0982477, 0.101663, 0.0935151, 0.101481,
                      				0.0968593, 0.101779, 0.0903588, 0.101192,
                      				0.0980835, 0.101959, 0.0868719, 0.101917,
                      				0.0997735, 0.100775, 0.09357, 0.101263 ) ;

dyn_float pca_weight_A_y =  //    D-O         D-I         L-O         L-I
                makeDynFloat( -0.0984053 , -0.10038 , 0.0977468 , 0.101693 ,
                          		-0.0973498 , -0.100584 , 0.0890584 , 0.101343 ,
                          		-0.092916 , -0.101569 , 0.0962338 , 0.100539 ,
                          		-0.0977561 , -0.101476 , 0.0878159 , 0.100989 ,
                          		-0.100718 , -0.100982 , 0.0987775 , 0.101493 ,
                          		-0.0991635 , -0.101373 , 0.0911306 , 0.101509 ,
                          		-0.0921834 , -0.100898 , 0.0910414 , 0.101324 ,
                          		-0.0974803 , -0.101534 , 0.0962066 , 0.100486 ,
                          		-0.0937936 , -0.100812 , 0.0954416 , 0.0997851 ,
                          		-0.0916718 , -0.100614 , 0.0705324 , 0.100473 ,
                          		-0.0928601 , -0.100354 , 0.0971228 , 0.101733 ,
                          		-0.0944165 , -0.101047 , 0.0965036 , 0.101166 ,
                          		-0.0945505 , -0.10037 , 0.0991612 , 0.101102 ,
                          		-0.0926202 , -0.0999821 , 0.0966755 , 0.100559 ,
                          		-0.0988075 , -0.0997229 , 0.094588 , 0.100673 ,
                          		-0.0968592 , -0.101571 , 0.0919188 , 0.100978 ,
                          		-0.0986967 , -0.101285 , 0.0937476 , 0.100637 ,
                          		-0.0951728 , -0.101573 , 0.0963159 , 0.100889 ,
                          		-0.0963478 , -0.100673 , 0.0972734 , 0.101194 ,
                          		-0.098037 , -0.100664 , 0.097898 , 0.100486 ,
                          		-0.0966673 , -0.101009 , 0.100187 , 0.0998983 ,
                          		-0.0980279 , -0.100983 , 0.0972731 , 0.101312 ,
                          		-0.0862607 , -0.101643 , 0.0961827 , 0.100778 ,
                          		-0.0915025 , -0.10123 , 0.0953563 , 0.101188 ,
                          		-0.0937225 , -0.101587 , 0.0925483 , 0.100918 ,
                          		-0.0993365 , -0.10054 , 0.0979309 , 0.100004);

dyn_float pca_weight_A_z = //    D-O         D-I         L-O         L-I
                makeDynFloat(-0.0973699 , 0.0237486 , -0.118449 , 0.0307782 ,
                            -0.0990031 , -0.0015673 , -0.110194 , -0.0298795 ,
                            -0.111322 , -0.013935 , -0.113031 , -0.00328904 ,
                            -0.109467 , -0.0619196 , -0.118813 , -0.0523484 ,
                            -0.124752 , -0.119537 , -0.1266 , -0.114376 ,
                            -0.108635 , -0.118911 , -0.116954 , -0.110101 ,
                            -0.0741598 , -0.100158 , -0.0896405 , -0.079755 ,
                            -0.00469079 , -0.0334172 , -0.0153021 , -0.0148532 ,
                            0.0798552 , 0.0422142 , 0.0500341 , 0.0563409 ,
                            0.107461 , 0.0929597 , 0.104493 , 0.09903 ,
                            0.121958 , 0.108175 , 0.118151 , 0.121288 ,
                            0.127216 , 0.128401 , 0.124488 , 0.127227 ,
                            0.123507 , 0.124219 , 0.130135 , 0.124228 ,
                            0.124998 , 0.113896 , 0.126755 , 0.121078 ,
                            0.120533 , 0.108546 , 0.118482 , 0.114586 ,
                            0.121261 , 0.104796 , 0.126595 , 0.10726 ,
                            0.121954 , 0.0837141 , 0.11932 , 0.0944467 ,
                            0.109058 , 0.0450187 , 0.118747 , 0.0964076 ,
                            0.108289 , 0.0426132 , 0.119206 , 0.0759766 ,
                            0.11703 , 0.00147019 , 0.111657 , 0.0463116 ,
                            0.104562 , 0.0154268 , 0.110685 , 0.0947979 ,
                            0.0789095 , 0.0887892 , 0.104005 , 0.110299 ,
                            0.0725191 , 0.082541 , 0.104588 , 0.0874523 ,
                            0.0761016 , 0.103252 , 0.097719 , 0.113383 ,
                            0.103793 , 0.0924929 , 0.0882758 , 0.106307 ,
                            0.0820061 , 0.0331131 , 0.0995903 , 0.0952583);

dyn_float pca_weight_C_x = //   U-O        U-I        R-O        R-I
                makeDynFloat(0.0911185, 0.102666, 0.0984093, 0.102368,
                      				0.0941081, 0.101042, 0.0976427, 0.100379,
                      				0.0924174, 0.102973, 0.0955742, 0.102203,
                      				0.0959247, 0.102487, 0.0934434, 0.101818,
                      				0.0933837, 0.102323, 0.0850215, 0.102293,
                      				0.0852829, 0.101386, 0.0951819, 0.101573,
                      				0.0994581, 0.10234, 0.0890355, 0.102051,
                      				0.0958798, 0.102202, 0.0939005, 0.102799,
                      				0.091817, 0.102833, 0.0970432, 0.102737,
                      				0.0945956, 0.10198, 0.0876322, 0.101061,
                      				0.0950188, 0.102127, 0.0921403, 0.102085,
                      				0.0989232, 0.102348, 0.0929951, 0.101858,
                      				0.0976571, 0.102065, 0.0967358, 0.102451,
                      				0.0930169, 0.10211, 0.0949185, 0.101954,
                      				0.0957427, 0.102305, 0.099183, 0.102678,
                      				0.0947678, 0.101935, 0.100919, 0.102769,
                      				0.0910996, 0.101655, 0.0947124, 0.102719,
                      				0.0996427, 0.102873, 0.0993195, 0.102764,
                      				0.096521, 0.100897, 0.0817648, 0.1018,
                      				0.0892904, 0.102383, 0.0992003, 0.102672,
                      				0.0917687, 0.101855, 0.093907, 0.10164,
                      				0.0950524, 0.10244, 0.0974233, 0.102228,
                      				0.0828978, 0.101608, 0.0946753, 0.102186,
                      				0.0932517, 0.102684, 0.0926205, 0.102033,
                      				0.0960584, 0.102242, 0.0924706, 0.102533,
                      				0.078794, 0.101913, 0.0918412, 0.102271 ) ;

dyn_float pca_weight_C_y = //    U-O        U-I        R-O        R-I
                makeDynFloat( -0.0980511 , -0.101557 , 0.0943686 , 0.101298 ,
                              -0.0985412 , -0.100263 , 0.0968938 , 0.101256 ,
                              -0.0927723 , -0.100426 , 0.0941747 , 0.101123 ,
                              -0.0951099 , -0.101319 , 0.0950409 , 0.100733 ,
                              -0.0952141 , -0.101071 , 0.0914959 , 0.101388 ,
                              -0.0959561 , -0.0999475 , 0.097683 , 0.101043 ,
                              -0.0910079 , -0.100998 , 0.0937327 , 0.101187 ,
                              -0.089517 , -0.101264 , 0.0933937 , 0.100778 ,
                              -0.0946672 , -0.101249 , 0.0954103 , 0.10083 ,
                              -0.0940308 , -0.101561 , 0.0987688 , 0.100355 ,
                              -0.0972285 , -0.101109 , 0.0938358 , 0.10157 ,
                              -0.10118 , -0.0987974 , 0.0947965 , 0.100894 ,
                              -0.0936159 , -0.101311 , 0.0943543 , 0.100412 ,
                              -0.0930728 , -0.101309 , 0.0994309 , 0.101107 ,
                              -0.0967873 , -0.100586 , 0.0945616 , 0.10152 ,
                              -0.0966347 , -0.100817 , 0.0908778 , 0.100433 ,
                              -0.0912968 , -0.101279 , 0.0907688 , 0.101475 ,
                              -0.0975364 , -0.101514 , 0.0964713 , 0.0992618 ,
                              -0.0959015 , -0.10113 , 0.0967228 , 0.101801 ,
                              -0.0949278 , -0.100963 , 0.096226 , 0.100845 ,
                              -0.0934744 , -0.101645 , 0.0983785 , 0.100844 ,
                              -0.0928385 , -0.101575 , 0.0978644 , 0.10132 ,
                              -0.0914848 , -0.101526 , 0.0977503 , 0.10006 ,
                              -0.0964487 , -0.100865 , 0.0919367 , 0.100769 ,
                              -0.0955308 , -0.101215 , 0.0932714 , 0.100713 ,
                              -0.0916474 , -0.10159 , 0.0990429 , 0.0999029);

dyn_float pca_weight_C_z = //   U-O        U-I        R-O        R-I
                makeDynFloat(-0.0851928 , -0.0308683 , -0.0756737 , -0.046338 ,
                              -0.0947872 , 0.0099053 , -0.103549 , -0.0397928 ,
                              -0.0882488 , -0.00803032 , -0.106635 , -0.0653754 ,
                              -0.113363 , -0.0860996 , -0.117582 , -0.0684657 ,
                              -0.118109 , -0.109613 , -0.122494 , -0.120629 ,
                              -0.116445 , -0.11665 , -0.125176 , -0.118616 ,
                              -0.0999541 , -0.11762 , -0.10902 , -0.0927453 ,
                              -0.0554364 , -0.0640182 , -0.0604899 , -0.0361431 ,
                              0.050904 , 0.00299487 , -0.006233 , 0.0367435 ,
                              0.0908081 , 0.0672333 , 0.0787907 , 0.0863662 ,
                              0.109048 , 0.101528 , 0.116103 , 0.108742 ,
                              0.121042 , 0.122316 , 0.123972 , 0.122781 ,
                              0.125503 , 0.124059 , 0.123381 , 0.12115 ,
                              0.124631 , 0.120205 , 0.127705 , 0.119776 ,
                              0.119621 , 0.113893 , 0.124724 , 0.116836 ,
                              0.122412 , 0.105554 , 0.122617 , 0.103686 ,
                              0.116931 , 0.108402 , 0.119211 , 0.101837 ,
                              0.113174 , 0.0948587 , 0.114148 , 0.0849453 ,
                              0.110148 , 0.0912267 , 0.109012 , 0.096818 ,
                              0.10146 , 0.0537438 , 0.100011 , 0.0737953 ,
                              0.0957985 , 0.105007 , 0.104524 , 0.0833117 ,
                              0.113071 , 0.0587636 , 0.0896877 , 0.0828044 ,
                              0.0940248 , 0.0829873 , 0.11343 , 0.0681252 ,
                              0.056508 , 0.0995427 , 0.104538 , 0.0739949 ,
                              0.0840728 , 0.102158 , 0.107652 , 0.10946 ,
                              0.0651272 , 0.0662681 , 0.0910452 , 0.104726);



//**********************************************************************************************************************************
//**********************************************************************************************************************************
//**********************************************************************************************************************************


import os
from os.path import exists
import sys
from functools import reduce
from pathlib import Path
import numpy as np
import pandas as pd
from pandas.errors import EmptyDataError
import matplotlib.pyplot as plt
import yaml
import seaborn as sns
import colorcet as cc
from scipy.stats import trim_mean
import datetime as dt


# Function to transform original data (x) to pattern space (p)
def data_to_pca(x, config_dictionary):
    """
    Transform original data x to pattern space p using PCA.

    Args:
        x (numpy.ndarray): Input data.
        config_dictionary (dict): Dictionary containing configuration parameters.

    Returns:
        float: Transformed value in pattern space.
    """
    # Extract configuration parameters
    eigen_vectors = config_dictionary["gEigenVectors"]
    n_variables = config_dictionary["gNVariables"]
    mean_values = config_dictionary["gMeanValues"]
    sigma_values = config_dictionary["gSigmaValues"]
    p = 0
    for j in range(n_variables):
        p += (x[j])* eigen_vectors[j * n_variables]#-mean_values[j]) * eigen_vectors[j * n_variables] / sigma_values[j]
    return p

def transform_data(df_x_counters, config_dictionary):
    """
    Transform data using Principal Component Analysis (PCA).

    Args:
        df_x_counters (pd.DataFrame): Dataframe with counters data.
        config_dictionary (dict): Dictionary containing configuration parameters.

    Returns:
        pd.Series: Transformed data.
    """
    coef = config_dictionary["calib_coeff"]

    p_c = []
    for i in range(len(df_x_counters.index)):
        x = df_x_counters.iloc[i].to_numpy()
        a = data_to_pca(x, config_dictionary)
        p_c.append(a)

    pos = p_c  # + np.full(len(df_x_counters.index), 0.1 / coef)
    return pos

def main():
    # Load configuration
    stream = open("../config/config.yaml", 'r', encoding="utf-8")
    config = yaml.safe_load(stream) 
    stream = open("../config/pca_x_veloC_all.yaml", 'r', encoding="utf-8")
    config_pca_xC = yaml.safe_load(stream)

    stream = open("../config/pca_y_veloC_all.yaml", 'r', encoding="utf-8")
    config_pca_yC = yaml.safe_load(stream)

    stream = open("../config/pca_x_veloA_all.yaml", 'r', encoding="utf-8")
    config_pca_xA = yaml.safe_load(stream)

    stream = open("../config/pca_y_veloA_all.yaml", 'r', encoding="utf-8")
    config_pca_yA = yaml.safe_load(stream)

    stream = open("../config/pca_z_veloA_all.yaml", 'r', encoding="utf-8")
    config_pca_zA = yaml.safe_load(stream)

    stream = open("../config/pca_z_veloC_all.yaml", 'r', encoding="utf-8")
    config_pca_zC = yaml.safe_load(stream)

    stream = open("../config/pca_x_config.yaml", 'r', encoding="utf-8")
    config_pca_x = yaml.safe_load(stream)

    stream = open("../config/pca_y_config.yaml", 'r', encoding="utf-8")
    config_pca_y = yaml.safe_load(stream)

    # Load the data
    df = pd.read_csv("/Users/zenith378/Documents/Uni/Tesi/beamline_lumi_clustering/beamline/pbpb/fill_10398.csv") #df = pd.read_csv("../../../data_vdm_2022/output_data/df_counters_wo_offset.csv") #data_vdm_2024/input_data/data_merged.csv"
    df["TS"] = pd.to_datetime(df["TS"], format='%Y.%m.%d %H:%M:%S.%f')
    df = df.set_index("TS")
    #df_lumi = pd.read_csv('../../../data_vdm_2024/output_data/lumi.csv') #lumi9476.csv
    #df_lumiA = pd.read_csv('../../../data_vdm_2024/output_data/lumi_trim_wo_offsetA.csv')
    #df_lumiC = pd.read_csv('../../../data_vdm_2024/output_data/lumi_trim_wo_offsetC.csv')
    #df_lumi["TS"] = pd.to_datetime(df_lumi["TS"], format='%Y-%m-%d %H:%M:%S')
    #df_lumiA["TS"] = pd.to_datetime(df_lumiA["TS"], format='%Y-%m-%d %H:%M:%S')
    #df_lumiC["TS"] = pd.to_datetime(df_lumiC["TS"], format='%Y-%m-%d %H:%M:%S')
    #df_lumi["TS"] = df_lumi["TS"].dt.round('1s')
    #df_lumi = df_lumi.set_index("TS")
    #moltiplica tutti i valori del df per 1e3
    #df_lumi = df_lumi*1e3
    #df_lumiA = df_lumiA.set_index("TS")
    #df_lumiC = df_lumiC.set_index("TS")
    #print(df_lumi)
    #df = df.merge(df_lumi, left_index=True, right_index=True)
    #df = df.merge(df_lumiA, left_index=True, right_index=True)
    #df = df.merge(df_lumiC, left_index=True, right_index=True)
    #print(df)
    #select all columns ending with outer
    #df_lumi_outer = df_lumi.filter(regex='_outer')
    #df_lumi_inner = df_lumi.filter(regex='_inner')
    #vorrei fare due istogrammi uno per i contatori esterni e uno per i contatori interni prendendo i valori alla decima entry del df
    '''
    plt.figure(figsize=(10,7))
    df_lumi_outer.iloc[10].hist(bins=20)
    plt.title("Luminosity measurements from outer counters")
    plt.xlabel("Luminosity [Hz/\mu b]")
    plt.ylabel("Frequency")
    #calcola media e deviazione standard
    mean_outer = df_lumi_outer.iloc[10].mean()
    std_outer = df_lumi_outer.iloc[10].std()
    #print mean and standard deviation on canvas
    ax = plt.gca()
    plt.text(0.1,0.9,f'Mean: '+str(round(mean_outer,4)),transform=ax.transAxes)
    plt.text(0.1,0.8,f'Std: '+str(round(std_outer,4)),transform=ax.transAxes)
    plt.text(0.1,0.7,f'Relative std: '+str(round(std_outer/mean_outer,4)),transform=ax.transAxes)
    #plotta linee che rappresentano media +- 3% della media
    plt.axvline(mean_outer-0.05*mean_outer, color='r', linestyle='dashed', linewidth=1)
    plt.axvline(mean_outer+0.05*mean_outer, color='r', linestyle='dashed', linewidth=1)
    #plt.show()

    plt.figure(figsize=(10,7))
    df_lumi_inner.iloc[10].hist(bins=20)
    plt.title("Luminosity measurements from inner counters")
    plt.xlabel("Luminosity [Hz/\mu b]")
    plt.ylabel("Frequency")
    #calcola media e deviazione standard
    mean_inner = df_lumi_inner.iloc[10].mean()
    std_inner = df_lumi_inner.iloc[10].std()
    #print mean and standard deviation and also relative standard deviation on canvas
    ax = plt.gca()
    plt.text(0.1,0.9,f'Mean: '+str(round(mean_inner,4)),transform=ax.transAxes)
    plt.text(0.1,0.85,f'Std: '+str(round(std_inner,4)),transform=ax.transAxes)
    plt.text(0.1,0.8,f'Relative std: '+str(round(std_inner/mean_inner,4)),transform=ax.transAxes)
    #plotta linee che rappresentano media +- 3% della media
    plt.axvline(mean_inner-0.03*mean_inner, color='r', linestyle='dashed', linewidth=1)
    plt.axvline(mean_inner+0.03*mean_inner, color='r', linestyle='dashed', linewidth=1)
    #plt.show()

    #plot temporale della deviazione standard dei valori della luminsotà
    plt.figure(figsize=(10,7))
    df_lumi_outer.std(axis=1).plot(linestyle='', marker='.',markersize=2)
    plt.title("Standard deviation of luminosity measurements from outer counters")
    plt.xlabel("Time")
    plt.ylabel("Standard deviation [Hz/\mu b]")
    plt.ylim(0,0.2)
    #plt.show()

    plt.figure(figsize=(10,7))
    df_lumi_inner.std(axis=1).plot(linestyle='', marker='.',markersize=2)
    plt.title("Standard deviation of luminosity measurements from inner counters")
    plt.xlabel("Time")
    plt.ylabel("Standard deviation [Hz/\mu b]")
    plt.ylim(0,0.2)
    #plt.show()

    #plot temporale della media dei valori della luminsotà
    plt.figure(figsize=(10,7))
    df_lumi.std(axis=1).plot(linestyle='', marker='.',markersize=2)
    plt.title("Standard deviation of all luminosity measurements")
    plt.xlabel("Time")
    plt.ylabel("Standard deviation [Hz/\mu b]")
    plt.ylim(0,0.2)
    #plt.show()
    #seleziona valori per cui la media della luminosità è sopra la soglia
    df_lumi_rel = df_lumi[df_lumi.mean(axis=1)>0.01]
    df_lumi_outer_rel = df_lumi_outer[df_lumi_outer.mean(axis=1)>0.1]
    df_lumi_inner_rel = df_lumi_inner[df_lumi_inner.mean(axis=1)>0.1]
    #plot temporale della deviazione standard relativa dei valori della luminosità.
    plt.figure(figsize=(10,7))
    df_lumi_outer_rel.std(axis=1).div(df_lumi_outer_rel.mean(axis=1)).plot(linestyle='', marker='.',markersize=2)
    #df_lumi_outer.std(axis=1).div(df_lumi_outer.mean(axis=1)).plot()
    plt.title("Relative standard deviation of luminosity measurements from outer counters")
    plt.xlabel("Time")
    plt.ylabel("Relative standard deviation")
    plt.ylim(0,0.125)
    #plt.show()

    plt.figure(figsize=(10,7))
    df_lumi_inner_rel.std(axis=1).div(df_lumi_inner_rel.mean(axis=1)).plot(linestyle='', marker='.',markersize=2)
    plt.title("Relative standard deviation of luminosity measurements from inner counters")
    plt.xlabel("Time")
    plt.ylabel("Relative standard deviation")
    plt.ylim(0,0.07)
    #plt.show()

    plt.figure(figsize=(10,7))
    df_lumi_rel.std(axis=1).div(df_lumi_rel.mean(axis=1)).plot(linestyle='', marker='.',markersize=2)
    plt.title("Relative standard deviation of all luminosity measurements")
    plt.xlabel("Time")
    plt.ylabel("Relative standard deviation")
    plt.ylim(0,0.10)
    #plt.show()

'''



    sigma_lhcb = config["sigma_lhcb"]
    lhcb_freq = config["lhc_frequency"]
    nCollisions = config["nCollisions"]
    #df['mu']=df['lumi']*sigma_lhcb/(lhcb_freq*nCollisions)
    #df['muA']=df['lumiA']*sigma_lhcb/(lhcb_freq*nCollisions)
    #df['muC']=df['lumiC']*sigma_lhcb/(lhcb_freq*nCollisions)
    counters_col_all = [col for col in df.columns if col.startswith("M")] 
    counters_colC = config["veloC_all"]
    #print(counters_colC)
    #counters_colC = [col.replace("_outer", "_A").replace("_inner", "_B") for col in counters_colC]
    #print(counters_colC)
    #counters_colC = [col for col in counters_colC if col in counters_col_all]
    #print(counters_col_all)
    #print((counters_colC))
    counters_colA = config["veloA_all"]
    #counters_colA = [col.replace("_outer", "_A").replace("_inner", "_B") for col in counters_colA]
    #counters_colA = [col for col in counters_colA if col in counters_col_all]
    #print(len(counters_col))
    all_counters = config_pca_x["columns"]
    #
    df_counters_all = df[counters_col_all]
    #substitute the value 12237598 with Nan
    #df_counters_all = df_counters_all.replace(12237598, np.nan)
    #replace badread values with no data
    #df_counters_all = df_counters_all.replace(12237598, np.nan)
    #if value is less than 1 replace it with nan
    df_counters_all = df_counters_all.where(df_counters_all<1)

    #if one raw is all nan drop it
    df_counters_all = df_counters_all.dropna(how='all')
    #if one column is all nan drop it
    df_counters_all = df_counters_all.dropna(axis=1, how='all')
    #print(df_counters_all)

    counters_colA = [col for col in counters_colA]
    df_counters_all = df_counters_all.rename(columns=lambda x: x.replace('_outer', '_A').replace('_inner', '_B'))
    #df_countersC = df_counters_all[counters_colC]
    #df_countersA = df_counters_all[counters_colA]
    col_a = [col for col in df_counters_all.columns if col.endswith("_A")]
    col_b = [col for col in df_counters_all.columns if col.endswith("_B")]
    #media_aA = [col for col in df_countersA.columns if col.endswith("_A")]
    #media_bA = [col for col in df_countersA.columns if col.endswith("_B")]
    #media_aC = [col for col in df_countersC.columns if col.endswith("_A")]
    #media_bC = [col for col in df_countersC.columns if col.endswith("_B")]
    mediana_a = df_counters_all[col_a].median(axis=1)
    mediana_b = df_counters_all[col_b].median(axis=1)
    trim_a = df_counters_all[col_a].apply(lambda x: trim_mean(x, 0.15), axis=1)
    trim_b = df_counters_all[col_b].apply(lambda x: trim_mean(x, 0.15), axis=1)
    trim = (trim_a + trim_b)/2
    df["mu"] = trim
    df_counters_outer_norm = df_counters_all[col_a].div(mediana_a,axis=0)
    df_counters_inner_norm = df_counters_all[col_b].div(mediana_b,axis=0)
    #media_aA = df_countersA[media_aA].mean(axis=1)
    #media_bA = df_countersA[media_bA].mean(axis=1)
    #media_aC = df_countersC[media_aC].mean(axis=1)
    #media_bC = df_countersC[media_bC].mean(axis=1)

    #ciao giulietto tvb <3 <3 <3
    # Definisci i limiti del range (0.5-1.5 volte la mediana)
    range_0_5_outer = mediana_a * 0.5
    range_1_5_outer = mediana_a * 1.5
    range_0_5_inner = mediana_b * 0.5
    range_1_5_inner = mediana_b * 1.5
    #print(df_counters.columns)
    #media troncata al 15% dei contatori
    #df['mu'] = df_counters_all.apply(lambda x: trim_mean(x, 0.15), axis=1)
    print(mediana_a)
    print(mediana_b)

    #distriuzione dei valori dei contatori
    plt.figure(figsize=(10,7))
    df_counters_all.iloc[361].hist(bins=20)
    plt.title("Distribution of counters values at 19-11-2024 12:00")
    plt.xlabel("Counter values")
    plt.ylabel("Frequency")
    #calcola linee che rappresentano il 15% e l'85% percentili
    #q_15 = df_counters_all.iloc[10].quantile(0.15)
    #q_85 = df_counters_all.iloc[10].quantile(0.85)
    #disegna mediane e i range
    plt.axvline(mediana_a[361], color='black', linestyle='solid', linewidth=3)
    plt.axvline(mediana_b[361], color='r', linestyle='solid', linewidth=3)
    plt.axvline(range_0_5_outer[361], color='black', linestyle='dashed', linewidth=3)
    plt.axvline(range_1_5_outer[361], color='black', linestyle='dashed', linewidth=3)
    plt.axvline(range_0_5_inner[361], color='r', linestyle='dashed', linewidth=3)
    plt.axvline(range_1_5_inner[361], color='r', linestyle='dashed', linewidth=3)
    #print percentiles on canvas
    #plt.axvline(q_15, color='r', linestyle='dashed', linewidth=1)
    #plt.axvline(q_85, color='r', linestyle='dashed', linewidth=1)



    plt.show()


    
    
    for col in all_counters:
        if col.endswith("_A"):
            if (col not in df_counters_all.columns):
                df_counters_all[col] = mediana_a
                print('column_does_not_exist')
                print(col)
            else:
                tmp_median = df_counters_outer_norm[col].median()

                df_counters_all[col] = np.where(df_counters_all[col]<range_0_5_outer, mediana_a, df_counters_all[col])
                #df_counters_all[col] = np.where(df_counters_outer_norm[col]<tmp_median*0.98, mediana_a*tmp_median, df_counters_all[col])
                #df_counters_all[col] = np.where(df_counters_outer_norm[col]>tmp_median*1.02, mediana_a*tmp_median, df_counters_all[col])
                df_counters_all[col] = np.where(df_counters_all[col]>range_1_5_outer, mediana_a, df_counters_all[col])
                #substitute nan values with median
                df_counters_all[col] = df_counters_all[col].fillna(mediana_a)

        elif col.endswith("_B"):
            if (col not in df_counters_all.columns):
                df_counters_all[col] = mediana_b
                print(col)
            else:
                tmp_median = df_counters_inner_norm[col].median()
                df_counters_all[col] = np.where(df_counters_all[col]<range_0_5_inner, mediana_b, df_counters_all[col])
                df_counters_all[col] = np.where(df_counters_all[col]>range_1_5_inner, mediana_b, df_counters_all[col])
                #df_counters_all[col] = np.where(df_counters_inner_norm[col]<tmp_median*0.9, mediana_b*tmp_median, df_counters_all[col])
                #df_counters_all[col] = np.where(df_counters_inner_norm[col]>tmp_median*1.1, mediana_b*tmp_median, df_counters_all[col])
                #substitute nan values with median
                df_counters_all[col] = df_counters_all[col].fillna(mediana_b)
        
   
    df_counters_all = df_counters_all.reindex(sorted(df_counters_all.columns), axis=1)
    df_counters_all = df_counters_all.rename(columns=lambda x: x.replace('_A', '_outer').replace('_B', '_inner'))
    df_countersC = df_counters_all[counters_colC]
    df_countersA = df_counters_all[counters_colA]
    #print(len(counters_col))
    #print(df_counters_all.columns)
    
    df['mu_A'] = df_countersA.mean(axis=1)
    df['mu_C'] = df_countersC.mean(axis=1)
    df['mu'] = df_counters_all.mean(axis=1)
    #df['mu'] = df['lumi_plume']
    df = df.dropna(how='all')
    #cut values of mu below 5
    df_counters_all = df_counters_all[df['mu']>0.001]
    df = df[df['mu']>0.001]
    #df_counters_normalized = pd.DataFrame(index=df_counters.index)
    df_position = pd.DataFrame(index=df_counters_all.index)
    # Aggiungi "_outer" a ogni elemento della lista
    #outer_names = [item + "_A" for item in original_list]
    #inner_names = [item + "_B" for item in original_list]
    #all_column_names = outer_names + inner_names
 #   merged_df = df.merge(df_lumi, left_index=True, right_index=True)       

    #merged_df = merged_df.dropna()

#    df_lumi = df[['lumi']]

    #missing_columns=[col for col in all_column_names if col not in df_counters.columns]
    
    #df_position["Nominal_Displacement_B1_sepPlane"]=df["Nominal_Displacement_B1_sepPlane"]
    #df_position["Nominal_Displacement_B2_sepPlane"]=df["Nominal_Displacement_B2_sepPlane"]
    #df_position["Nominal_Displacement_B1_xingPlane"]=df["Nominal_Displacement_B1_xingPlane"]
    #df_position["Nominal_Displacement_B2_xingPlane"]=df["Nominal_Displacement_B2_xingPlane"]
    
    #df_position = df_position[df_position["Nominal_Displacement_B1_sepPlane"]==df_position["Nominal_Displacement_B2_sepPlane"]]
    #df_position = df_position[df_position["Nominal_Displacement_B1_xingPlane"]==df_position["Nominal_Displacement_B2_xingPlane"]]
    #df_position["y_set"]=(df["Nominal_Displacement_B2_sepPlane"]+df["Nominal_Displacement_B1_sepPlane"])/2
    #df_position["x_set"]=(df["Nominal_Displacement_B2_xingPlane"]+df["Nominal_Displacement_B1_xingPlane"])/2
    #df_filtered = df_counters[(df_position["x_pos"] > -0.45) & (df_position["x_pos"] < 0.45) & (df_position["x_pos"] != 0.) & (abs(df_position["y_pos"]) < 0.0001)]
    #mean_values = list(df_filtered.mean(axis=0))
    #std_values = list(df_filtered.std(axis=0))
    #df_filtered = df_counters[(df_position["y_pos"] > -0.45) & (df_position["y_pos"] < 0.45) & (df_position["y_pos"] != 0.) & (abs(df_position["x_pos"]) < 0.0001)]
    #mean_values_y = list(df_filtered.mean(axis=0))
    #std_values_y = list(df_filtered.std(axis=0))
    #config_pca_x["gMeanValues"] = mean_values
    #config_pca_x["gSigmaValues"] = std_values
    #config_pca_y["gMeanValues"] = mean_values_y
    #config_pca_y["gSigmaValues"] = std_values_y
    # Transform data using PCA
    
    df_counters_all["1_comp_x"] = transform_data(df_counters_all, config_pca_x)
    df_counters_all["1_comp_y"] = transform_data(df_counters_all, config_pca_y)
    df_countersC["1_comp_x"] = transform_data(df_countersC, config_pca_xC)
    df_countersC["1_comp_y"] = transform_data(df_countersC, config_pca_yC)
    df_countersA["1_comp_x"] = transform_data(df_countersA, config_pca_xA)
    df_countersA["1_comp_y"] = transform_data(df_countersA, config_pca_yA)
    df_countersA["1_comp_z"] = transform_data(df_countersA, config_pca_zA)
    df_countersC["1_comp_z"] = transform_data(df_countersC, config_pca_zC)
    z1=-(df_countersC["1_comp_z"]/df['mu'])
    z2=-(df_countersA["1_comp_z"]/df['mu'])
    df_position["x_estimate"] = (df_counters_all["1_comp_x"]/df['mu'])/1.09177+0.30965/1.09177#/1.20491+0.23720/1.20491#/0.00583+0.00115/0.00583#/1.21069+0.23825/1.21069#/0.00349+0.00083/0.00349#/0.03147+0.008927/0.03147#/0.01051+0.002731/0.01051#/0.01084+0.002818/0.01084#+0.2182)*0.9662-0.0144#/0.02954+ 0.003657/0.02954 #/(0.2759)+0.03415/0.2759#/df_lumi['lumi'] * 8.*0.0112450/(63.4*0.2759)+0.03415/0.2759
    df_position["y_estimate"] = -(df_counters_all["1_comp_y"]/df['mu'])/1.03565+0.06694/1.03565#/1.09919+0.02553/1.09919#/0.00532+0.00012/0.00532#/1.10336+0.02564/1.10336#/0.00314-0.00003/0.00314#/0.02985+0.0001929/0.02985#/0.009755+0.0004956#/0.01006+0.0005095/0.01006#*34.73+0.01834+0.004832#/0.02786  +0.0001357/0.02786 #/(0.2592)+0.001214/0.2759#/df_lumi['lumi']
    df_position["xVeloC"] = -(df_countersC["1_comp_x"]/df['mu'])/0.78941+11.03146/0.78941#/0.88906+11.04425/0.88906#/0.00413+0.05334/0.00413#/0.99212+11.15395/0.99212#/0.00241+0.05205/0.00241#/0.02301+0.318/0.02301#/0.007341+0.1023/0.007341#/0.008613+0.106/0.008613#*46.62+13.33#*45.83+13.11#*4.321+11.38
    df_position["yVeloC"] = (df_countersC["1_comp_y"]/df['mu'])/0.79598+0.10051/0.79598#0.86935+0.10603/0.86935#/0.00421+0.00051/0.00421#/0.87235+0.10638/0.87235#/0.00292+0.00025/0.00292#/0.02293+0.002895/0.02293#/0.007368+0.0008725/0.007368#/0.007609+0.0008996/0.007609#*43.91+0.1226#*43.91+0.1228#*8.631-0.1176
    df_position["xVeloA"] = (df_countersA["1_comp_x"]/df['mu'])/0.76674-10.50362/0.76674#/0.81536-10.58366/0.81536#/0.00414-0.05132/0.00414#/0.71712-10.57377/0.71712#/0.00254-0.05057/0.00254#/0.02169-0.3026/0.02169#/0.007176-0.09767/0.007176#/0.006864-0.1003/0.006864#*43.13-11.91#*47.01-12.89#*4.283-10.83
    df_position["yVeloA"] = -(df_countersA["1_comp_y"]/df['mu'])/0.66291+0.01324/0.66291#/0.68642-0.03456/0.68642#/0.00332-0.00017/0.00332#/0.68934-0.03475/0.68934#/0.00159-0.00004/0.00159#/0.01911+0.0003813/0.01911#/0.006114+0.00009598/0.006114#/0.0063+0.0000999/0.0063#*55.46-0.05156#*55.41-0.04321#*10.38-0.1218
    df_position["zVeloA"] = 7525*z2+956.5 #7779*z2+983.3#-4.977e07*z2**3-1.904e07*z2**2-2.43e06*z2-1.021e05
    df_position["zVeloC"] = 5872*z1+704.1 #5983*z1+715.6  #-2.0471e07*(z1**3) -7.439e06*(z1**2) -8.963e05*z1- 3.551e04#7260*z1+919.8#-4.015e06*z1**3-1.62e06*z1**2-2.09e06*z1-8688
    df_position["1_comp_x"] = df_counters_all["1_comp_x"]/df['mu']
    df_position["1_comp_y"] = df_counters_all["1_comp_y"]/df['mu']
    df_position["1_comp_Ax"] = df_countersA["1_comp_x"]/df['mu']
    df_position["1_comp_Ay"] = df_countersA["1_comp_y"]/df['mu']
    df_position["1_comp_Cx"] = df_countersC["1_comp_x"]/df['mu']
    df_position["1_comp_Cy"] = df_countersC["1_comp_y"]/df['mu']
    df_position["1_comp_Az"] = df_countersA["1_comp_z"]/df['mu']
    df_position["1_comp_Cz"] = df_countersC["1_comp_z"]/df['mu']
    df_position["mu"] = df['mu']
    #select only the data for which B1 and B2 are in the same position
    #select only the rows for which the nominal displacement is the same
    #df_position = df_position[abs(df_position["Nominal_Displacement_B1_sepPlane"]-df_position["Nominal_Displacement_B2_sepPlane"])<0.01]
    #df_position = df_position[abs(df_position["Nominal_Displacement_B1_xingPlane"]-df_position["Nominal_Displacement_B2_xingPlane"])<0.01]
    #df_position[['x_pos', 'y_pos', 'x_estimate', 'y_estimate']].to_csv("../../beamline_calibration/calibration_dataset/offline_LSC_2024_mean.csv")
    #df_filtered = df_position[(df_position["x_pos"] > -0.45) & (df_position["x_pos"] < 0.45) & (df_position["x_pos"] != 0.)]
    #df_filtered.to_csv("../../beamline_calibration/calibration_dataset/x_2024_veloC.csv")

    #df_filtered = df_position[(df_position["y_pos"] > -0.45) & (df_position["y_pos"] < 0.45) & (df_position["y_pos"] != 0.)]
    df_position.to_csv("../../beamline_calibration/calibration_dataset/fill_10398.csv")

if __name__ == "__main__":
    main()
import yaml
import re

def read_c_file(file_path):
    with open(file_path, 'r') as file:
        content = file.read()
    return content

def extract_value(content, pattern):
    match = re.search(pattern, content)
    if match:
        return int(match.group(1))
    return None

def extract_array(content, pattern):
    match = re.search(pattern, content, re.DOTALL)
    if match:
        array_string = match.group(1).replace('\n', '').replace(' ', '')
        array_list = [float(num) for num in array_string.split(',') if num]
        return array_list
    return []

class LiteralList(list):
    pass

def literal_list_representer(dumper, data):
    return dumper.represent_sequence('tag:yaml.org,2002:seq', data, flow_style=True)

yaml.add_representer(LiteralList, literal_list_representer)

def write_yaml(file_path, data):
    with open(file_path, 'w') as file:
        yaml.dump(data, file, default_flow_style=False)

def main():
    string_file = 'pca_x_VELO_A_unnormalized'
    input_c_file = string_file +'.C'
    output_yaml_file = '../../../beamline_estimation/config/'+string_file+'.yaml'
    
    content = read_c_file(input_c_file)
    
    gNVariables = extract_value(content, r'gNVariables\s*=\s*(\d+);')
    gEigenVectors = extract_array(content, r'gEigenVectors\[\]\s*=\s*\{([^}]*)\};')
    gSigmaValues = extract_array(content, r'gSigmaValues\[\]\s*=\s*\{([^}]*)\};')
    gMeanValues = extract_array(content, r'gMeanValues\[\]\s*=\s*\{([^}]*)\};')
    gEigenValues = extract_array(content, r'gEigenValues\[\]\s*=\s*\{([^}]*)\};')
    
    data = {
        'gNVariables': gNVariables,
        'gEigenVectors': LiteralList(gEigenVectors),
        'gSigmaValues': LiteralList(gSigmaValues),
        'gMeanValues': LiteralList(gMeanValues),
        'gEigenValues': LiteralList(gEigenValues)
    }
    
    write_yaml(output_yaml_file, data)

if __name__ == "__main__":
    main()

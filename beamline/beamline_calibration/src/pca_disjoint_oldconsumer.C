
#include "TPrincipal.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include <iostream>
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TBrowser.h"
#include "TPaveText.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TF1.h"
#include <ctime>

Double_t data2comp(Double_t *x, const TMatrixD gEigenVectors, Int_t gNVariables) {
    Double_t p = 0;
    for (Int_t j = 0; j < gNVariables; j++)
    {
      p += x[j] * (gEigenVectors(j,0));
    }
    return p;

  }

void pca_disjoint_oldconsumer()
{
   using namespace std;
   int nbin = 12;
   using namespace ROOT::VecOps;
   Int_t integration_costant = 20000;
   Int_t train_size = 2;
   std::string variable= "x"; 
   std::string velo_side= "A";
   TString res_name="Residuals from pv "+variable+" mean  [mm]";
   //std::string numpix = "";
   std::string numpix = "";
   //TString filename = "../MC_data/tree"+numpix+"_20k_vdm_" + variable + "_compact.root";
   //TString filename = "../MC_data/tree_100k_lumi.root";
   //TString filename = "../MC_data/tree"+numpix+"_100k_" + variable + "_veloC_all.root";
   std::string postname = "";
   if(velo_side!="")
      postname = "_VELO_"+velo_side;
   //TString filename = "../MC_data/" +variable+ "_tree"+postname+".root";
   TString filename = "../MC_data/" +variable+ "_tree"+postname+".root";
   ROOT::RDataFrame df("Events", filename);
   Double_t std_dev_num=0;
   if(variable=="z"){
      std_dev_num=64/sqrt(111000);
   }
   else if(variable=="x" || variable == "y"){
      std_dev_num=0.027/sqrt(111000);
   }

   TString nominal = "nominal_"+variable;
   //TString clu_string = "pv_" + variable + "_cluster";
   TString clu_string = nominal;
   auto vec_cluster = df.Take<ROOT::RVec<unsigned long>>("array_cluster").GetValue();
   auto vec_z = df.Take<double, RVec<double>>(nominal).GetValue();
   auto vec_pvz = df.Take<double, RVec<double>>(clu_string).GetValue();
   auto sorted_idx = StableArgsort(vec_z);
   auto sorted_vec_z = StableSort(vec_z);
    Double_t *sorted_pvz = new Double_t[sorted_vec_z.size()];
    std::vector<ROOT::RVec<unsigned long>> sorted_cluster;
    for (int j = 0; j < vec_z.size(); j++)
    {
        sorted_pvz[j] = vec_pvz[sorted_idx[j]];
        sorted_cluster.insert(sorted_cluster.begin() + sorted_idx[j], vec_cluster[j]);
    }

   // Create the new branches

   Int_t N = vec_pvz.size();
   Int_t m = N / integration_costant;
   Int_t n = vec_cluster[0].size();

   cout << "*************************************************" << endl;
   cout << "*         Principal Component Analysis          *" << endl;
   cout << "*                                               *" << endl;
   cout << "*  Number of variables:           " << setw(4) << n
        << "          *" << endl;
   cout << "*  Number of data points:         " << setw(8) << m
        << "      *" << endl;
   cout << "*                                               *" << endl;
   cout << "*************************************************" << endl;

   // Initilase the TPrincipal object. Use the empty string for the
   // final argument, if you don't wan't the covariance
   // matrix. Normalising the covariance matrix is a good idea if your
   // variables have different orders of magnitude.
   TPrincipal *principal = new TPrincipal(n, "ND");

   // fill the data points pushing back the ith element of the arrays previously defined
   Double_t vertex_z = 0;
   ROOT::RVecD tmp;
   ROOT::RVecD vert_z_m;
   ROOT::RVecD vert_z_s;
   ROOT::RVecD first_comp;
   //ROOT::RVecD final_vec = {0, 0, 0, 0};//, 0, 0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0}; //, 0, 0, 0, 0, 0, 0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0,0,0};
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
   ROOT::RVecD final_vec;
   for(int i=0;i<n;i++){
      final_vec.push_back(0);
   }
   Int_t sum = 0;
    Int_t n_zeros = 0;
    Int_t n_pos = 6;
    if(variable=="z") n_pos=11;

   //for (auto idx : sorted_idx)
   for(int i=0;i<sorted_vec_z.size();i++)
   {
    for(int k=0;k<n_pos;k++){
        if (i == (k*100000+train_size * integration_costant)){
        i += (100000-(integration_costant * train_size));
        }
    }
     if(i == sorted_vec_z.size()) break;
    //if (((i) % (integration_costant * train_size) == 0)&& i!=0)
      if (sorted_pvz[i] > 500 || sorted_pvz[i] < -500|| sorted_pvz[i]!=vec_pvz[i] ||  sorted_pvz[i]==0)
      {
         //std::cout << "Ho skippato" << std::endl;
        n_zeros++;
         continue;
      }
      final_vec += sorted_cluster[i];

      if (((i) % integration_costant == integration_costant - 1) || ((i) == N - 1))
      {
       // std::cout << i << std::endl;
         Double_t *data = new Double_t[n];
         final_vec /= sum;

         for (int j = 0; j < final_vec.size(); j++)
         {
            data[j] = final_vec[j];
         }

         principal->AddRow(data);

        //ROOT::RVecD final_vec = {0, 0, 0, 0};//, 0, 0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0}; //, 0, 0, 0, 0, 0, 0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0,0,0};
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
         for(int j=0;j<n;j++){
            final_vec[j]=0;
         }
         sum = 0;

      }
      sum++;
   }
    std::cout << "Number of zeros in train: " << n_zeros << std::endl;
    n_zeros =0;
   // Do the actual analysis
   principal->MakePrincipals();

   // Print out the result on
   principal->Print();

   // Make two functions to map between feature and pattern space
   TString code = "pca_config/pca_"+variable+postname+"_unnormalized.C";
   principal->MakeCode(filename=code);
   //principal->MakeCode();

   auto covm = principal->GetCovarianceMatrix();

   // Print the covariance matrix
   auto cov_array = covm->GetMatrixArray();                                                    // + z_lumi
   TH2D *cov_hist = new TH2D("", "covariance matrix; cluster region; cluster region;", n, 0, n // + z_lumi
                             ,
                             n, -n, 0);
   int index = 0;
   for (int x = 0; x < n; x++)
   {
      for (int y = 0; y < n; y++)
      {
         cov_hist->SetBinContent(x + 1, n - y, cov_array[index]);
         index++;
      }
   }

   //auto c11 = new TCanvas();
   //cov_hist->Draw("colz");
   //c11->SaveAs("covariance_matrix.png");
   auto eigenvectors = principal->GetEigenVectors();
   Int_t k = 0;
   auto g3 = new TGraphErrors();
   auto g1 = new TGraphErrors();
   int sum1 = 0;
   for (int i=integration_costant*train_size;i<sorted_vec_z.size();i++)
   {
    if ((i % 100000 == 0) && i!=0) {
        i += (integration_costant * train_size);
    }
      if (sorted_pvz[i] > 500 || sorted_pvz[i] < -500 || sorted_pvz[i]!=sorted_pvz[i] || sorted_pvz[i]==0)
      {
        //std::cout << "Ho skippato" << std::endl;
        n_zeros++;
         continue;
      }
//std::cout << i << std::endl;
      final_vec += sorted_cluster[i];
      tmp.push_back(sorted_pvz[i]);

      if ((i % integration_costant == integration_costant - 1) || (i == N - 1))
      {
      Double_t *data = new Double_t[n];

      Double_t *p = new Double_t[n];
         final_vec = final_vec / sum1;
         Double_t mean_v = Mean(tmp);
         //Double_t std_v = StdDev(tmp);
         Double_t std_v = std_dev_num; 
         for (int j = 0; j < final_vec.size(); j++)
         {
            data[j] = final_vec[j];
         }
         //principal->X2P(data, p);
         //Double_t comp = p[0];
         Double_t comp = data2comp(data,*eigenvectors,n);
         comp=comp/5.5; //normalization to pile-up
         vert_z_m.push_back(mean_v);
         vert_z_s.push_back(std_v);
         if(mean_v==0.5 || mean_v==-0.5){
            //std::cout << "mean_v: " << mean_v  << std::endl;
            //std::cout << final_vec << std::endl;
         }
         first_comp.push_back(comp);
         g3->SetPoint(k,mean_v,comp);
         g3->SetPointError(k,std_v,0);
         g1->SetPoint(k,comp,mean_v);
         g1->SetPointError(k,0,std_v);
         k++;
          //ROOT::RVecD final_vec = {0, 0, 0, 0};//, 0, 0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0}; //, 0, 0, 0, 0, 0, 0, 0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0,0,0};
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            //0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
         for(int j=0;j<n;j++){
            final_vec[j]=0;
         }
                                     sum1 = 0;
         tmp.clear();
      }
      sum1++;
   }
    std::cout << "Number of zeros in test: " << n_zeros << std::endl;  
   auto tp1 = new TPaveText(0.65, 0.4, 0.85, 0.6, "NDC");
   tp1->AddText("Beamline Monitoring");
   tp1->AddText("Giulio Cordova");
   std::time_t time = std::time({});
   char timeString[std::size("yyyy-mm-ddThh:mm:ssZ")];
   std::strftime(std::data(timeString), std::size(timeString),
                  "%FT%TZ", std::gmtime(&time));
   tp1->AddText(timeString);
   auto tp2 = new TPaveText(0.65, 0.4, 0.85, 0.6, "NDC");
   gStyle->SetOptFit(1111);
   gStyle->SetLabelFont(43, "XYZ");
   gStyle->SetLabelSize(19, "xyz");
   gStyle->SetTitleSize(19, "xyz");
   gStyle->SetTitleFont(43, "XYZ");
   gStyle->SetOptStat(0);

   // tp2->AddText("only positive labels");
   //tp2->AddText("3<=st_ID<16");
   if(numpix!="")
      tp2->AddText("cluster with 1 pixel omitted");
   // tp2->AddText("stationID<16");
   TString integration_string = "integration over " + std::to_string(integration_costant)+ " events";
   tp2->AddText(integration_string);
   tp2->AddText("2 pts per pv in train");
   tp2->AddText("3 pts per pv in test");
   TString canvas_name="Nominal "+variable+ " estimator";


   auto c = new TCanvas("c", canvas_name, 800, 600);
   c->SetGrid();

   auto f1 = new TF1("f1", "pol3");
   g1->Fit(f1,"EX0");
   auto gRes = new TGraph();
   auto res_array = new Double_t[first_comp.size()];
   for (int i = 0; i < first_comp.size(); i++)
   {
      auto y = f1->Eval(first_comp[i]);
      gRes->SetPoint(i, first_comp[i], vert_z_m[i] - y);
      res_array[i] = vert_z_m[i] - y;
      
   }

   std::cout << std::setprecision(10) << f1->GetParameter(0) << std::endl;
   std::cout << std::setprecision(10) << f1->GetParameter(1) << std::endl;
   std::cout << std::setprecision(10) << f1->GetParameter(2) << std::endl;
   std::cout << std::setprecision(10) << f1->GetParameter(3) << std::endl;



   TPad *pad1 = new TPad("pad1", "The pad 80 of the height", 0.0, 0.2, 1.0, 1.0);
   TPad *pad2 = new TPad("pad2", "The pad 20 of the height", 0.0, 0.0, 1.0, 0.25);
   pad1->Draw();
   pad1->SetGrid();
   pad2->Draw();
   pad2->SetGrid();
   pad1->cd();

   g1->SetTitle(canvas_name);

   g1->GetYaxis()->SetTitle("mean vertex position [mm]");


   g1->SetMarkerStyle(5);

   g1->Draw("AP");
   f1->Draw("same");

   tp1->Draw();
   tp2->Draw();
   pad2->cd();
   pad2->SetBottomMargin(0.5);

   gRes->GetYaxis()->SetNdivisions(6);
   gRes->GetYaxis()->SetTitle("Residuals [mm]");
   gRes->GetXaxis()->SetTitle("First Component [a.u.]");

   gRes->SetMarkerStyle(5);

   gRes->SetTitle("");
   gRes->Draw("AP");

   c->Update();

   auto c1 = new TCanvas("c1", "Residuals", 800, 600);
   c1->SetGrid();
   auto h1 = new TH1D("h1", "Residuals Distribution for cubic fit", nbin, -30, 30);
   for (int i = 0; i < first_comp.size(); i++)
   {
      h1->Fill(res_array[i]);
   }
   auto f2 = new TF1("f2", "gaus");
   h1->Fit(f2, "LI");

   auto gRes2 = new TH1D("h2", "h2", nbin, -30, 30);

   for (Int_t i = 1; i <= nbin; i++)
   {
      Double_t diff = h1->GetBinContent(i) - f2->Eval(h1->GetBinCenter(i));
      gRes2->SetBinContent(i, diff);
   }

   TPad *pad12 = new TPad("pad12", "The pad 80 of the height", 0.0, 0.2, 1.0, 1.0);
   TPad *pad22 = new TPad("pad22", "The pad 20 of the height", 0.0, 0.0, 1.0, 0.25);
   pad12->Draw();
   pad22->Draw();
   pad12->SetGrid();
   pad22->SetGrid();
   pad12->cd();

   h1->GetYaxis()->SetTitle("Counts");

   h1->SetBinErrorOption(TH1::kPoisson);

   h1->SetMarkerColorAlpha(kBlack, 1);
   h1->Draw("E");
   f2->Draw("same");
   tp1->Draw();
   tp2->Draw();
   pad22->cd();
   pad22->SetBottomMargin(0.5);

   gRes2->GetYaxis()->SetNdivisions(6);
   gRes2->GetYaxis()->SetTitle("Residuals [mm]");
   gRes2->GetXaxis()->SetTitle(res_name);

   gStyle->SetOptStat(0);
   gRes2->SetMarkerColorAlpha(kBlack, 1);
   gRes2->SetTitle("");
   gRes2->Draw("E");

   c1->Update();

   auto c3 = new TCanvas("c3", canvas_name, 800, 600);
   c3->SetGrid();

   //auto g3 = new TGraph(m, f_c, v_z);
   auto f3 = new TF1("f3", "pol1");
   g3->Fit(f3,"EX0");
   auto gRes3 = new TGraph();
   auto res_array3 = new Double_t[first_comp.size()];

   for (int i = 0; i < first_comp.size(); i++)
   {
      auto y = f3->Eval(vert_z_m[i]);
      gRes3->SetPoint(i, vert_z_m[i], first_comp[i] - y);
      //res_array3[i] = first_comp[i] - y;
   }
   TF1 *fit = g3->GetFunction("f3");

    double p1 = fit->GetParameter(0);
    double p2 = fit->GetParameter(1);
   for (int i = 0; i < first_comp.size(); i++)
   {
    auto y = (first_comp[i]-p1)/p2;
    res_array3[i] = y-vert_z_m[i];
   }
   TPad *pad13 = new TPad("pad13", "The pad 80 of the height", 0.0, 0.2, 1.0, 1.0);
   TPad *pad23 = new TPad("pad23", "The pad 20 of the height", 0.0, 0.0, 1.0, 0.25);
   pad13->Draw();
   pad23->Draw();
   pad13->SetGrid();
   pad23->SetGrid();
   pad13->cd();

   g3->SetTitle(canvas_name);

   gRes3->GetXaxis()->SetTitle("mean vertex position [mm]");


   g3->SetMarkerStyle(5);

   g3->Draw("AP");
   f3->Draw("same");


   tp1->Draw();
   tp2->Draw();
   pad23->cd();
   pad23->SetBottomMargin(0.5);

   gRes3->GetYaxis()->SetNdivisions(6);
   gRes3->GetYaxis()->SetTitle("Residuals [mm]");
   g3->GetYaxis()->SetTitle("First Component [a.u.]");
   gRes3->SetMarkerStyle(5);
   gRes3->SetTitle("");
   gRes3->Draw("AP");

   c3->Update();
   auto c4 = new TCanvas("c4", "Residuals", 800, 600);
   c4->SetGrid();
   auto h4 = new TH1D("h4", "", nbin, -0.3,0.3);
   for (int i = 0; i < first_comp.size(); i++)
   {
      h4->Fill(res_array3[i]);
   }
   auto f4 = new TF1("f4", "gaus");
   h4->Fit(f4, "LI");

   auto gRes4 = new TH1D("gRes4", "gRes4", nbin, -0.3,0.3);

   for (Int_t i = 1; i <= nbin; i++)
   {
      Double_t diff = h4->GetBinContent(i) - f4->Eval(h4->GetBinCenter(i));
      gRes4->SetBinContent(i, diff);
   }

   TPad *pad124 = new TPad("pad12", "The pad 80 of the height", 0.0, 0.2, 1.0, 1.0);
   TPad *pad224 = new TPad("pad22", "The pad 20 of the height", 0.0, 0.0, 1.0, 0.25);
   pad124->Draw();
   pad224->Draw();
   pad124->SetGrid();
   pad224->SetGrid();
   pad124->cd();
   h4->SetTitle("Residual distribution for linear fit");
   h4->SetBinErrorOption(TH1::kPoisson);
   h4->GetYaxis()->SetTitle("Counts");
   h4->SetMarkerColorAlpha(kBlack, 1);

   h4->Draw("E");
   f4->Draw("same");
   tp1->Draw();
   tp2->Draw();
   pad224->cd();
   pad224->SetBottomMargin(0.5);
   gRes4->SetMarkerColorAlpha(kBlack, 1);
   //gRes4->SetBinErrorOption(TH1::kPoisson);
   gRes4->GetYaxis()->SetNdivisions(6);
   gRes4->GetYaxis()->SetTitle("Residuals [mm]");
   gRes4->GetXaxis()->SetTitle(res_name);

   gRes4->SetTitle("");
   gRes4->SetMarkerColorAlpha(kBlack, 1);
   gRes4->Draw("E");

   c4->Update();

/*
   auto c5 = new TCanvas("c5", canvas_name, 800, 600);
   c5->SetGrid();
   Double_t err_x[m];
   std::fill(err_x, err_x + m, 0);
   auto g5 = new TGraphErrors(m, f_c, v_z_m,err_x,v_z_s);
   auto f5 = new TF1("f5", "pol1");
   g5->Fit(f5);
   auto gRes5 = new TGraph();
   auto res_array5 = new Double_t[first_comp.size()];

   for (int i = 0; i < first_comp.size(); i++)
   {
      auto y = f5->Eval(first_comp[i]);
      gRes5->SetPoint(i, first_comp[i], (v_z_m[i] - y)/v_z_s[i]);
      res_array5[i] = v_z_m[i] - y;
   }

   TPad *pad15 = new TPad("pad13", "The pad 80 of the height", 0.0, 0.2, 1.0, 1.0);
   TPad *pad25 = new TPad("pad23", "The pad 20 of the height", 0.0, 0.0, 1.0, 0.25);
   pad15->Draw();
   pad25->Draw();
   pad15->SetGrid();
   pad25->SetGrid();
   pad15->cd();

   g5->SetTitle(canvas_name);

   g5->GetYaxis()->SetTitle("mean vertex position [mm]");


   g5->SetMarkerStyle(5);

   g5->Draw("AP");
   f5->Draw("same");


   tp1->Draw();
   tp2->Draw();
   pad25->cd();
   pad25->SetBottomMargin(0.5);

   gRes5->GetYaxis()->SetNdivisions(6);
   gRes5->GetYaxis()->SetTitle("Norm Residuals");
   gRes5->GetXaxis()->SetTitle("First Component [a.u.]");
   gRes5->SetMarkerStyle(5);
   gRes5->SetTitle("");
   gRes5->Draw("AP");

   c5->Update();
   auto c6 = new TCanvas("c6", "Residuals", 800, 600);
   c6->SetGrid();
   auto h6 = new TH1D("h6", "Residual distribution for linear fit", nbin, -0.15,0.15);
   for (int i = 0; i < first_comp.size(); i++)
   {
      h6->Fill(res_array5[i]);
   }
   auto f6 = new TF1("f6", "gaus");
   h6->Fit(f6, "LI");

   auto gRes6 = new TH1D("gRes6", "gRes6", nbin, -0.15,0.15);

   for (Int_t i = 1; i <= nbin; i++)
   {
      Double_t diff = h6->GetBinContent(i) - f6->Eval(h6->GetBinCenter(i));
      gRes4->SetBinContent(i, diff);
   }

   TPad *pad126 = new TPad("pad12", "The pad 80 of the height", 0.0, 0.2, 1.0, 1.0);
   TPad *pad226 = new TPad("pad22", "The pad 20 of the height", 0.0, 0.0, 1.0, 0.25);
   pad126->Draw();
   pad226->Draw();
   pad126->SetGrid();
   pad226->SetGrid();
   pad126->cd();
   h6->SetTitle("Residual distribution for linear fit");
   h6->SetBinErrorOption(TH1::kPoisson);
   h6->GetYaxis()->SetTitle("Counts");
   h6->SetMarkerColorAlpha(kBlack, 1);

   h6->Draw("E");
   f6->Draw("same");
   tp1->Draw();
   tp2->Draw();
   pad226->cd();
   pad226->SetBottomMargin(0.5);
   gRes6->SetMarkerColorAlpha(kBlack, 1);
   gRes6->SetBinErrorOption(TH1::kPoisson);
   gRes6->GetYaxis()->SetNdivisions(6);
   gRes6->GetYaxis()->SetTitle("Residuals [mm]");
   gRes6->GetXaxis()->SetTitle(res_name);

   gRes6->SetTitle("");
   gRes6->SetMarkerColorAlpha(kBlack, 1);
   gRes6->Draw("E");

   c6->Update();
   */
}

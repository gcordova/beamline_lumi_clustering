import numpy as np
import matplotlib.pyplot as plt
import mplhep


# Set the style of the plots
mplhep.style.use('LHCb2')
# Generate points according to a multivariate Gaussian distribution
mean = [0, 0, 0]
cov = [[10, 2, 4], [2, 5, 0], [4, 0, 1]]
num_points = 10000
points = np.random.multivariate_normal(mean, cov, num_points)

# Perform PCA on the dataset
from sklearn.decomposition import PCA
pca = PCA(n_components=3)
transformed_points = pca.fit_transform(points)

# Plot the points in a 3D plot using mplhep
fig = plt.figure(figsize=(12, 10))
ax = fig.add_subplot(111, projection='3d')
ax.scatter(points[:, 0], points[:, 1], points[:, 2], s=1, alpha=0.15)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

# Plot the principal components
for i in range(3):
    ax.plot([0, pca.explained_variance_[i]*pca.components_[i, 0]], [0, pca.explained_variance_[i]*pca.components_[i, 1]], [0, pca.explained_variance_[i ]*pca.components_[i, 2]], color='red')
    print(pca.explained_variance_[i])
plt.show()



# Plot the transformed dataset in a 2D plot using mplhep
fig, ax = plt.subplots(figsize=(12, 10))
ax.scatter(transformed_points[:, 0], transformed_points[:, 1],s=1)
#plot the principal components

ax.plot([0, pca.explained_variance_[0]], [0, 0], color='red')
ax.plot([0, 0], [0, pca.explained_variance_[1]], color='red')
ax.set_xlabel('Frist Principal Component')
ax.set_ylabel('Second Principal Component')
ax.set_xlim(-15, 15)
ax.set_ylim(-15, 15)

plt.show()
import yaml

import matplotlib.pyplot as plt

import mplhep as hep

# Load the YAML file
path = "../../beamline_estimation/config/"
variable = "z"
velo_side = ""
path_velo = ""
if velo_side != "":
    path_velo = "_VELO_" + velo_side
stream_x = open(path+"pca_"+variable+path_velo+".yaml", "r", encoding="utf-8")
config = yaml.safe_load(stream_x)

# Extract the gEigenValues
gEigenValues = config['gEigenValues']

# Plot only the first 10 components
gEigenValues_first_10 = gEigenValues[:15]

# Set the style of LHCb
plt.style.use(hep.style.LHCb2)

# Create a bar plot
plt.figure(figsize=(10, 6))
plt.bar(range(len(gEigenValues_first_10)), gEigenValues_first_10, color='green')
plt.xlabel('Number of PC')
plt.ylabel('% Variance Explained')
plt.xticks(range(len(gEigenValues_first_10)))
name_plot = 'Bar Plot of First 10 Eigenvalues for '+variable
if velo_side != "":
    name_plot += ' VELO '+velo_side
plt.title(name_plot)

# Save the plot
plt.savefig('bar_plot_gEigenValues_'+variable+path_velo+'.pdf')

plt.show()


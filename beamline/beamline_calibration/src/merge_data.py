"""
Script for merging data from DPS of TEll40

VADAQTELL40:Velo_TELL40_Mij_*_k.luminosity_rates.inst_lumi_bb

The script is called via

python3 merge_data.py

"""
#c
import os
from os.path import exists
import sys
from functools import reduce
from pathlib import Path
import numpy as np
import pandas as pd
from pandas.errors import EmptyDataError
import matplotlib.pyplot as plt
import yaml
import seaborn as sns
import colorcet as cc


def get_data(path, count_type, config_dict,yearfirst=False):
    """
    Load data from a CSV file, format it, and return a DataFrame.

    Args:
        path (str): The path to the CSV file.

    Returns:
        pd.DataFrame: The formatted DataFrame.
    """
    str_year = config_dict["foldername"]
    _, tail = os.path.split(path)
    root, _ = os.path.splitext(tail)
    df = pd.read_csv(path)
    if yearfirst:
        df["TS"] = pd.to_datetime(df["TS"], format="%Y-%m-%d %H:%M:%S.%f")
    else:
        df["TS"] = pd.to_datetime(df["TS"], format="%d-%m-%Y %H:%M:%S.%f")
    df["TS"] = df["TS"].dt.floor("s")
    df = df.drop(columns=[" DPE"])
    newname = root.replace("output_", "")
    count_type = count_type.replace("../../../" + str_year + "/input_data/", "")
    count_type = count_type.replace("_inner", "")
    if newname.startswith("M"):
        newname = newname + count_type
    df = df.rename(columns={" VALUE": newname})
    return df


def create_file_list(config_dict, count_type, counter):
    """
    Create a list of DataFrames to merge. Handle B4BB1E, infinity, and empty rows.

    Args:
        config_dict (dict): Configuration dictionary.
        count_type (str): Type of count ("bb" or "bb_inner").
        counter (str): Name of module (e.g. M23_0)

    Returns:
        list: List of DataFrames to merge.
    """
    start_time = config_dict["StartOfVdm"]
    end_time = config_dict["EndOfCalib"]
    year = config_dict["year"]
    str_year = config_dict["foldername"]
    
    count_type_bb = "../../../" + str_year + "/input_data/" + count_type
    count_type_eb = "../../../" + str_year + "/input_data/eb" + count_type[2:]
    count_type_be = "../../../" + str_year + "/input_data/be" + count_type[2:]
    count_type_ee = "../../../" + str_year + "/input_data/ee" + count_type[2:]
    #print(count_type)
    if year == "2022":
        path_to_search = [count_type_bb]
    else:
        path_to_search = [count_type_bb, count_type_be, count_type_eb, count_type_ee]

    file_list = []
    for path_to_folder in path_to_search:
        for root_folder, _, files in os.walk(path_to_folder):
            for file in files:
                if file == "output_" + counter + ".csv":
                    try:
                        _, tail = os.path.split(path_to_folder)
                        root, _ = os.path.splitext(tail)
                        full_path = os.path.join(root_folder, file)
                        element = get_data(str(full_path), root, config_dict)
                        element = element.loc[element["TS"] >= start_time]
                        element = element.loc[element["TS"] <= end_time]
                        numeric_columns = element.select_dtypes(
                            include=["int64", "float64"]
                        )
                        #print(element)
                        for col in numeric_columns:
                            element = element.loc[
                                element[col] != 9.9900000000000004e125
                            ]
                            element = element.loc[element[col] != 1.2237598e7]
                            element = element.loc[element[col] != 7.64849875e5]
                            element = element.loc[element[col] < 1.64849875e5]
                            if (len(element[col]) > 100 and not np.isnan(  # type: ignore              #element.loc[:, col].mean() != 0
                                element.loc[:, col].mean())):
                                if year == "2022":
                                    element[col] = element[col] / 247400.6 
                                element = (
                                    element.drop_duplicates("TS")
                                    .set_index("TS")
                                    .resample("3s")
                                    .ffill()
                                    .reset_index()
                                )
                                file_list.append(element)
                                print(
                                    "File successfully formatted and inserted into the list"
                                )
                            else:
                                print("Too many B4BB1E. File will not be processed")
                    except EmptyDataError:
                        print(f"No columns to parse from file {str(file)}")
                        print("File will not be processed")
                    except ValueError:
                        print(f"ValueError in file {str(file)}")
    return file_list


def merge_dataset(config_dict, input_file):
    """
    Merge the CSV data of each TELL40 into one DataFrame with a unique timestamp, which will be set as the index.

    Args:
        config_dict (dict): Configuration dictionary.
        input_file (str): Type of input file (e.g., "bb" or "beam").
        year (str): year of calibration (e.g. "2022" or "2023")

    Returns:
        pd.DataFrame: Merged DataFrame.
    """

    #define an empty list where to store all the csv counters
    df_list = []
    #define year
    year = config_dict["year"]
    #get list of counters from configuration file
    for counter in config_dict["counters_name"]:
        if counter == "M32_0" and input_file == "bb" and year == "2022":
            print(f"Counter {str(counter)} corrupted, skipping...")
            continue
        if counter == "M24_1" and input_file == "bb" and year == "2022":
            print(f"Counter {str(counter)} corrupted, skipping...")
            continue
        print(f"Processing {str(counter)} counters")
        #create a list with the bxtype counters for a module (in counters list you find bb, eb, be ee for a single counter)
        counters_list = create_file_list(config_dict, input_file, counter)
        #if the list is empty (no data for counters were found) skip loop
        if not counters_list:
            continue
            #merge the different type of counters in a unique timestamp
        df_merged_bb = reduce(
                lambda left, right: pd.merge_asof(
                    left,
                    right,
                    on="TS",
                    direction="nearest",
                    tolerance=pd.Timedelta("0.5s"),
                ),
                counters_list,
            )
        #elif year == "2022":
        #    df_merged_bb = counters_list[0]
        #define a list with the existing counters
        var_list = [col for col in df_merged_bb.columns if col.startswith("M")]
        for el in var_list:
            if year == "2022":
                #subtract background
                if input_file == "bb":
                   col_name = f"{el[:-2]}_outer"
                   new_col_name = f"Delta_{el[:-2]}_outer"
                elif input_file == "bb_inner":
                   col_name = f"{el[:-2]}_inner"
                   new_col_name = f"Delta_{el[:-2]}_inner"   
                df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                )
                #define error
                df_merged_bb[new_col_name] = np.sqrt(
                        df_merged_bb[el[:-2] + "bb"]/247400.6 
                    )
            elif input_file == "bb_inner":
                #subtract background
                col_name = f"{el[:-2]}_inner"
                if year == "2023":
                    df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                    - df_merged_bb[el[:-2] + "be"]
                    - 16 * df_merged_bb[el[:-2] + "ee"]
                    )
                else:
                    df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                    - df_merged_bb[el[:-2] + "be"]
                    - df_merged_bb[el[:-2] + "eb"]
                    + df_merged_bb[el[:-2] + "ee"]
                    )
                #define error
                new_col_name = f"Delta_{el[:-2]}_inner"
                #function calculated with monte carlo
                if year == "2023":
                    df_merged_bb[new_col_name] = 0.000709511 * np.sqrt(
                    0.955167
                    * (
                        df_merged_bb[el[:-2] + "bb"]
                        + df_merged_bb[el[:-2] + "be"]
                        + 16 * df_merged_bb[el[:-2] + "ee"]
                    )
                    )
                else:
                    df_merged_bb[new_col_name] = 0.000709511 * np.sqrt(
                    0.955167
                    * (
                        df_merged_bb[el[:-2] + "bb"]
                        + df_merged_bb[el[:-2] + "be"]
                        + df_merged_bb[el[:-2] + "eb"]
                        + df_merged_bb[el[:-2] + "ee"]
                    )
                )
            elif input_file == "bb":
                #subtract background
                col_name = f"{el[:-2]}_outer"
                df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                    - df_merged_bb[el[:-2] + "be"]
                    - df_merged_bb[el[:-2] + "eb"]
                    + df_merged_bb[el[:-2] + "ee"]
                )
                #define error
                new_col_name = f"Delta_{el[:-2]}_outer"
                df_merged_bb[new_col_name] = 0.000709511 * np.sqrt(
                    0.955167
                    * (
                        df_merged_bb[el[:-2] + "bb"]
                        + df_merged_bb[el[:-2] + "be"]
                        + df_merged_bb[el[:-2] + "eb"]
                        + df_merged_bb[el[:-2] + "ee"]
                    )
                )
        #define columns to remove: every type counters
        col_to_remove = [
            col
            for col in df_merged_bb.columns
            if (
                col.endswith("bb")
                or col.endswith("be")
                or col.endswith("eb")
                or col.endswith("ee")
            )
        ]
        #drop the columns defined above
        df_merged_bb.drop(columns=col_to_remove, inplace=True)

        df_list.append(df_merged_bb)

        #df_merged_bb.to_csv("merged_counters/merged_" + counter + ".csv")
    #merge all counters in a single csv
    df_merged_unique = reduce(
        lambda left, right: pd.merge_asof(
            left,
            right,
            on="TS",
            direction="nearest",
            tolerance=pd.Timedelta("0.5s"),
        ),
        df_list,
    )
    df_merged_unique.set_index("TS", inplace=True)
    return df_merged_unique





def main():
    """
    Main body of the script.
    """
    #open and read configuration file. It takes informations such as the year of the calibration, 
    #the start and end of the calibration, the display of the figure, the counters to be used,
    #the bunch population, the frequency of lhc, the number of colliding bunches
    stream = open("config.yaml", "r", encoding="utf-8")
    config_dictionary = yaml.safe_load(stream)
    #set options according to configuration file
    str_year=config_dictionary["foldername"]
    #get year of calibration from config file, search for data in the right folder
    
    #if the dataset is not already create, merge all the counters in a csv file and save it
    if not exists("../../../" + str_year + "/input_data/data_merged.csv"):
        #merge all the bb counters in a single csv 
        df_merged_bb = merge_dataset(config_dictionary, "bb")
        #merge all the bb_inner counters in a single csv
        df_merged_bb_inner =  merge_dataset(config_dictionary, "bb_inner")
        #merge the bb and bb_inner counters in a single csv
        df_merged_unique = pd.merge_asof(df_merged_bb,
        df_merged_bb_inner,
        left_index = True, right_index = True,
        direction="nearest",
        tolerance=pd.Timedelta("0.5s"),
        )
        #select only the data in the calibration period
        mask = (df_merged_unique.index> config_dictionary["StartOfCalib"]) & (df_merged_unique.index <= config_dictionary["EndOfCalib"])
        df_merged_unique = df_merged_unique.loc[mask]
        #from_ts = "2022-11-10 10:29:45" #2022-11-10 10:29:45
        #to_ts = "2022-11-10 10:32:00" #2022-11-10 10:32:00
        #df_merged_unique = df_merged_unique.loc[
        #    (df_merged_unique.index < from_ts) | (df_merged_unique.index > to_ts)
        #]       
        #save the merged dataset
        df_merged_unique.to_csv(
            "../../../" + str_year + "/input_data/data_merged.csv"
        )

#

if __name__ == "__main__":
    main()

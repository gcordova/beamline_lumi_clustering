import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import yaml

stream = open("../../beamline_estimation/config/pca_x_2022.yaml", "r", encoding="utf-8")
config_dictionary_pca = yaml.safe_load(stream)

df = pd.read_csv('../../../data_vdm_2022/output_data/df_counters_wo_offset.csv')

bin_names = config_dictionary_pca["all_columns"]
#bin_names = [w.replace("A", "O") for w in bin_names]
#bin_names = [w.replace("B", "I") for w in bin_names]
bin_names = ["M" + w for w in bin_names]
all_columns = bin_names
print(all_columns)
df['TS'] = pd.to_datetime(df['TS'], format='%Y-%m-%d %H:%M:%S')
df.set_index('TS', inplace=True)
df = df.between_time("10:24:00","10:38:15") #"10:07:01","10:21:54" #"10:07:33","10:20:15"

df_same = df[abs(df["Nominal_Displacement_B1_sepPlane"]-df["Nominal_Displacement_B2_sepPlane"])<0.001]

counters = [tmp for tmp in df_same.columns if tmp.startswith("M")]
df_counters = df_same[counters]
df_lumi = df_same["lumi"]
df_counters = df_counters.div(df_lumi, axis=0)
df_counters['Nominal_Displacement_B1_sepPlane'] = df_same['Nominal_Displacement_B1_sepPlane']
df_counters['lumi'] = df_same['lumi']
df_same = df_counters 
df_same_grouped_mean = df_same.groupby("Nominal_Displacement_B1_sepPlane").mean()
df_same_grouped_std = df_same.groupby("Nominal_Displacement_B1_sepPlane").std()
df_counters = df_same_grouped_mean[counters] #occhio qui hai cambiato mean con std
df_selected = df_counters[df_counters.index.to_series().abs() > 0.4]
df_selected_err = np.sqrt(df_selected/247400.6)
df_selected_std = df_same_grouped_std[counters]
df_selected_std = df_selected_std[df_selected_std.index.to_series().abs() > 0.4]
df_selected = df_selected.rename(columns=lambda x: x.replace('_outer', 'A').replace('_inner', 'B'))
df_selected = df_selected.reindex(sorted(df_selected.columns), axis=1)
df_selected_std = df_selected_std.rename(columns=lambda x: x.replace('_outer', 'A').replace('_inner', 'B'))
df_selected_std = df_selected_std.reindex(sorted(df_selected_std.columns), axis=1)
df_selected_err = df_selected_err.rename(columns=lambda x: x.replace('_outer', 'A').replace('_inner', 'B'))
df_selected_err = df_selected_err.reindex(sorted(df_selected_err.columns), axis=1)
MC_05 = [0.0719, 0.2011, 0.07785, 0.2034, 0.076, 0.2167, 0.08195, 0.2259, 0.0704, 0.2074, 0.07725, 0.2067, 0.07285, 0.228, 0.2286, 0.2021, 0.0777, 0.07065, 0.2137, 0.0771, 0.2208, 0.2029, 0.07125, 0.2132, 0.0748, 0.2183, 0.06745, 0.0721, 0.2155, 0.06005, 0.2069, 0.0618, 0.2058, 0.05755, 0.1799, 0.05525, 0.1835, 0.05925, 0.2029, 0.0601, 0.1922, 0.05465, 0.177, 0.181, 0.05755, 0.2006, 0.1974, 0.0535, 0.1779, 0.05735, 0.1777, 0.0599, 0.1938, 0.05745, 0.1925, 0.0571, 0.1832, 0.05735, 0.1888, 0.062, 0.207, 0.06375, 0.2009, 0.05985, 0.1889, 0.05935, 0.1897, 0.0719, 0.2057, 0.2109, 0.0624, 0.1953, 0.05975, 0.1971, 0.068, 0.2161, 0.06815, 0.2221, 0.0683, 0.1988, 0.2076, 0.0714, 0.2225, 0.06795, 0.2062, 0.0721, 0.2066, 0.0787, 0.228, 0.0728, 0.2334, 0.0704, 0.2035, 0.07655, 0.2135, 0.0772, 0.2308, 0.2306, 0.0792, 0.2084, 0.07605, 0.2165, 0.0818, 0.2303, 0.07915, 0.2119, 0.0807, 0.2176, 0.2276, 0.07435, 0.2128, 0.07825, 0.2055, 0.07825, 0.2122, 0.216, 0.08265, 0.2276, 0.0869, 0.2415, 0.0784, 0.2076, 0.2072, 0.0807, 0.2255, 0.2273, 0.07505, 0.1978, 0.079, 0.2076, 0.0792, 0.2134, 0.0812, 0.2204, 0.0723, 0.1972, 0.0764, 0.2042, 0.2064, 0.074, 0.2006, 0.2076, 0.07805, 0.2058, 0.0821, 0.2154, 0.073, 0.192, 0.08435, 0.2061, 0.0746, 0.2053, 0.08395, 0.2138, 0.07495, 0.1942, 0.2036, 0.0771, 0.2006, 0.2182]
MC_n05 = [0.07685, 0.2225, 0.07915, 0.2254, 0.07125, 0.2021, 0.07515, 0.2024, 0.07175, 0.2199, 0.0807, 0.2247, 0.06785, 0.1962, 0.2052, 0.2216, 0.08275, 0.0682, 0.1998, 0.073, 0.1968, 0.2178, 0.06665, 0.191, 0.0729, 0.1971, 0.06475, 0.0677, 0.1942, 0.0575, 0.1865, 0.0597, 0.186, 0.0585, 0.196, 0.06075, 0.2095, 0.0559, 0.1802, 0.0534, 0.1799, 0.05885, 0.1947, 0.1949, 0.05615, 0.1711, 0.1749, 0.06035, 0.1973, 0.06175, 0.198, 0.05575, 0.1722, 0.05695, 0.1731, 0.06015, 0.2052, 0.0615, 0.2062, 0.0577, 0.1845, 0.05445, 0.1852, 0.06495, 0.2124, 0.0624, 0.2077, 0.06275, 0.1895, 0.1908, 0.0697, 0.2114, 0.06715, 0.2183, 0.06595, 0.1941, 0.06375, 0.196, 0.06875, 0.2196, 0.2219, 0.0659, 0.2011, 0.07275, 0.2298, 0.0757, 0.2287, 0.0721, 0.2014, 0.07055, 0.2102, 0.0761, 0.2261, 0.07865, 0.2289, 0.0742, 0.2038, 0.212, 0.0781, 0.2321, 0.08155, 0.2345, 0.0745, 0.2046, 0.0791, 0.233, 0.0767, 0.234, 0.2072, 0.08045, 0.2308, 0.07985, 0.2386, 0.0836, 0.2227, 0.2366, 0.0766, 0.2107, 0.078, 0.2076, 0.0827, 0.2309, 0.2319, 0.08085, 0.2016, 0.2034, 0.08075, 0.2178, 0.08555, 0.236, 0.0754, 0.1946, 0.07635, 0.2058, 0.075, 0.2173, 0.0796, 0.2281, 0.2293, 0.076, 0.2152, 0.2253, 0.07125, 0.1807, 0.0755, 0.1981, 0.07645, 0.2116, 0.09025, 0.2213, 0.0712, 0.185, 0.0769, 0.1958, 0.079, 0.2139, 0.2259, 0.07285, 0.1844, 0.1984]
MC_dict = dict(zip(df_selected.columns,MC_05))
MC_n_dict = dict(zip(df_selected.columns,MC_n05))

for column in bin_names:
    if column not in df_selected.columns:
        df_selected[column] = np.nan
        MC_dict[column] = np.nan
        MC_n_dict[column] = np.nan
        df_selected_std[column] = np.nan
        df_selected_err[column] = np.nan
df_selected = df_selected.reindex(sorted(df_selected.columns), axis=1)
df_selected_std = df_selected_std.reindex(sorted(df_selected_std.columns), axis=1)
df_selected_err = df_selected_err.reindex(sorted(df_selected_err.columns), axis=1)
MC_dict = dict(sorted(MC_dict.items()))
MC_n_dict = dict(sorted(MC_n_dict.items()))
MC_05 = list(MC_dict.values())
MC_n05 =list( MC_n_dict.values())
df_selected = df_selected.rename(columns=lambda x: x.replace('A', 'O').replace('B', 'I'))
df_selected_std = df_selected_std.rename(columns=lambda x: x.replace('A', 'O').replace('B', 'I'))
df_selected_err = df_selected_err.rename(columns=lambda x: x.replace('A', 'O').replace('B', 'I'))
bin_names = list(df_selected.columns)
bin_names = [temp[-1:] + temp[:-1] for temp in bin_names] #cosa fa questa roba????
list1, list3 = df_selected.iloc[0].tolist(), df_selected.iloc[1].tolist()#, df_selected.iloc[2].tolist()
list1_std, list3_std = df_selected_std.iloc[0].tolist(), df_selected_std.iloc[1].tolist()#, df_selected_std.iloc[2].tolist()
list1_err, list3_err = df_selected_err.iloc[0].tolist(), df_selected_err.iloc[1].tolist()#, df_selected_err.iloc[2].tolist()
# Separate the bin names and content into different categories
print(len(bin_names))
bin_names_down = []
bin_names_up = []
bin_names_left = []
bin_names_right = []
bin_content1_down = []
bin_content1_up = []
bin_content1_left = []
bin_content1_right = []
bin_content2_down = []
bin_content2_up = []
bin_content2_left = []
bin_content2_right = []
bin_content3_down = []
bin_content3_up = []
bin_content3_left = []
bin_content3_right = []
bin_content1_std_down = []
bin_content1_std_up = []
bin_content1_std_left = []
bin_content1_std_right = []
bin_content3_std_down = []
bin_content3_std_up = []
bin_content3_std_left = []
bin_content3_std_right = []
bin_content1_err_down = []
bin_content1_err_up = []
bin_content1_err_left = []
bin_content1_err_right = []
bin_content3_err_down = []
bin_content3_err_up = []
bin_content3_err_left = []
bin_content3_err_right = []

MC_05_up = []
MC_05_down = []
MC_05_left = []
MC_05_right = []
MC_n05_up = []
MC_n05_down = []

MC_n05_left = []
MC_n05_right = []



# Organize bin names and content into their respective categories
for j in range(0, 208, 8):
    bin_names_down.append(bin_names[j])
    bin_content1_down.append(list1[j])
    bin_content1_std_down.append(list1_std[j])
    bin_content1_err_down.append(list1_err[j])
    bin_names_down.append(bin_names[j + 1])
    bin_content1_down.append(list1[j + 1])
    bin_content1_std_down.append(list1_std[j+1])
    bin_content1_err_down.append(list1_err[j+1])
    bin_names_left.append(bin_names[j + 2])
    bin_content1_left.append(list1[j + 2])
    bin_content1_std_left.append(list1_std[j+2])
    bin_content1_err_left.append(list1_err[j+2])
    bin_names_left.append(bin_names[j + 3])
    bin_content1_left.append(list1[j + 3])
    bin_content1_std_left.append(list1_std[j+3])
    bin_content1_err_left.append(list1_err[j+3])
    bin_names_up.append(bin_names[j + 4])
    bin_content1_up.append(list1[j + 4])
    bin_content1_std_up.append(list1_std[j+4])
    bin_content1_err_up.append(list1_err[j+4])
    bin_names_up.append(bin_names[j + 5])
    bin_content1_up.append(list1[j + 5])
    bin_content1_std_up.append(list1_std[j+5])
    bin_content1_err_up.append(list1_err[j+5])
    bin_names_right.append(bin_names[j + 6])
    bin_content1_right.append(list1[j + 6])
    bin_content1_std_right.append(list1_std[j+6])
    bin_content1_err_right.append(list1_err[j+6])
    bin_names_right.append(bin_names[j + 7])
    bin_content1_right.append(list1[j + 7])
    bin_content1_std_right.append(list1_std[j+7])
    bin_content1_err_right.append(list1_err[j+7])
    #bin_content2_down.append(list2[j])
    #bin_content2_down.append(list2[j + 1])
    #bin_content2_left.append(list2[j + 2])
    #bin_content2_left.append(list2[j + 3])
    #bin_content2_up.append(list2[j + 4])
    #bin_content2_up.append(list2[j + 5])
    #bin_content2_right.append(list2[j + 6])
    #bin_content2_right.append(list2[j + 7])
    bin_content3_down.append(list3[j])
    bin_content3_down.append(list3[j + 1])
    bin_content3_left.append(list3[j + 2])
    bin_content3_left.append(list3[j + 3])
    bin_content3_up.append(list3[j + 4])
    bin_content3_up.append(list3[j + 5])
    bin_content3_right.append(list3[j + 6])
    bin_content3_right.append(list3[j + 7])
    bin_content3_std_down.append(list3_std[j])
    bin_content3_std_down.append(list3_std[j + 1])
    bin_content3_std_left.append(list3_std[j + 2])
    bin_content3_std_left.append(list3_std[j + 3])
    bin_content3_std_up.append(list3_std[j + 4])
    bin_content3_std_up.append(list3_std[j + 5])
    bin_content3_std_right.append(list3_std[j + 6])
    bin_content3_std_right.append(list3_std[j + 7])
    bin_content3_err_down.append(list3_err[j])
    bin_content3_err_down.append(list3_err[j + 1])
    bin_content3_err_left.append(list3_err[j + 2])
    bin_content3_err_left.append(list3_err[j + 3])
    bin_content3_err_up.append(list3_err[j + 4])
    bin_content3_err_up.append(list3_err[j + 5])
    bin_content3_err_right.append(list3_err[j + 6])
    bin_content3_err_right.append(list3_err[j + 7])
    MC_05_down.append(MC_05[j])
    MC_05_down.append(MC_05[j + 1])
    MC_05_left.append(MC_05[j + 2])
    MC_05_left.append(MC_05[j + 3])
    MC_05_up.append(MC_05[j + 4])
    MC_05_up.append(MC_05[j + 5])
    MC_05_right.append(MC_05[j + 6])
    MC_05_right.append(MC_05[j + 7])
    MC_n05_down.append(MC_n05[j])
    MC_n05_down.append(MC_n05[j + 1])
    MC_n05_left.append(MC_n05[j + 2])
    MC_n05_left.append(MC_n05[j + 3])
    MC_n05_up.append(MC_n05[j + 4])
    MC_n05_up.append(MC_n05[j + 5])
    MC_n05_right.append(MC_n05[j + 6])
    MC_n05_right.append(MC_n05[j + 7])

bin_content1_down = np.array(bin_content1_down)
bin_content1_up = np.array(bin_content1_up)
bin_content1_left = np.array(bin_content1_left)
bin_content1_right = np.array(bin_content1_right)
bin_content3_down = np.array(bin_content3_down)
bin_content3_up = np.array(bin_content3_up)
bin_content3_left = np.array(bin_content3_left)
bin_content3_right = np.array(bin_content3_right)
bin_content1_std_down = np.array(bin_content1_std_down)
bin_content1_std_up = np.array(bin_content1_std_up)
bin_content1_std_left = np.array(bin_content1_std_left)
bin_content1_std_right = np.array(bin_content1_std_right)
bin_content3_std_down = np.array(bin_content3_std_down)
bin_content3_std_up = np.array(bin_content3_std_up)
bin_content3_std_left = np.array(bin_content3_std_left)
bin_content3_std_right = np.array(bin_content3_std_right)

MC_05_down = np.array(MC_05_down)
MC_05_up = np.array(MC_05_up)
MC_05_left = np.array(MC_05_left)
MC_05_right = np.array(MC_05_right)
MC_n05_down = np.array(MC_n05_down)
MC_n05_up = np.array(MC_n05_up)
MC_n05_left = np.array(MC_n05_left)
MC_n05_right = np.array(MC_n05_right)

ratio_pos_down = MC_05_down/bin_content3_down
ratio_pos_up = MC_05_up/bin_content3_up
ratio_pos_left = MC_05_left/bin_content3_left
ratio_pos_right = MC_05_right/bin_content3_right
ratio_neg_down = MC_n05_down/bin_content1_down
ratio_neg_up = MC_n05_up/bin_content1_up
ratio_neg_left = MC_n05_left/bin_content1_left
ratio_neg_right = MC_n05_right/bin_content1_right

std_ratio_pos_down = bin_content3_std_down/bin_content3_down
std_ratio_pos_up = bin_content3_std_up/bin_content3_up
std_ratio_pos_left = bin_content1_std_left/bin_content3_left
std_ratio_pos_right = bin_content1_std_right/bin_content3_right
std_ratio_neg_down = bin_content1_std_down/bin_content1_down
std_ratio_neg_up = bin_content1_std_up/bin_content1_up
std_ratio_neg_left = bin_content1_std_left/bin_content1_left
std_ratio_neg_right = bin_content1_std_right/bin_content1_right


bin_names_all = [bin_names_down, bin_names_left, bin_names_up, bin_names_right] 
bin_contents_all = [bin_content1_down, bin_content1_left, bin_content1_up, bin_content1_right]
ratio_all = [ratio_neg_down, ratio_neg_left, ratio_neg_up, ratio_neg_right]
std_all = [bin_content1_std_down, bin_content1_std_left, bin_content1_std_up, bin_content1_std_right]
bin_contents_all = np.array(bin_contents_all)
bin_names_all = np.array(bin_names_all)
ratio_all = np.array(ratio_all)
std_all = np.array(std_all)
#print(std_all)
mask = (ratio_all < 300)*(ratio_all > 100)
#mask = (std_all > 0.00002)*(std_all < 0.0001)

good_counters_ratio = bin_names_all[mask]
#good_counters_ratio = good_counters_ratio[ratio_all >100]

good_counters = []
for j in range(0,52):
    if (mask[0][j] == True and mask[1][j] == True and mask[2][j] == True and mask[3][j] == True):
        good_counters.append(bin_names_all[0][j])
        good_counters.append(bin_names_all[1][j])
        good_counters.append(bin_names_all[2][j])
        good_counters.append(bin_names_all[3][j])
    else:
        ratio_pos_down[j] = np.nan
        ratio_pos_up[j] = np.nan
        ratio_pos_left[j] = np.nan
        ratio_pos_right[j] = np.nan
        ratio_neg_down[j] = np.nan
        ratio_neg_up[j] = np.nan
        ratio_neg_left[j] = np.nan
        ratio_neg_right[j] = np.nan
        std_ratio_pos_down[j] = np.nan
        std_ratio_pos_up[j] = np.nan
        std_ratio_pos_left[j] = np.nan
        std_ratio_pos_right[j] = np.nan
        std_ratio_neg_down[j] = np.nan
        std_ratio_neg_up[j] = np.nan
        std_ratio_neg_left[j] = np.nan
        std_ratio_neg_right[j] = np.nan
        bin_content1_std_down[j] = np.nan
        bin_content1_std_up[j] = np.nan
        bin_content1_std_left[j] = np.nan
        bin_content1_std_right[j] = np.nan
        bin_content3_std_down[j] = np.nan
        bin_content3_std_up[j] = np.nan
        bin_content3_std_left[j] = np.nan
        bin_content3_std_right[j] = np.nan
        bin_content1_err_down[j] = np.nan
        bin_content1_err_up[j] = np.nan
        bin_content1_err_left[j] = np.nan
        bin_content1_err_right[j] = np.nan
        bin_content3_err_down[j] = np.nan
        bin_content3_err_up[j] = np.nan
        bin_content3_err_left[j] = np.nan
        bin_content3_err_right[j] = np.nan



good_counters = [counter[1:] + counter[:1] for counter in good_counters]
good_counters = [counter.replace('O', 'A').replace('I', 'B') for counter in good_counters]
good_counters.sort()
good_counters = [counter.replace('A', '_outer').replace('B', '_inner') for counter in good_counters]

good_counters_ratio = [counter[1:] + counter[:1] for counter in good_counters_ratio]
good_counters_ratio = [counter.replace('O', 'A').replace('I', 'B') for counter in good_counters_ratio]
good_counters_ratio.sort()
good_counters_ratio = [counter.replace('A', '_outer').replace('B', '_inner') for counter in good_counters_ratio]

all_columns = [counter.replace('A', '_outer').replace('B', '_inner') for counter in all_columns]


print(len(good_counters))
#print(good_counters_ratio)
tmp = good_counters_ratio
tmp= ['M00_0_outer', 'M00_0_inner', 'M00_1_outer', 'M00_1_inner', 'M01_0_outer', 'M01_0_inner', 'M01_1_outer', 'M01_1_inner']
tmp = all_columns
formatted_list = []
check_list = []

# Ciclo sugli indici
for i in range(0,26):
    '''
    if(f"M{2*i:02d}_0_outer" in tmp):
        formatted_list.append(f"numclusterod{i}")
        check_list.append(f"M{2*i:02d}_0_outer")
    if(f"M{2*i:02d}_0_inner" in tmp):
        formatted_list.append(f"numclusterid{i}")
        check_list.append(f"M{2*i:02d}_0_inner")
    
    if(f"M{2*i:02d}_1_outer" in tmp):
        formatted_list.append(f"numclusterol{i}")
        check_list.append(f"M{2*i:02d}_1_outer")
    if(f"M{2*i:02d}_1_inner" in tmp): 
        formatted_list.append(f"numclusteril{i}")
        check_list.append(f"M{2*i:02d}_1_inner")
    '''
    if(f"M{2*i+1:02d}_0_outer" in tmp):
        formatted_list.append(f"numclusterou{i}")
        check_list.append(f"M{2*i+1:02d}_0_outer")
    if(f"M{2*i+1:02d}_0_inner" in tmp):
        formatted_list.append(f"numclusteriu{i}")
        check_list.append(f"M{2*i+1:02d}_0_inner")
    
    if(f"M{2*i+1:02d}_1_outer" in tmp):
        formatted_list.append(f"numclusteror{i}")
        check_list.append(f"M{2*i+1:02d}_1_outer")
    if(f"M{2*i+1:02d}_1_inner" in tmp):
        formatted_list.append(f"numclusterir{i}")
        check_list.append(f"M{2*i+1:02d}_1_inner")
    


formatted_output = '\n'.join(formatted_list)
print(len(formatted_list))
print(formatted_list)
array_in_mc = ["numclusterod0", "numclusterid0", "numclusterol0", "numclusteril0", "numclusterou0", "numclusteriu0", "numclusteror0", "numclusterir0", "numclusterod1", "numclusterid1", "numclusterol1", "numclusteril1", "numclusterou1", "numclusteriu1", "numclusterir1", "numclusterid2", "numclusterol2", "numclusterou2", "numclusteriu2", "numclusteror2", "numclusterir2", "numclusterid3", "numclusterou3", "numclusterir3", "numclusterou4", "numclusteror4", "numclusterir4", "numclusteriu5", "numclusteror5", "numclusterod6", "numclusterid6", "numclusterol6", "numclusteril6", "numclusterou6", "numclusteriu6", "numclusteror6", "numclusterir6", "numclusterod7", "numclusterid7", "numclusteril7", "numclusterou7", "numclusterir7", "numclusterod8", "numclusterid8", "numclusterol8", "numclusteril8", "numclusterou8", "numclusteror8", "numclusterir8", "numclusterod9", "numclusterid9", "numclusterol9", "numclusteril9", "numclusterou9", "numclusteriu9", "numclusteror9", "numclusterir9", "numclusterod10", "numclusterid10", "numclusterol10", "numclusteril10", "numclusterou10", "numclusteriu10", "numclusterir10", "numclusterod11", "numclusterid11", "numclusterol11", "numclusteril11", "numclusterou11", "numclusterir11", "numclusterod12", "numclusterid12", "numclusteril12", "numclusteriu12", "numclusterod13", "numclusterid13", "numclusterol13", "numclusteril13", "numclusterou13", "numclusteriu13", "numclusteror13", "numclusterod14", "numclusterid14", "numclusterol14", "numclusteril14", "numclusterou14", "numclusteriu14", "numclusterod15", "numclusterid15", "numclusterol15", "numclusteril15", "numclusterou15", "numclusteriu15", "numclusteror15", "numclusterid16", "numclusterol16", "numclusteril16", "numclusterir16", "numclusterod17", "numclusterid17", "numclusterol17", "numclusteril17", "numclusterod18", "numclusterid18", "numclusteril18", "numclusterou18", "numclusteror18", "numclusterir18", "numclusterod19", "numclusterid19", "numclusteril19", "numclusteriu19", "numclusterir19", "numclusterod20", "numclusterid20", "numclusterol20", "numclusteril20", "numclusteriu20", "numclusteror20", "numclusterir20", "numclusterod21", "numclusterid21", "numclusterol21", "numclusteril21", "numclusteril22", "numclusterod23", "numclusterid23", "numclusteril23", "numclusterou23", "numclusteriu23", "numclusteror23", "numclusterir23", "numclusterod24", "numclusterid24", "numclusterol24", "numclusteril24", "numclusterou24", "numclusteriu24", "numclusterir24", "numclusterod25", "numclusterid25", "numclusteril25", "numclusteriu25"]

print(good_counters == check_list) 
good_counters = check_list
print(good_counters)
print(len(good_counters))
'''
ratio_pos_down[ratio_pos_down > 300] = 50
ratio_pos_up[ratio_pos_up > 300] = 50
ratio_pos_left[ratio_pos_left > 300] = 50
ratio_pos_right[ratio_pos_right > 300] = 50
ratio_neg_down[ratio_neg_down > 300] = 50
ratio_neg_up[ratio_neg_up > 300] = 50
ratio_neg_left[ratio_neg_left > 300] = 50
ratio_neg_right[ratio_neg_right > 300] = 50
bin_content1_std_down[ratio_neg_down < 51] = np.nan
bin_content1_std_up[ratio_neg_up < 51] = np.nan
bin_content1_std_left[ratio_neg_left < 51] = np.nan
bin_content1_std_right[ratio_neg_right < 51] = np.nan
bin_content3_std_down[ratio_pos_down < 51] = np.nan
bin_content3_std_up[ratio_pos_up < 51] = np.nan
bin_content3_std_left[ratio_pos_left < 51] = np.nan
bin_content3_std_right[ratio_pos_right < 51] = np.nan
std_ratio_pos_down[ratio_pos_down < 51] = np.nan
std_ratio_pos_up[ratio_pos_up < 51] = np.nan
std_ratio_pos_left[ratio_pos_left < 51] = np.nan
std_ratio_pos_right[ratio_pos_right < 51] = np.nan
std_ratio_neg_down[ratio_neg_down < 51] = np.nan
std_ratio_neg_up[ratio_neg_up < 51] = np.nan
std_ratio_neg_left[ratio_neg_left < 51] = np.nan
std_ratio_neg_right[ratio_neg_right < 51] = np.nan

ratio_pos_down[ratio_pos_down < 50] = 20
ratio_pos_up[ratio_pos_up < 50] = 20
ratio_pos_left[ratio_pos_left < 50] = 20
ratio_pos_right[ratio_pos_right < 50] = 20
ratio_neg_down[ratio_neg_down < 50] = 20
ratio_neg_up[ratio_neg_up < 50] = 20
ratio_neg_left[ratio_neg_left < 50] = 20
ratio_neg_right[ratio_neg_right < 50] = 20
bin_content1_std_down[ratio_neg_down < 100] = np.nan
bin_content1_std_up[ratio_neg_up < 100] = np.nan
bin_content1_std_left[ratio_neg_left < 100] = np.nan
bin_content1_std_right[ratio_neg_right < 100] = np.nan
bin_content3_std_down[ratio_pos_down < 100] = np.nan
bin_content3_std_up[ratio_pos_up < 100] = np.nan
bin_content3_std_left[ratio_pos_left < 100] = np.nan
bin_content3_std_right[ratio_pos_right < 100] = np.nan
std_ratio_pos_down[ratio_pos_down < 100] = np.nan
std_ratio_pos_up[ratio_pos_up < 100] = np.nan
std_ratio_pos_left[ratio_pos_left < 100] = np.nan
std_ratio_pos_right[ratio_pos_right < 100] = np.nan
std_ratio_neg_down[ratio_neg_down < 100] = np.nan
std_ratio_neg_up[ratio_neg_up < 100] = np.nan
std_ratio_neg_left[ratio_neg_left < 100] = np.nan
std_ratio_neg_right[ratio_neg_right < 100] = np.nan
'''
# Create subplots for different categories of counters
fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, figsize=(30, 20))
width = 0.3       # the width of the bars

# Upper counters
axes[0].bar(np.arange(52)-0.5*width, ratio_pos_up, label="-0.484500",width=0.3)
#axes[0].bar(np.arange(52), bin_content2_up, label="0.0",width=0.3)
axes[0].bar(np.arange(52)+0.5*width, ratio_neg_up, label="0.484488",width=0.3)
axes[0].set_xticks(np.arange(0, 52, 2), minor=False)
axes[0].set_xticklabels(bin_names_up[::2], fontdict=None, minor=False)
axes[0].set_ylabel("Counts per lumi [$\mu$barn s]")
axes[0].set_title("Upper counters")

# Down counters
axes[1].bar(np.arange(52)-0.5*width, ratio_pos_down, label="-0.484500",width=0.3)
#axes[1].bar(np.arange(52), bin_content2_down, label="0.0",width=0.3)
axes[1].bar(np.arange(52)+0.5*width, ratio_neg_down, label="0.484488",width=0.3)
axes[1].set_xticks(np.arange(0, 52, 2), minor=False)
axes[1].set_xticklabels(bin_names_down[::2], fontdict=None, minor=False)
axes[1].set_ylabel("Counts per lumi  [$\mu$barn s]")
axes[1].set_title("Down counters")

# Left counters
axes[2].bar(np.arange(52)-0.5*width, ratio_pos_left, label="-0.484500",width=0.3)
#axes[2].bar(np.arange(52), bin_content2_left, label="0.0",width=0.3)
axes[2].bar(np.arange(52)+0.5*width, ratio_neg_left, label="0.484488",width=0.3)
axes[2].set_xticks(np.arange(0, 52, 2), minor=False)
axes[2].set_xticklabels(bin_names_left[::2], fontdict=None, minor=False)
axes[2].set_ylabel("Counts per lumi [$\mu$barn s]")
axes[2].set_title("Left counters")

# Right counters
axes[3].bar(np.arange(52)-0.5*width, ratio_pos_right, label="-0.484500",width=0.3)
#axes[3].bar(np.arange(52), bin_content2_right, label="0.0",width=0.3)
axes[3].bar(np.arange(52)+0.5*width, ratio_neg_right, label="0.484488",width=0.3)
axes[3].set_xticks(np.arange(0, 52, 2), minor=False)
axes[3].set_xticklabels(bin_names_right[::2], fontdict=None, minor=False)
axes[3].set_ylabel("Counts per lumi [$\mu$barn s]")
axes[3].set_xlabel("Counter")
axes[3].set_title("Right counters")

# Add legend
axes[0].legend(loc='upper left')

# Add title
fig.suptitle('Ratio MC/data at counts per lumi for different counters at different beam displacements', fontsize=16, y=1.0)
plt.tight_layout()
# Save the plot
plt.savefig('./MC_comparison_symmetric_ratio_counters_y.pdf')
# Show the plot

plt.show()




# Create subplots for different categories of counters
fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, figsize=(30, 20))
width = 0.3       # the width of the bars

# Upper counters
axes[0].bar(np.arange(52)-0.5*width, std_ratio_neg_up, label="-0.484500",width=0.3)
#axes[0].bar(np.arange(52), bin_content2_up, label="0.0",width=0.3)
axes[0].bar(np.arange(52)+0.5*width, std_ratio_pos_up, label="0.484488",width=0.3)
axes[0].set_xticks(np.arange(0, 52, 2), minor=False)
axes[0].set_xticklabels(bin_names_up[::2], fontdict=None, minor=False)
axes[0].set_ylabel("stdev/mean of Counts [$\mu$barn s]")
axes[0].set_title("Upper counters")

# Down counters
axes[1].bar(np.arange(52)-0.5*width, std_ratio_neg_down, label="-0.484500",width=0.3)
#axes[1].bar(np.arange(52), bin_content2_down, label="0.0",width=0.3)
axes[1].bar(np.arange(52)+0.5*width, std_ratio_pos_down, label="0.484488",width=0.3)
axes[1].set_xticks(np.arange(0, 52, 2), minor=False)
axes[1].set_xticklabels(bin_names_down[::2], fontdict=None, minor=False)
axes[1].set_ylabel("stdev/mean of Counts [$\mu$barn s]")
axes[1].set_title("Down counters")

# Left counters
axes[2].bar(np.arange(52)-0.5*width, std_ratio_neg_left, label="-0.484500",width=0.3)
#axes[2].bar(np.arange(52), bin_content2_left, label="0.0",width=0.3)
axes[2].bar(np.arange(52)+0.5*width, std_ratio_pos_left, label="0.484488",width=0.3)
axes[2].set_xticks(np.arange(0, 52, 2), minor=False)
axes[2].set_xticklabels(bin_names_left[::2], fontdict=None, minor=False)
axes[2].set_ylabel("stdev/mean of Counts [$\mu$barn s]")
axes[2].set_title("Left counters")

# Right counters
axes[3].bar(np.arange(52)-0.5*width, std_ratio_neg_right, label="-0.484500",width=0.3)
#axes[3].bar(np.arange(52), bin_content2_right, label="0.0",width=0.3)
axes[3].bar(np.arange(52)+0.5*width, std_ratio_pos_right, label="0.484488",width=0.3)
axes[3].set_xticks(np.arange(0, 52, 2), minor=False)
axes[3].set_xticklabels(bin_names_right[::2], fontdict=None, minor=False)
axes[3].set_ylabel("stdev/mean of Counts [$\mu$barn s]")
axes[3].set_xlabel("Counter")
axes[3].set_title("Right counters")

# Add legend
axes[0].legend(loc='upper left')

# Add title
fig.suptitle('Relative uncertainty for different counters at different beam displacements', fontsize=16, y=1.0)
plt.tight_layout()
# Save the plot
plt.savefig('./rel_unc_symmetric_ratio_counters_y.pdf')
# Show the plot

plt.show()


# Create subplots for different categories of counters
fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, figsize=(30, 20))
width = 0.2       # the width of the bars

# Upper counters
axes[0].bar(np.arange(52)-1.5*width, bin_content1_std_up, label="-0.484500",width=0.2)
axes[0].bar(np.arange(52)-0.5*width, bin_content1_err_up, label="expected neg",width=0.2)
axes[0].bar(np.arange(52)+0.5*width, bin_content3_err_up, label="expected pos",width=0.2)
axes[0].bar(np.arange(52)+1.5*width, bin_content3_std_up, label="0.484488",width=0.2)
axes[0].set_xticks(np.arange(0, 52, 2), minor=False)
axes[0].set_xticklabels(bin_names_up[::2], fontdict=None, minor=False)
axes[0].set_ylabel("stdev of Counts per event per lumi [$\mu$barn s]")
axes[0].set_title("Upper counters")

# Down counters
axes[1].bar(np.arange(52)-1.5*width, bin_content1_std_down, label="-0.484500",width=0.2)
axes[1].bar(np.arange(52)-0.5*width, bin_content1_err_down, label="expected neg",width=0.2)
axes[1].bar(np.arange(52)+0.5*width, bin_content3_err_down, label="expected pos",width=0.2)
axes[1].bar(np.arange(52)+1.5*width, bin_content3_std_down, label="0.484488",width=0.2)
axes[1].set_xticks(np.arange(0, 52, 2), minor=False)
axes[1].set_xticklabels(bin_names_down[::2], fontdict=None, minor=False)
axes[1].set_ylabel("stdev of Counts per event per lumi [$\mu$barn s]")
axes[1].set_title("Down counters")

# Left counters
axes[2].bar(np.arange(52)-1.5*width, bin_content1_std_left, label="-0.484500",width=0.2)
axes[2].bar(np.arange(52)-0.5*width, bin_content1_err_left, label="expected neg",width=0.2)
axes[2].bar(np.arange(52)+0.5*width, bin_content3_err_left, label="expected pos",width=0.2)
axes[2].bar(np.arange(52)+1.5*width, bin_content3_std_left, label="0.484488",width=0.2)
axes[2].set_xticks(np.arange(0, 52, 2), minor=False)
axes[2].set_xticklabels(bin_names_left[::2], fontdict=None, minor=False)
axes[2].set_ylabel("stdev of Counts per event per lumi [$\mu$barn s]")
axes[2].set_title("Left counters")

# Right counters
axes[3].bar(np.arange(52)-1.5*width, bin_content1_std_right, label="-0.484500",width=0.2)
axes[3].bar(np.arange(52)-0.5*width, bin_content1_err_right, label="expected neg",width=0.2)
axes[3].bar(np.arange(52)+0.5*width, bin_content3_err_right, label="expected pos",width=0.2)
axes[3].bar(np.arange(52)+1.5*width, bin_content3_std_right, label="0.484488",width=0.2)
axes[3].set_xticks(np.arange(0, 52, 2), minor=False)
axes[3].set_xticklabels(bin_names_right[::2], fontdict=None, minor=False)
axes[3].set_ylabel("stdev of Counts per event per lumi [$\mu$barn s]")
axes[3].set_xlabel("Counter")
axes[3].set_title("Right counters")

# Add legend
axes[0].legend(loc='upper left')

# Add title
fig.suptitle('Stdev for different counters at different beam displacements', fontsize=16, y=1.0)
plt.tight_layout()
# Save the plot
plt.savefig('./stdev_symmetric_ratio_counters_y.pdf')
# Show the plot

plt.show()
# Import necessary libraries
import numpy as np
import yaml
import matplotlib.pyplot as plt

# Read the configuration from a YAML file
path = "../../beamline_estimation/config/"
velo_side = "C"
path_velo = ""
if velo_side != "":
    path_velo = "_VELO_" + velo_side
stream_x = open(path+"pca_x"+path_velo+".yaml", "r", encoding="utf-8")
config_dictionary_pca_x = yaml.safe_load(stream_x)

stream_y = open(path+"pca_y"+path_velo+".yaml", "r", encoding="utf-8")
config_dictionary_pca_y = yaml.safe_load(stream_y)

stream2 = open("../../beamline_estimation/config/config.yaml", "r", encoding="utf-8")
config_dictionary = yaml.safe_load(stream2)

# Extract eigenvectors and the number of variables from the configuration
eigenvectors_x = config_dictionary_pca_x["gEigenVectors"]
nVariables = config_dictionary_pca_x["gNVariables"]
eigenvectors_y = config_dictionary_pca_y["gEigenVectors"]

# Initialize lists to store bin content and bin names
bin_content_x = []
bin_content_y = []

# Extract every nth value from eigenvectors as bin content
for j in range(0, len(eigenvectors_x), nVariables):
    bin_content_x.append(eigenvectors_x[j])
    bin_content_y.append(eigenvectors_y[j])

# Process and modify bin names
bin_names = config_dictionary["all_columns"]
bin_names = ["M" + w for w in bin_names]
bin_names = [w.replace("A", "O").replace("B", "I") for w in bin_names]
bin_names = [temp[:-1] + "_" + temp[-1:] for temp in bin_names]
if velo_side == "A":
    working_columns = config_dictionary["veloA_all"]
elif velo_side == "C":
    working_columns = config_dictionary["veloC_all"]
else:
    working_columns = config_dictionary["veloA_all"] + config_dictionary["veloC_all"]
working_columns = [w.replace("_outer", "A").replace("_inner", "B") for w in working_columns]
working_columns.sort()
working_columns = [w.replace("A", "O").replace("B", "I") for w in working_columns]
working_columns = [temp[:-1] + "_" + temp[-1:] for temp in working_columns]

new_dict_x = {working_columns[i]: bin_content_x[i] for i in range(len(working_columns))}
new_dict_y = {working_columns[i]: bin_content_y[i] for i in range(len(working_columns))}

for element in bin_names:
    if element not in new_dict_x:
        new_dict_x[element] = 0
    if element not in new_dict_y:
        new_dict_y[element] = 0

sorted_dict_x = {key: value for key, value in sorted(new_dict_x.items())}
sorted_dict_y = {key: value for key, value in sorted(new_dict_y.items())}

bin_content_x = list(sorted_dict_x.values())
bin_content_y = list(sorted_dict_y.values())

# Define missing values and process them similar to bin names
missing_values = [
    # ...existing missing values...
]
missing_values = [w.replace("_outer", "O").replace("_inner", "I") for w in missing_values]
missing_values = [temp[:-1] + "_" + temp[-1:] for temp in missing_values]

# Separate the bin names and content into different categories
def separate_bins(bin_names, bin_content):
    bin_names_down, bin_names_up, bin_names_left, bin_names_right = [], [], [], []
    bin_content_down, bin_content_up, bin_content_left, bin_content_right = [], [], [], []

    for j in range(0, len(bin_names), 8):
        bin_names_down.extend(bin_names[j:j+2])
        bin_content_down.extend(bin_content[j:j+2])
        bin_names_left.extend(bin_names[j+2:j+4])
        bin_content_left.extend(bin_content[j+2:j+4])
        bin_names_up.extend(bin_names[j+4:j+6])
        bin_content_up.extend(bin_content[j+4:j+6])
        bin_names_right.extend(bin_names[j+6:j+8])
        bin_content_right.extend(bin_content[j+6:j+8])
    return (bin_names_down, bin_content_down, bin_names_up, bin_content_up,
            bin_names_left, bin_content_left, bin_names_right, bin_content_right)

(bin_names_down_x, bin_content_down_x, bin_names_up_x, bin_content_up_x,
 bin_names_left_x, bin_content_left_x, bin_names_right_x, bin_content_right_x) = separate_bins(bin_names, bin_content_x)
(bin_names_down_y, bin_content_down_y, bin_names_up_y, bin_content_up_y,
 bin_names_left_y, bin_content_left_y, bin_names_right_y, bin_content_right_y) = separate_bins(bin_names, bin_content_y)

# Create color lists based on the presence of bin names in missing values
def create_color_list(bin_names, missing_values):
    return ["red" if nome in missing_values else "green" for nome in bin_names]

colori_up_x = create_color_list(bin_names_up_x, missing_values)
colori_down_x = create_color_list(bin_names_down_x, missing_values)
colori_left_x = create_color_list(bin_names_left_x, missing_values)
colori_right_x = create_color_list(bin_names_right_x, missing_values)

colori_up_y = create_color_list(bin_names_up_y, missing_values)
colori_down_y = create_color_list(bin_names_down_y, missing_values)
colori_left_y = create_color_list(bin_names_left_y, missing_values)
colori_right_y = create_color_list(bin_names_right_y, missing_values)

# Function to create subplots for different categories of counters
def create_subplots(bin_content_up, bin_content_down, bin_content_left, bin_content_right,
                    bin_names_up, bin_names_down, bin_names_left, bin_names_right,
                    colori_up, colori_down, colori_left, colori_right, title, filename):
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, figsize=(13, 5))

    # Upper counters
    axes[0].bar(np.arange(len(bin_content_up)), bin_content_up, color=colori_up)
    axes[0].set_xticks(np.arange(0, len(bin_content_up), 5), minor=False)
    axes[0].set_xticklabels(bin_names_up[::5], fontdict=None, minor=False)
    axes[0].set_ylabel("Magnitude")
    axes[0].set_title("Upper counters")
    axes[0].yaxis.set_label_coords(-0.05, 0.5)

    # Down counters
    axes[1].bar(np.arange(len(bin_content_down)), bin_content_down, color=colori_down)
    axes[1].set_xticks(np.arange(0, len(bin_content_down), 5), minor=False)
    axes[1].set_xticklabels(bin_names_down[::5], fontdict=None, minor=False)
    axes[1].set_ylabel("Magnitude")
    axes[1].set_title("Down counters")
    axes[1].yaxis.set_label_coords(-0.05, 0.5)

    # Left counters
    axes[2].bar(np.arange(len(bin_content_left)), bin_content_left, color=colori_left)
    axes[2].set_xticks(np.arange(0, len(bin_content_left), 5), minor=False)
    axes[2].set_xticklabels(bin_names_left[::5], fontdict=None, minor=False)
    axes[2].set_ylabel("Magnitude")
    axes[2].set_title("Left counters")
    axes[2].yaxis.set_label_coords(-0.05, 0.5)

    # Right counters
    axes[3].bar(np.arange(len(bin_content_right)), bin_content_right, color=colori_right)
    axes[3].set_xticks(np.arange(0, len(bin_content_right), 5), minor=False)
    axes[3].set_xticklabels(bin_names_right[::5], fontdict=None, minor=False)
    axes[3].set_ylabel("Magnitude")
    axes[3].set_xlabel("Counter name")
    axes[3].set_title("Right counters")
    axes[3].yaxis.set_label_coords(-0.05, 0.5)

    # Set legend for color coding
    colors = {"Malfunction": "red", "Correct read": "green"}
    labels = list(colors.keys())
    handles = [plt.Rectangle((0, 0), 1, 1, color=colors[label]) for label in labels]
    # plt.figlegend(handles, labels)

    # Add titles and labels to the plot
    fig.suptitle(title)
    fig.tight_layout()

    # Save the plot as a PDF
    plt.savefig(filename)

    # Show the plot
    plt.show()

name_plot = "Components of first PC for x estimation"
if velo_side != "":
    name_plot += ' VELO '+velo_side

name_plot_y = "Components of first PC for y estimation"
if velo_side != "":
    name_plot_y += ' VELO '+velo_side

# Create subplots for x and y estimations
create_subplots(bin_content_up_x, bin_content_down_x, bin_content_left_x, bin_content_right_x,
                bin_names_up_x, bin_names_down_x, bin_names_left_x, bin_names_right_x,
                colori_up_x, colori_down_x, colori_left_x, colori_right_x,
                name_plot, "1pc_x"+path_velo+".pdf")

create_subplots(bin_content_up_y, bin_content_down_y, bin_content_left_y, bin_content_right_y,
                bin_names_up_y, bin_names_down_y, bin_names_left_y, bin_names_right_y,
                colori_up_y, colori_down_y, colori_left_y, colori_right_y,
                name_plot_y, "1pc_y"+path_velo+".pdf")
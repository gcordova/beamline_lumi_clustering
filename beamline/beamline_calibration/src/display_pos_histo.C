
#include "TPrincipal.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include <iostream>
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TBrowser.h"
#include "TPaveText.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TF1.h"
#include <ctime>

void display_pos_histo()
{
    gStyle->SetOptStat("eoumr");
   using namespace std;
   using namespace ROOT::VecOps;
   std::string variable= "x";
   TString x_axis_name = "pv " +variable + " [mm]";
   std::string numpix = "";
   TString clu_string = "pv_" + variable + "_cluster"; 
   TString filename = "../MC_data/tree"+numpix+"_100k_" + variable + "_compact.root";
   ROOT::RDataFrame df("Events", filename);
   auto h = df.Filter("pv_x_cluster == 0").Histo1D({"h","Distribution of primary vertices",5000,-2,2},"nominal_x");
   auto g = df.Filter("pv_x_cluster == 0").Graph("nominal_x","pv_y_cluster");
   TCanvas *c =new TCanvas();
   h->GetXaxis()->SetTitle(x_axis_name);
   h->GetYaxis()->SetTitle("Counts");
   h->DrawClone();

   TCanvas *c1 =new TCanvas();
   g->SetMarkerStyle(5);
   g->DrawClone("AP");


   
}
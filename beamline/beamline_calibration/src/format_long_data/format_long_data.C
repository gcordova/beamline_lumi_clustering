#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include <stdio.h>
#include <iostream>
#include <string>

auto helper(std::string con)
{
   //create a TChain
   //TChain *tree = new TChain("Data_NTuple");
   // Open the file

   TString file_name = "../../MC_data/dati_Daniele/num_pix/histo_z";
   file_name += con;
   file_name += ".root";
   TFile *f = TFile::Open(file_name);

   // Get the tree
   TTree *tree = (TTree *)f->Get("Data_NTuple_num_pix");
   // Create a new tree
   TString output_file_name = "../../MC_data/dati_Daniele/tree_100k_z";
   output_file_name += con;
   output_file_name += "_numpix.root";
   TFile *new_file = new TFile(output_file_name, "RECREATE");
   TTree *new_tree = new TTree("Events", "Events");

   // Create the variables
   Int_t counter_rect_0[26];
   Int_t counter_rect_1[26];
   Int_t counter_rect_2[26];
   Int_t counter_rect_3[26];
   Int_t counter_rect_4[26];
   Int_t counter_rect_5[26];
   Int_t counter_rect_6[26];
   Int_t counter_rect_7[26];

   // Set the branches
   tree->SetBranchAddress("counter_rect_0", &counter_rect_0);
   tree->SetBranchAddress("counter_rect_1", &counter_rect_1);
   tree->SetBranchAddress("counter_rect_2", &counter_rect_2);
   tree->SetBranchAddress("counter_rect_3", &counter_rect_3);
   tree->SetBranchAddress("counter_rect_4", &counter_rect_4);
   tree->SetBranchAddress("counter_rect_5", &counter_rect_5);
   tree->SetBranchAddress("counter_rect_6", &counter_rect_6);
   tree->SetBranchAddress("counter_rect_7", &counter_rect_7);

   // Create the new branches
   Int_t array_cluster_od[26];
   Int_t array_cluster_id[26];
   Int_t array_cluster_ou[26];
   Int_t array_cluster_iu[26];
   Int_t array_cluster_ol[26];
   Int_t array_cluster_il[26];
   Int_t array_cluster_or[26];
   Int_t array_cluster_ir[26];
   Int_t array_cluster[208];
   Double_t nominal_z;
   // Set the new branches
   new_tree->Branch("array_cluster_od", &array_cluster_od, "array_cluster_od[26]/I");
   new_tree->Branch("array_cluster_id", &array_cluster_id, "array_cluster_id[26]/I");
   new_tree->Branch("array_cluster_ou", &array_cluster_ou, "array_cluster_ou[26]/I");
   new_tree->Branch("array_cluster_iu", &array_cluster_iu, "array_cluster_iu[26]/I");
   new_tree->Branch("array_cluster_ol", &array_cluster_ol, "array_cluster_ol[26]/I");
   new_tree->Branch("array_cluster_il", &array_cluster_il, "array_cluster_il[26]/I");
   new_tree->Branch("array_cluster_or", &array_cluster_or, "array_cluster_or[26]/I");
   new_tree->Branch("array_cluster_ir", &array_cluster_ir, "array_cluster_ir[26]/I");
   new_tree->Branch("array_cluster", &array_cluster, "array_cluster[208]/I");
   new_tree->Branch("nominal_z", &nominal_z, "nominal_z/D");


   // Loop over the entries
   for (Int_t i = 0; i < tree->GetEntries(); i++)
   {
      // Get the entry
      ROOT::RVecD counts_od_tmp; 
      ROOT::RVecD counts_id_tmp;
      ROOT::RVecD counts_ou_tmp;
      ROOT::RVecD counts_iu_tmp;
      ROOT::RVecD counts_ol_tmp;
      ROOT::RVecD counts_il_tmp;
      ROOT::RVecD counts_or_tmp;
      ROOT::RVecD counts_ir_tmp;
      ROOT::RVecD counts_tmp;


      tree->GetEntry(i);
      nominal_z = stof(con)/1;
      for (Int_t j = 0; j < 26; j++)
      {         
         
         counts_id_tmp.push_back(counter_rect_0[j]);
         counts_od_tmp.push_back(counter_rect_1[j]);
         counts_il_tmp.push_back(counter_rect_2[j]);
         counts_ol_tmp.push_back(counter_rect_3[j]);
         counts_iu_tmp.push_back(counter_rect_4[j]);
         counts_ou_tmp.push_back(counter_rect_5[j]);
         counts_ir_tmp.push_back(counter_rect_6[j]);
         counts_or_tmp.push_back(counter_rect_7[j]);
         counts_tmp.push_back(counter_rect_1[j]);
         counts_tmp.push_back(counter_rect_0[j]);
         counts_tmp.push_back(counter_rect_3[j]);
         counts_tmp.push_back(counter_rect_2[j]);
         counts_tmp.push_back(counter_rect_5[j]);
         counts_tmp.push_back(counter_rect_4[j]);
         counts_tmp.push_back(counter_rect_7[j]);
         counts_tmp.push_back(counter_rect_6[j]);


      }

      for (int i = 0; i < 26; i++)
      {
         array_cluster_od[i] = counts_od_tmp[i];
         array_cluster_id[i] = counts_id_tmp[i];
         array_cluster_iu[i] = counts_iu_tmp[i];
         array_cluster_ou[i] = counts_ou_tmp[i];
         array_cluster_il[i] = counts_il_tmp[i];
         array_cluster_ol[i] = counts_ol_tmp[i];
         array_cluster_ir[i] = counts_ir_tmp[i];
         array_cluster_or[i] = counts_or_tmp[i];

/*          std::cout << "element " << i << std::endl;
         std::cout << array_cluster_id[i] << std::endl;
         std::cout << array_cluster_od[i] << std::endl;
         std::cout << array_cluster_iu[i] << std::endl;
         std::cout << array_cluster_ou[i] << std::endl;
         std::cout << array_cluster_il[i] << std::endl;
         std::cout << array_cluster_ol[i] << std::endl;
         std::cout << array_cluster_ir[i] << std::endl;
         std::cout << array_cluster_or[i] << std::endl; */
      }


      for (int i = 0; i < 208; i++)
      {
         //std::cout << "element " << i << std::endl;
         array_cluster[i] = counts_tmp[i];
         //std::cout << array_cluster[i] << std::endl; 
      }


      // Fill the new tree
      new_tree->Fill();  
   }
   // Write the new tree
   new_tree->Write();
   // Close the file
   new_file->Close();
   // Close the old file
   f->Close();
   return new_tree;
}

void format_long_data(){
   helper("-120");
   helper("-100");
   helper("-80");
   helper("-60");
   helper("-40");
   helper("-20");
   helper("120");
   helper("100");
   helper("80");
   helper("60");
   helper("40");
   helper("20");



   ROOT::RDataFrame df1("Events", {"../../MC_data/dati_Daniele/tree_100k_z-120_numpix.root","../../MC_data/dati_Daniele/tree_100k_z-100_numpix.root","../../MC_data/dati_Daniele/tree_100k_z-80_numpix.root","../../MC_data/dati_Daniele/tree_100k_z-60_numpix.root", "../../MC_data/dati_Daniele/tree_100k_z-40_numpix.root","../../MC_data/dati_Daniele/tree_100k_z-20_numpix.root","../../MC_data/dati_Daniele/tree_100k_z120_numpix.root","../../MC_data/dati_Daniele/tree_100k_z100_numpix.root","../../MC_data/dati_Daniele/tree_100k_z80_numpix.root","../../MC_data/dati_Daniele/tree_100k_z60_numpix.root", "../../MC_data/dati_Daniele/tree_100k_z40_numpix.root","../../MC_data/dati_Daniele/tree_100k_z20_numpix.root"});
   df1.Snapshot("Events","../../MC_data/tree_100k_z_numpix.root");
}
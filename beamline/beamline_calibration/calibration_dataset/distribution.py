import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mplhep as hep

def read_csv(name,var,detector,type):
    df = pd.read_csv(name)
    name_estimate = ''
    if detector == '':
        name_estimate = var +'_estimate'
    else:
        name_estimate = var + 'Velo' + detector
    name_mon = 'Velo' + detector + 'BeamPosition' + var
    df['res_'+type] = df[name_estimate] - df[name_mon]
    #keep only values within 50 micron (0.05)
    df = df[(df['res_'+type] < 0.05) & (df['res_'+type] > -0.05)]
    return df


def main():
    var = ['x','y']
    detector = ['A','C','']
    name_dfs = ['sum_raw','mu','sep','trim','sum']
    #path_dfs = ['Velo'+d+v+'_'+df+'_test.csv' for d in detector for v in var for df in name_dfs]
    labels = ['A','mu','D','B','C']
    colors = ['g','r','c','orange','b']
    hep.style.use(hep.style.LHCb2)
    for v in var:
        for d in detector:
            dfs =[]
            for type in name_dfs:
                path_df = 'Velo'+d+v+'_'+type+'_test.csv'
                df = read_csv(path_df,v,d,type)
                #select only columns that starts with res
                df = df.filter(regex='^res')
                df = df*1e3
                dfs.append(df)
            
            #concatenate all dataframes
            df = pd.concat(dfs,axis=1)
            #make 50 bins from minimum to maximum value in the dataframe
            #bins = np.linspace(df.min().min(),df.max().max(),50)
            bins = np.linspace(-50,50,60)
            plt.figure(figsize=(10,7))
            #plot histogram
            Velo = ''
            if d != '':
                Velo = 'Velo'
            print('Histogram of residuals for '+v+Velo+d)
            i=0
            for (df,type,lab,c) in zip(dfs,name_dfs,labels,colors):
                df['res_'+type].hist(bins=bins,alpha=0.2,histtype='stepfilled',linewidth=2,color=c)
                df['res_'+type].hist(alpha=1,label=lab,bins=bins,histtype='step',linewidth=2,color=c)
                #compute statistics and display them
                mean = df['res_'+type].mean()
                std = df['res_'+type].std()
                stat = np.sqrt(mean**2 + std**2)
                #print mean and standard deviation on canvas
                ax = plt.gca()
                #Y_POS = 0.91  # in range 0.0 to 1.0 (100%)
                #this_text = f"plt.text(0.01, {Y_POS}, \"this text\", transform=ax.transAxes"
                plt.text(0.03,0.9-i,f'$\mu$ ({lab})='+str(round(mean,2)),transform=ax.transAxes)
                plt.text(0.03,0.6-i,f'$\sigma$ ({lab})='+str(round(std,2)),transform=ax.transAxes)
                plt.text(0.03,0.3-i,'$\sqrt{\mu^2 + \sigma^2}$ ('+lab+')='+str(round(stat,2)),transform=ax.transAxes)
                i+=0.05
                #plt.axvline(mean, color=c, linestyle='dashed', linewidth=2)
                #plt.axvline(mean+std, color=c, linestyle='dotted', linewidth=2)
                #plt.axvline(mean-std, color=c, linestyle='dotted', linewidth=2)
                print('Mean for '+type+':',mean)
                print('Standard deviation for '+type+':',std)

            
            plt.title('Histogram of residuals for '+v+Velo+d)
            plt.xlabel('Residuals [$\mu$m]')
            plt.ylabel('Counts')
            plt.legend(loc='upper right')
            #grid off
            #plt.grid(False)
            plt.tight_layout()
            plt.savefig('Histogram of residuals for '+v+Velo+d+'.pdf')
            #plt.show()

if __name__ == '__main__': 
    main()

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import yaml


#open and read configuration file
stream = open("config.yaml", "r", encoding="utf-8")
config_dictionary = yaml.safe_load(stream)
year = config_dictionary["year"]
smog = "" #config_dictionary["calib_name"]
str_year=""
if year == "2023":
    str_year = "Vdm_07_09_2023"
    data_year = "data_vdm_2023"
    smog=""
elif year == "2022":
    str_year = "Vdm_10_11_2022"
    data_year = "data_vdm_2022"
elif year == "2024":
    str_year = "Vdm_06_04_2024"
    data_year = "data_minivdm_2024"
df_x = pd.read_csv("../"+str_year+"/output_data/fit_coefficients_grouped_x.csv")
df_y = pd.read_csv("../"+str_year+"/output_data/fit_coefficients_grouped_y.csv")
df_xy =pd.merge(df_x, df_y, on='colName', how='inner')
df_xy['norm'] = (df_xy['norm_x'] * df_xy['norm_y'])/2
df_xy['err_norm'] = np.sqrt((df_xy['err_norm_x'] * df_xy['norm_y'])**2 + (df_xy['err_norm_y'] * df_xy['norm_x'])**2)/2
#df_xy = pd.read_csv("../"+str_year+"/output_data/fit_coefficients_"+smog+".csv")
df_xy.set_index("colName",inplace=True)
df_xy["calibration"]=2*np.pi*df_xy["norm"]*df_xy["sigma_x"]*df_xy["sigma_y"]
df=df_xy
df['d_calibration_d_sigma_x'] = 2 * np.pi * df['sigma_y'] * df['norm']
df['d_calibration_d_sigma_y'] = 2  * np.pi * df['sigma_x'] * df['norm']
df['d_calibration_d_norm'] = 2  * np.pi * df['sigma_x'] * df['sigma_y']

# Calcola l'errore totale usando la formula di propagazione degli errori
df['error_calibration'] = np.sqrt((df['d_calibration_d_sigma_x'] * df['err_sigma_x'])**2 + \
                          (df['d_calibration_d_sigma_y'] * df['err_sigma_y'])**2 + \
                          (df['d_calibration_d_norm'] * df['err_norm'])**2 )
df_xy=df
# Crea un nuovo DataFrame df3 con le colonne da df1 e df2
df_plot_xy = pd.DataFrame({
    'calibration': df_xy["calibration"],
    "error_calibration":df_xy["error_calibration"],
})
#cut all the values below zero in dfplot_xy
#df_plot_xy = df_plot_xy[df_plot_xy["calibration"]>0]
#df_plot_xy = df_plot_xy[df_plot_xy["calibration"]<10]
#df_plot_xy = df_plot_xy[df_plot_xy["error_calibration"]<0.2]
# Crea un nuovo DataFrame vuoto con le colonne desiderate
new_df = pd.DataFrame(columns=['calibration_outer', 'calibration_inner', 'error_calibration_outer', 'error_calibration_inner'])
# Itera sull'indice del DataFrame originale
for counter_name in df_plot_xy.index:
    # Separa i contatori esterni da quelli interni
    if counter_name.endswith('_outer'):
        counter_type = 'outer'
    elif counter_name.endswith('_inner'):
        counter_type = 'inner'
    else:
        raise ValueError(f"Nome del contatore non valido: {counter_name}")
    # Rimuovi il suffisso (_outer o _inner) per ottenere il nome del contatore
    counter_name_without_suffix = counter_name.rsplit('_', 1)[0]
    # Aggiungi una nuova riga al nuovo DataFrame
    new_df.loc[counter_name_without_suffix, f'calibration_{counter_type}'] = df_plot_xy.loc[counter_name, 'calibration']
    new_df.loc[counter_name_without_suffix, f'error_calibration_{counter_type}'] = df_plot_xy.loc[counter_name, 'error_calibration']
z_pos = config_dictionary["z_pos"]
new_df['z_pos'] = new_df.index.map(z_pos)
# Crea un oggetto figura e un asse
new_df = new_df[new_df["calibration_outer"]>0]
#new_df = new_df[new_df["calibration_outer"]<5]
new_df = new_df[new_df["error_calibration_outer"]<1]
new_df = new_df[new_df["calibration_inner"]>0]
#new_df = new_df[new_df["calibration_inner"]<10]
new_df = new_df[new_df["error_calibration_inner"]<10]
fig, ax = plt.subplots(figsize=(15, 5))

# Grafico con errori per calibration_inner
ax.errorbar(new_df.index, new_df['calibration_inner'], yerr=new_df['error_calibration_inner'], linestyle='None', marker='.', label='inner',color = "red")
ax.set(ylabel="$\sigma_{vis}$ [mbarn]", title=f"Calibration pp-VdM {str_year}", xlabel="Counter")

# Grafico con errori per calibration_outer sullo stesso asse y
ax.errorbar(new_df.index, new_df['calibration_outer'], yerr=new_df['error_calibration_outer'], linestyle='None', marker='.',label='outer',color='blue')

#ax.errorbar(df_old.index, df_old['sigma_vis_inner'], linestyle='None', marker='.', color='green',label='inner 10/11/2022')
#ax.errorbar(df_old.index, df_old['sigma_vis_outer'], linestyle='None', marker='.', color='brown',label='outer 10/11/2022')


# Imposta le etichette personalizzate per le ascisse
number_of_steps = 5  # Cambia il numero di passi desiderato
l = np.arange(0, len(new_df), number_of_steps)
ax.set(xticks=l, xticklabels=new_df.index[::number_of_steps])

plt.xticks(rotation=90)  # Ruota le etichette sull'asse x per una migliore leggibilità

# Aggiungi una legenda per i due plot
ax.legend()

plt.tight_layout()  # Per evitare sovrapposizioni di etichette

plt.show()

# Crea un oggetto figura e un asse
fig, ax = plt.subplots(figsize=(15, 5))

# Grafico con errori per calibration_inner
ax.errorbar(new_df["z_pos"], new_df['calibration_inner'], yerr=new_df['error_calibration_inner'], linestyle='None', marker='.', label='inner',color='red')
ax.set(ylabel="$\sigma_{vis}$ [mbarn]", title=f"Visible cross section of counters vs z position - Calibration pp-{str_year}", xlabel="z position of counter [mm]")

# Grafico con errori per calibration_outer sullo stesso asse y
ax.errorbar(new_df["z_pos"], new_df['calibration_outer'], yerr=new_df['error_calibration_outer'], linestyle='None', marker='.', color='blue',label='outer')


# Imposta le etichette personalizzate per le ascisse
number_of_steps = 5  # Cambia il numero di passi desiderato
#l = np.arange(0, len(df_plot_xy), number_of_steps)
#ax.set(xticks=l, xticklabels=df_plot_xy["z_pos"][::number_of_steps])

#plt.xticks(rotation=90)  # Ruota le etichette sull'asse x per una migliore leggibilità

# Aggiungi una legenda per i due plot
ax.legend()

plt.tight_layout()  # Per evitare sovrapposizioni di etichette

plt.show()

df_plot_xy.to_csv("../../../"+data_year+"/output_data/sigma_vis_"+smog+"_new.csv")
"""
Script for merging data from DPS of TEll40

VADAQTELL40:Velo_TELL40_Mij_*_k.luminosity_rates.inst_lumi_bb

The script is called via

python3 merge_data.py

"""
#c
import os
from os.path import exists
import sys
from functools import reduce
from pathlib import Path
import numpy as np
import pandas as pd
from pandas.errors import EmptyDataError
import matplotlib.pyplot as plt
import yaml
import seaborn as sns
import colorcet as cc


def get_data(path, count_type, config_dict,yearfirst=False):
    """
    Load data from a CSV file, format it, and return a DataFrame.

    Args:
        path (str): The path to the CSV file.

    Returns:
        pd.DataFrame: The formatted DataFrame.
    """
    str_year = config_dict["foldername"]
    _, tail = os.path.split(path)
    root, _ = os.path.splitext(tail)
    df = pd.read_csv(path)
    if yearfirst:
        df["TS"] = pd.to_datetime(df["TS"], format="%Y-%m-%d %H:%M:%S.%f")
    else:
        df["TS"] = pd.to_datetime(df["TS"], format="%d-%m-%Y %H:%M:%S.%f")
    df["TS"] = df["TS"].dt.floor("s")
    df = df.drop(columns=[" DPE"])
    newname = root.replace("output_", "")
    count_type = count_type.replace("../../../" + str_year + "/input_data/", "")
    count_type = count_type.replace("_inner", "")
    if newname.startswith("M"):
        newname = newname + count_type
    df = df.rename(columns={" VALUE": newname})
    return df


def create_file_list(config_dict, count_type, counter):
    """
    Create a list of DataFrames to merge. Handle B4BB1E, infinity, and empty rows.

    Args:
        config_dict (dict): Configuration dictionary.
        count_type (str): Type of count ("bb" or "bb_inner").
        counter (str): Name of module (e.g. M23_0)

    Returns:
        list: List of DataFrames to merge.
    """
    start_time = config_dict["StartOfVdm"]
    end_time = config_dict["EndOfCalib"]
    year = config_dict["year"]
    str_year = config_dict["foldername"]
    
    count_type_bb = "../../../" + str_year + "/input_data/" + count_type
    count_type_eb = "../../../" + str_year + "/input_data/eb" + count_type[2:]
    count_type_be = "../../../" + str_year + "/input_data/be" + count_type[2:]
    count_type_ee = "../../../" + str_year + "/input_data/ee" + count_type[2:]
    #print(count_type)
    if year == "2022":
        path_to_search = [count_type_bb]
    else:
        path_to_search = [count_type_bb, count_type_be, count_type_eb, count_type_ee]

    file_list = []
    for path_to_folder in path_to_search:
        for root_folder, _, files in os.walk(path_to_folder):
            for file in files:
                if file == "output_" + counter + ".csv":
                    try:
                        _, tail = os.path.split(path_to_folder)
                        root, _ = os.path.splitext(tail)
                        full_path = os.path.join(root_folder, file)
                        element = get_data(str(full_path), root, config_dict)
                        element = element.loc[element["TS"] >= start_time]
                        element = element.loc[element["TS"] <= end_time]
                        numeric_columns = element.select_dtypes(
                            include=["int64", "float64"]
                        )
                        #print(element)
                        for col in numeric_columns:
                            element = element.loc[
                                element[col] != 9.9900000000000004e125
                            ]
                            element = element.loc[element[col] != 1.2237598e7]
                            element = element.loc[element[col] != 7.64849875e5]
                            element = element.loc[element[col] < 1.64849875e5]
                            if (len(element[col]) > 100 and not np.isnan(  # type: ignore              #element.loc[:, col].mean() != 0
                                element.loc[:, col].mean())):
                                if year == "2022":
                                    element[col] = element[col] / 247400.6 
                                element = (
                                    element.drop_duplicates("TS")
                                    .set_index("TS")
                                    .resample("3s")
                                    .ffill()
                                    .reset_index()
                                )
                                file_list.append(element)
                                print(
                                    "File successfully formatted and inserted into the list"
                                )
                            else:
                                print("Too many B4BB1E. File will not be processed")
                    except EmptyDataError:
                        print(f"No columns to parse from file {str(file)}")
                        print("File will not be processed")
                    except ValueError:
                        print(f"ValueError in file {str(file)}")
    return file_list


def create_file_list_pos(config_dict):
    """
    Create a list of DataFrames to merge. Handle B4BB1E, infinity, and empty rows.
    Works only for 2023 data, 2022 data are already formatted
    Args:
        config_dict (dict): Configuration dictionary.

    Returns:
        list: List of DataFrames to merge.
    """
    start_time = config_dict["StartOfVdm"]
    end_time = config_dict["EndOfCalib"]
    year = config_dict["year"]
    file_list = []
    str_year=config_dict["foldername"]
    count_type = "../../../"+str_year+"/input_data/beam"
    for path in Path(count_type).rglob("*.csv"):
        print(f"Processing file: {str(path)}")
        try:
            yearfirst = False
            if year == "2024":
                yearfirst = True

            element = get_data(str(path), count_type, config_dict,yearfirst)
            element = element.loc[element["TS"] >= start_time]
            element = element.loc[element["TS"] <= end_time]
            numeric_columns = element.select_dtypes(include=["int64", "float64"])
            for col in numeric_columns:
                element = element.loc[element[col] != 9.9900000000000004e125]
                element = element.loc[element[col] != 1.2237598e7]
                element = element.loc[element[col] != 7.64849875e5]
                if element.loc[:, col].mean() != 0 and not np.isnan(  # type: ignore
                    element.loc[:, col].mean()
                ):
                    element = (
                        element.assign(date=element.TS.dt.date)
                        .set_index("TS")
                        .groupby("date")
                        .apply(lambda x: x.resample("3S").ffill())
                        .reset_index("date", drop=True)
                        .drop(columns=["date"])
                        .reset_index()
                    )
                    file_list.append(element)
                    print("File successfully formatted and inserted into the list")
                else:
                    print("Too many B4BB1E. File will not be processed")

        except EmptyDataError:
            print(f"No columns to parse from file {str(path)}")
            print("File will not be processed")
    return file_list


def merge_dataset(config_dict, input_file):
    """
    Merge the CSV data of each TELL40 into one DataFrame with a unique timestamp, which will be set as the index.

    Args:
        config_dict (dict): Configuration dictionary.
        input_file (str): Type of input file (e.g., "bb" or "beam").
        year (str): year of calibration (e.g. "2022" or "2023")

    Returns:
        pd.DataFrame: Merged DataFrame.
    """

    #define an empty list where to store all the csv counters
    df_list = []
    #define year
    year = config_dict["year"]
    #get list of counters from configuration file
    for counter in config_dict["counters_name"]:
        if counter == "M32_0" and input_file == "bb" and year == "2022":
            print(f"Counter {str(counter)} corrupted, skipping...")
            continue
        if counter == "M24_1" and input_file == "bb" and year == "2022":
            print(f"Counter {str(counter)} corrupted, skipping...")
            continue
        print(f"Processing {str(counter)} counters")
        #create a list with the bxtype counters for a module (in counters list you find bb, eb, be ee for a single counter)
        counters_list = create_file_list(config_dict, input_file, counter)
        #if the list is empty (no data for counters were found) skip loop
        if not counters_list:
            continue
            #merge the different type of counters in a unique timestamp
        df_merged_bb = reduce(
                lambda left, right: pd.merge_asof(
                    left,
                    right,
                    on="TS",
                    direction="nearest",
                    tolerance=pd.Timedelta("3s"),
                ),
                counters_list,
            )
        #elif year == "2022":
        #    df_merged_bb = counters_list[0]
        #define a list with the existing counters
        var_list = [col for col in df_merged_bb.columns if col.startswith("M")]
        for el in var_list:
            if year == "2022":
                #subtract background
                if input_file == "bb":
                   col_name = f"{el[:-2]}_outer"
                   new_col_name = f"Delta_{el[:-2]}_outer"
                elif input_file == "bb_inner":
                   col_name = f"{el[:-2]}_inner"
                   new_col_name = f"Delta_{el[:-2]}_inner"   
                df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                )
                #define error
                df_merged_bb[new_col_name] = np.sqrt(
                        df_merged_bb[el[:-2] + "bb"]/247400.6 
                    )
            elif input_file == "bb_inner":
                #subtract background
                col_name = f"{el[:-2]}_inner"
                if year == "2023":
                    df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                    - df_merged_bb[el[:-2] + "be"]
                    - 16 * df_merged_bb[el[:-2] + "ee"]
                    )
                else:
                    df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                    - df_merged_bb[el[:-2] + "be"]
                    - df_merged_bb[el[:-2] + "eb"]
                    + df_merged_bb[el[:-2] + "ee"]
                    )
                #define error
                new_col_name = f"Delta_{el[:-2]}_inner"
                #function calculated with monte carlo
                if year == "2023":
                    df_merged_bb[new_col_name] = 0.000709511 * np.sqrt(
                    0.955167
                    * (
                        df_merged_bb[el[:-2] + "bb"]
                        + df_merged_bb[el[:-2] + "be"]
                        + 16 * df_merged_bb[el[:-2] + "ee"]
                    )
                    )
                else:
                    df_merged_bb[new_col_name] = 0.000709511 * np.sqrt(
                    0.955167
                    * (
                        df_merged_bb[el[:-2] + "bb"]
                        + df_merged_bb[el[:-2] + "be"]
                        + df_merged_bb[el[:-2] + "eb"]
                        + df_merged_bb[el[:-2] + "ee"]
                    )
                )
            elif input_file == "bb":
                #subtract background
                col_name = f"{el[:-2]}_outer"
                df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                    - df_merged_bb[el[:-2] + "be"]
                    - df_merged_bb[el[:-2] + "eb"]
                    + df_merged_bb[el[:-2] + "ee"]
                )
                #define error
                new_col_name = f"Delta_{el[:-2]}_outer"
                df_merged_bb[new_col_name] = 0.000709511 * np.sqrt(
                    0.955167
                    * (
                        df_merged_bb[el[:-2] + "bb"]
                        + df_merged_bb[el[:-2] + "be"]
                        + df_merged_bb[el[:-2] + "eb"]
                        + df_merged_bb[el[:-2] + "ee"]
                    )
                )
        #define columns to remove: every type counters
        col_to_remove = [
            col
            for col in df_merged_bb.columns
            if (
                col.endswith("bb")
                or col.endswith("be")
                or col.endswith("eb")
                or col.endswith("ee")
            )
        ]
        #drop the columns defined above
        df_merged_bb.drop(columns=col_to_remove, inplace=True)

        df_list.append(df_merged_bb)

        #df_merged_bb.to_csv("merged_counters/merged_" + counter + ".csv")
    #merge all counters in a single csv
    df_merged_unique = reduce(
        lambda left, right: pd.merge_asof(
            left,
            right,
            on="TS",
            direction="nearest",
            tolerance=pd.Timedelta("3s"),
        ),
        df_list,
    )
    df_merged_unique.set_index("TS", inplace=True)
    return df_merged_unique


def plot_figure(df, year):
    """
    Create a figure to display counters.

    Args:
        df (pd.DataFrame): DataFrame with counters data.
        year (str): Year of vdm (e.g., "2022" or "2023").
    """
    str_year = ""
    if year == "2023":
        str_year = "Van Der Meer Scan 07/09/2023"
    elif year == "2022":
        str_year = "Van Der Meer Scan 10/11/2022"
    elif year == "2024":
        str_year = "Mini Van Der Meer Scan 06/04/2024"
    elif year == "2024_new":
        str_year = "Van Der Meer Scan 18/05/2024"

    y_lab="Avg counts"
    _, axes = plt.subplots(nrows=3, ncols=1, sharex=True, figsize=(10, 20))

    counters_col = [col for col in df.columns if col.startswith("M")]
    df_counters = df[counters_col]

    df_x1_position = df["Nominal_Displacement_B1_xingPlane"]
    df_x2_position = df["Nominal_Displacement_B2_xingPlane"]
    df_y1_position = df["Nominal_Displacement_B1_sepPlane"]
    df_y2_position = df["Nominal_Displacement_B2_sepPlane"]

    num_colors = len(counters_col)

    sns.reset_orig()
    palette = sns.color_palette(cc.glasbey, n_colors=num_colors)

    ax1 = df_counters.plot(
        marker=".",
        markersize=1,
        title=str_year,
        xlabel="Time Stamp (H)",
        ylabel=y_lab,
        color=palette,
        ax=axes[0],
    )

    ax2 = df_x1_position.plot(
        ax=axes[1],
        marker=".",
        markersize=1,
        linestyle="solid",
        ylabel="x position [mm]",
    )

    ax4 = df_x2_position.plot(
        ax=axes[1],
        marker=".",
        markersize=1,
        linestyle="solid",
        ylabel="x position [mm]",
    )

    ax3 = df_y1_position.plot(
        ax=axes[2],
        marker=".",
        markersize=1,
        linestyle="solid",
        xlabel="Time Stamp (H)",
    )

    ax5 = df_y2_position.plot(
        ax=axes[2],
        marker=".",
        markersize=1,
        linestyle="solid",
        xlabel="Time Stamp (H)",
        ylabel="y position [mm]",
    )

    ax1.legend(prop={"size": 0}, ncols=10)

    ax2.legend(prop={"size": 4})
    ax3.legend(prop={"size": 4})
    ax4.legend(prop={"size": 4})
    ax5.legend(prop={"size": 4})

    plt.show()

def std_divided_count(x):
    return np.std(x) / np.sqrt(len(x))

# Definisci una funzione per eseguire le operazioni desiderate sui gruppi
def custom_operations(group):
    # Rimuovi il primo e l'ultimo valore se ci sono più di due valori nel gruppo
    if len(group) > 2:
        group = group.iloc[1:-1]

    M_columns = [col for col in group.columns if col.startswith("M")]
    # Calcola la media aritmetica per le colonne che iniziano con "M"
    M_std = group[M_columns].std(ddof=0)/np.sqrt(len(group))

    group[M_columns] = group[M_columns].mean()

    # Calcola la somma in quadratura diviso il numero di elementi per le colonne che iniziano con "Delta"
    Delta_columns = [col for col in group.columns if col.startswith("Delta")]
    group[Delta_columns] = np.sqrt((group[Delta_columns]**2).sum()) / len(group)

    #counts = len(group)
    M_std = M_std.rename(lambda x: x.replace('M', 'Delta_M'))
    d = M_std.to_dict()
    for col in Delta_columns:
        group[col] = np.sqrt(group[col]**2 + d.get(col, 0)**2) 
    return group

def get_vdm_counts_xy(df_x, config_dict):
    """
    Create a DataFrame for VDM counts.

    Args:
        df (pd.DataFrame): DataFrame with merged data.
        config_dict (dict): Configuration dictionary.
        

    Returns:
        pd.DataFrame: DataFrame for VDM counts.
    """

    year = config_dict["year"]
    if year == "2023":
        df_bunch = pd.read_csv(
            "../../../data_vdm_2023/input_data/bunch_population/vdM/mean_bunch_population.csv"
        )
        year_str = "Vdm_07_09_2023"
    elif year == "2022":
        df_bunch = pd.read_csv(
            "../../../data_vdm_2022/input_data/bunch_populations/output_bunch_populations.csv"
        )
        year_str = "Vdm_10_11_2022"
    df_x = df_x.drop(
            columns=[
                "Nominal_Displacement_B1_xingPlane",
                "Nominal_Displacement_B2_xingPlane",
                "Nominal_Displacement_B1_sepPlane",
                "Nominal_Displacement_B2_sepPlane",
            ]
        )
    df_bunch["TS"] = pd.to_datetime(df_bunch["TS"], format="%Y-%m-%d %H:%M:%S")
    df_bunch = df_bunch.set_index("TS")
    merged_df = df_x.merge(df_bunch, how="inner", on="TS")
    cols_M = [col for col in merged_df.columns if col.startswith("M")]
    cols_Delta = [col for col in df_x.columns if col.startswith("Delta")]
    for col_name in cols_M:
        merged_df[col_name] /= merged_df["media"]
    for col_name in cols_Delta:
        merged_df[col_name] /= merged_df["media"]
    merged_df = merged_df.drop(columns=["media"])
    grouped = merged_df.groupby(["x_displacement", "y_displacement"])

    new_df = grouped.apply(custom_operations).reset_index(drop=True)
    new_df.drop_duplicates(inplace=True)
    new_df.set_index(["x_displacement", "y_displacement"], inplace=True)
    name = config_dict["calib_name"]
    new_df.to_csv(
        "../"+year_str +"/output_data/Calibration_grouped_xy_"+name+".csv"
    )
    new_df.dropna(axis=0, inplace=True)
    return new_df
    


def get_vdm_counts_xy_consecutives(df_x, config_dict):
    """
    Create a DataFrame for VDM counts.

    Args:
        df (pd.DataFrame): DataFrame with merged data.
        config_dict (dict): Configuration dictionary.
        

    Returns:
        pd.DataFrame: DataFrame for VDM counts.
    """

    year = config_dict["year"]
    if year == "2023":
        df_bunch = pd.read_csv(
            "../../../data_vdm_2023/input_data/bunch_population/vdM/mean_bunch_population.csv"
        )
        year_str = "Vdm_07_09_2023"
    elif year == "2022":
        df_bunch = pd.read_csv(
            "../../../data_vdm_2022/input_data/bunch_populations/output_bunch_populations.csv"
        )
        year_str = "Vdm_10_11_2022"
    df_x = df_x.drop(
            columns=[
                "Nominal_Displacement_B1_xingPlane",
                "Nominal_Displacement_B2_xingPlane",
                "Nominal_Displacement_B1_sepPlane",
                "Nominal_Displacement_B2_sepPlane",
            ]
        )
    df_bunch["TS"] = pd.to_datetime(df_bunch["TS"], format="%Y-%m-%d %H:%M:%S")
    df_bunch = df_bunch.set_index("TS")
    merged_df = df_x.merge(df_bunch, how="inner", on="TS")
    cols_M = [col for col in merged_df.columns if col.startswith("M")]
    cols_Delta = [col for col in df_x.columns if col.startswith("Delta")]
    for col_name in cols_M:
        merged_df[col_name] /= merged_df["media"]
    for col_name in cols_Delta:
        merged_df[col_name] /= merged_df["media"]
    
    consecutives = (
        merged_df["x_displacement"] != merged_df["x_displacement"].shift()
    ) | (merged_df["y_displacement"] != merged_df["y_displacement"].shift())
    consecutives = consecutives.cumsum().tail(-1).head(-1)
    
    df_M_origin = merged_df[cols_M]
    df_M_origin["x_displacement"] = merged_df["x_displacement"]
    df_M_origin["y_displacement"] = merged_df["y_displacement"]
    df_Delta = merged_df[cols_Delta]
    df_Delta["x_displacement"] = merged_df["x_displacement"]
    df_Delta["y_displacement"] = merged_df["y_displacement"]
    df_M = (
        df_M_origin.groupby(consecutives)
        .agg("mean")
        .set_index(["x_displacement", "y_displacement"])
    )
    df_std = (
        df_M_origin.groupby(consecutives)
        .agg(std_divided_count)
        .set_index(["x_displacement", "y_displacement"])
    )
    df_std = df_std.set_index(df_M.index)
    df_std = df_std.add_prefix("Delta_")
    df_Delta = (
        df_Delta.groupby(consecutives)
        .agg(lambda x: np.sqrt((x**2).sum()) / len(x))
        .set_index(["x_displacement", "y_displacement"])
    )
    df_Delta = df_Delta.set_index(df_M.index)
    df_Delta = ((df_std)**2 + df_Delta**2) ** 0.5
    df_x_plot_group = pd.concat([df_M, df_Delta], axis=1)
    name = config_dict["calib_name"]
    df_x_plot_group.to_csv(
        "../"+year_str +"/output_data/Calibration_consecutives_xy_"+name+".csv"
    )
    df_x_plot_group.dropna(axis=0, inplace=True)
    return df_x_plot_group


def get_vdm_counts_x_consecutives(df_x, config_dict,string_var):
    """
    Create a DataFrame for VDM counts.

    Args:
        df (pd.DataFrame): DataFrame with merged data.
        config_dict (dict): Configuration dictionary.
        

    Returns:
        pd.DataFrame: DataFrame for VDM counts.
    """

    year = config_dict["year"]
    if year == "2023":
        df_bunch = pd.read_csv(
            "../../../data_vdm_2023/input_data/bunch_population/vdM/mean_bunch_population.csv"
        )
        year_str = "Vdm_07_09_2023"
    elif year == "2022":
        df_bunch = pd.read_csv(
            "../../../data_vdm_2022/input_data/bunch_populations/output_bunch_populations.csv"
        )
        year_str = "Vdm_10_11_2022"
    elif year == "2024":
        df_bunch = pd.read_csv('../../../data_minivdm_2024/input_data/bunch_population/media_bunch_population.csv')
        year_str= "Vdm_06_04_2024"
    df_x = df_x.drop(
            columns=[
                "Nominal_Displacement_B1_xingPlane",
                "Nominal_Displacement_B2_xingPlane",
                "Nominal_Displacement_B1_sepPlane",
                "Nominal_Displacement_B2_sepPlane",
            ]
        )
    df_bunch["TS"] = pd.to_datetime(df_bunch["TS"], format="%Y-%m-%d %H:%M:%S.%f").dt.round('1s')
    df_bunch = df_bunch.set_index("TS")
    merged_df = df_x.merge(df_bunch, how="inner", on="TS")
    cols_M = [col for col in merged_df.columns if col.startswith("M")]
    cols_Delta = [col for col in df_x.columns if col.startswith("Delta")]
    for col_name in cols_M:
        merged_df[col_name] /= merged_df["media"]
    for col_name in cols_Delta:
        merged_df[col_name] /= merged_df["media"]
    
    consecutives = (
        merged_df["x_displacement"] != merged_df["x_displacement"].shift()
    ) | (merged_df["y_displacement"] != merged_df["y_displacement"].shift())
    consecutives = consecutives.cumsum().tail(-1).head(-1)
    
    df_M_origin = merged_df[cols_M]
    df_M_origin["x_displacement"] = merged_df["x_displacement"]
    df_M_origin["y_displacement"] = merged_df["y_displacement"]
    df_Delta = merged_df[cols_Delta]
    df_Delta["x_displacement"] = merged_df["x_displacement"]
    df_Delta["y_displacement"] = merged_df["y_displacement"]
    df_M = (
        df_M_origin.groupby(consecutives)
        .agg("mean")
        .set_index(["x_displacement", "y_displacement"])
    )
    df_std = (
        df_M_origin.groupby(consecutives)
        .agg(std_divided_count)
        .set_index(["x_displacement", "y_displacement"])
    )
    df_std = df_std.set_index(df_M.index)
    df_std = df_std.add_prefix("Delta_")
    df_Delta = (
        df_Delta.groupby(consecutives)
        .agg(lambda x: np.sqrt((x**2).sum()) / len(x))
        .set_index(["x_displacement", "y_displacement"])
    )
    df_Delta = df_Delta.set_index(df_M.index)
    df_Delta = ((df_std)**2 + df_Delta**2) ** 0.5
    df_x_plot_group = pd.concat([df_M, df_Delta], axis=1)
    name = config_dict["calib_name"]
    df_x_plot_group.to_csv(
        "../"+year_str +"/output_data/Calibration_consecutives_"+string_var+"_"+name+".csv"
    )
    df_x_plot_group.dropna(axis=0, inplace=True)
    return df_x_plot_group



def gaus_fig(df, var, evt, input_file):
    """
    Generate a figure for VDM.

    Args:
        df (pd.DataFrame): DataFrame with VDM data.
        var (str): Variable (e.g., "x_displacement", "y_displacement").
        evt (str): Event type (e.g., "evt_0", "evt_1").
        input_file (str): Type of input file (e.g., "bb" or "beam").
    """
    if input_file == "bb":
        count_type = "out_bb"
    else:
        count_type = input_file

    figname = (
        count_type + " counters in 2023 VdM vs " + var[0] + " separation in " + evt
    )

    counters_col = [col for col in df.columns if col.startswith("M")]
    num_colors = len(counters_col)

    sns.reset_orig()
    palette = sns.color_palette(cc.glasbey, n_colors=num_colors)
    ax = df.plot(
        marker=".",
        markersize=1,
        title=figname,
        xlabel=var[0] + " separation of two beams [mm]",
        ylabel="Avg " + count_type + " counts",
        color=palette,
    )

    ax.legend(prop={"size": 4}, ncols=5)

    plt.show()


def main():
    """
    Main body of the script.
    """
    #open and read configuration file. It takes informations such as the year of the calibration, 
    #the start and end of the calibration, the display of the figure, the counters to be used,
    #the bunch population, the frequency of lhc, the number of colliding bunches
    stream = open("config.yaml", "r", encoding="utf-8")
    config_dictionary = yaml.safe_load(stream)
    #set options according to configuration file
    display_fig = config_dictionary["displayFig"]
    year = config_dictionary["year"]
    str_year=config_dictionary["foldername"]
    #get year of calibration from config file, search for data in the right folder
    
    #if the dataset is not already create, merge all the counters in a csv file and save it
    if not exists("../../../" + str_year + "/input_data/data_merged.csv"):
        #create file list for position of the beam
        #if the year is 2022, the data are already formatted
        if year == "2022":

            df_position = pd.read_csv(
                "../../../data_vdm_2022/input_data/position_beams/output_beam_position.csv"
            )
            
            df_position["TS"] = pd.to_datetime(
                df_position["TS"], format="%Y-%m-%d %H:%M:%S.%f"
            )
            #take the timestamp one hour before due to the time zone
            df_position["TS"] = df_position["TS"] - pd.Timedelta("1h")
            df_position = df_position.set_index("TS")   
            # Crea un indice con tutti i timestamp desiderati
            df_position = df_position.resample("3s").ffill()
        #if the year is 2023, the data are not already formatted
        else:
            position_list = create_file_list_pos(config_dictionary)
            #merge all the csv in the list above
            df_position = reduce(
            lambda left, right: pd.merge(left, right, on="TS", how="outer"), position_list
            ).sort_values(by="TS")
            df_position.set_index("TS",inplace=True)
        #merge all the bb counters in a single csv 
        df_merged_bb = merge_dataset(config_dictionary, "bb")
        #merge all the bb_inner counters in a single csv
        df_merged_bb_inner =  merge_dataset(config_dictionary, "bb_inner")
        #merge the bb and bb_inner counters in a single csv
        df_merged_unique = pd.merge_asof(df_merged_bb,
        df_merged_bb_inner,
        left_index = True, right_index = True,
        direction="nearest",
        tolerance=pd.Timedelta("3s"),
        )
        #merge the position of the beam with the counters
        df_merged_unique = pd.merge_asof(
        df_position,
        df_merged_unique,
        left_index = True, right_index = True,
        direction="nearest",
        tolerance=pd.Timedelta("3s"),
    )   
        #select only the data in the calibration period
        mask = (df_merged_unique.index> config_dictionary["StartOfCalib"]) & (df_merged_unique.index <= config_dictionary["EndOfCalib"])
        df_merged_unique = df_merged_unique.loc[mask]
        from_ts = "2022-11-10 10:29:45" #2022-11-10 10:29:45
        to_ts = "2022-11-10 10:32:00" #2022-11-10 10:32:00
        df_merged_unique = df_merged_unique.loc[
            (df_merged_unique.index < from_ts) | (df_merged_unique.index > to_ts)
        ]       
        #save the merged dataset
        df_merged_unique.to_csv(
            "../../../" + str_year + "/input_data/data_merged.csv"
        )
    else:
        #if the dataset is already created, load it
        df_merged_unique = pd.read_csv(
            "../../../" + str_year + "/input_data/data_merged.csv"
        )
        df_merged_unique["TS"] = pd.to_datetime(
            df_merged_unique["TS"], format="%Y-%m-%d %H:%M:%S"
            )
        df_merged_unique = df_merged_unique.set_index("TS")
        #mask = (df_merged_unique.index> config_dictionary["StartOfCalib"]) & (df_merged_unique.index <= config_dictionary["EndOfCalib"])
        #df_merged_unique = df_merged_unique.loc[mask]
    #plot the figure with the counters and the position of the beam
    if display_fig:
        plot_figure(df_merged_unique, year)
    #define the displacement of the beam in x and y
    df_merged_unique["x_displacement"] = (
        df_merged_unique["Nominal_Displacement_B2_xingPlane"]
        - df_merged_unique["Nominal_Displacement_B1_xingPlane"]
    )
    df_merged_unique["y_displacement"] = (
        df_merged_unique["Nominal_Displacement_B2_sepPlane"]
        - df_merged_unique["Nominal_Displacement_B1_sepPlane"]
    )
    #create a csv with the average counts and the error as a function of the beam displacement
    df_x = df_merged_unique.between_time("13:04:09", "13:13")
    #get_vdm_counts_x_consecutives(df_x,config_dictionary,"x")
    df_y = df_merged_unique.between_time("13:14:19", "13:23")
    #get_vdm_counts_x_consecutives(df_y,config_dictionary,"y")



#

if __name__ == "__main__":
    main()

"""
Script for merging data from DPS of TEll40

VADAQTELL40:Velo_TELL40_Mij_*_k.luminosity_rates.inst_lumi_bb

The script is called via

python3 merge_data.py

"""


import os
from os.path import exists
import sys
from functools import reduce
from pathlib import Path
import numpy as np
import pandas as pd
from pandas.errors import EmptyDataError
import matplotlib.pyplot as plt
import yaml
import seaborn as sns
import colorcet as cc
from scipy.stats import trim_mean
import datetime as dt



def get_data(path, count_type, year):
    """
    Load data from a CSV file, format it, and return a DataFrame.

    Args:
        path (str): The path to the CSV file.

    Returns:
        pd.DataFrame: The formatted DataFrame.
    """
    str_year = year
    _, tail = os.path.split(path)
    root, _ = os.path.splitext(tail)
    df = pd.read_csv(path)
    df["TS"] = pd.to_datetime(df["TS"], format="%d-%m-%Y %H:%M:%S.%f")
    df["TS"] = df["TS"].dt.floor("S")
    df = df.drop(columns=[" DPE"])
    newname = root.replace("output_", "")
    count_type = count_type.replace("../" + str_year + "/input_data/", "")
    count_type = count_type.replace("_inner", "")
    if newname.startswith("M"):
        newname = newname + count_type
    df = df.rename(columns={" VALUE": newname})
    return df


def create_file_list(config_dict, count_type, counter):
    """
    Create a list of DataFrames to merge. Handle B4BB1E, infinity, and empty rows.

    Args:
        config_dict (dict): Configuration dictionary.
        count_type (str): Type of count ("bb" or "bb_inner").
        counter (str): Name of module (e.g. M23_0)

    Returns:
        list: List of DataFrames to merge.
    """
    start_time = config_dict["StartOfData"]
    end_time = config_dict["EndOfData"]
    str_year = config_dict["folder_name"]
    
    file_list = []

    count_type_bb = "../" + str_year + "/input_data/" + count_type
    count_type_eb = "../" + str_year + "/input_data/eb" + count_type[2:]
    count_type_be = "../" + str_year + "/input_data/be" + count_type[2:]
    count_type_ee = "../" + str_year + "/input_data/ee" + count_type[2:]

    path_to_search = [count_type_bb, count_type_be, count_type_eb, count_type_ee]
    for path_to_folder in path_to_search:
        for root_folder, _, files in os.walk(path_to_folder):
            for file in files:
                if file == "output_" + counter + ".csv":
                    try:
                        _, tail = os.path.split(path_to_folder)
                        root, _ = os.path.splitext(tail)
                        full_path = os.path.join(root_folder, file)
                        element = get_data(str(full_path), root, str_year)
                        element = element.loc[element["TS"] >= start_time]
                        element = element.loc[element["TS"] <= end_time]
                        numeric_columns = element.select_dtypes(
                            include=["int64", "float64"]
                        )
                        for col in numeric_columns:
                            element = element.loc[
                                element[col] != 9.9900000000000004e125
                            ]
                            element = element.loc[element[col] != 1.2237598e7]
                            element = element.loc[element[col] != 7.64849875e5]
                            if element.loc[:, col].mean() != 0 and not np.isnan(  # type: ignore
                                element.loc[:, col].mean()
                            ):
                                element = (
                                    element.drop_duplicates("TS")
                                    .set_index("TS")
                                    .resample("3s")
                                    .ffill()
                                    .reset_index()
                                )
                                file_list.append(element)
                                print(
                                    "File successfully formatted and inserted into the list"
                                )
                            else:
                                print("Too many B4BB1E. File will not be processed")
                    except EmptyDataError:
                        print(f"No columns to parse from file {str(file)}")
                        print("File will not be processed")
                    # except FileNotFoundError:
                    #    print(f"still searching for {str(file)}")
    return file_list


def merge_dataset(config_dict, input_file):
    """
    Merge the CSV data of each TELL40 into one DataFrame with a unique timestamp, which will be set as the index.

    Args:
        config_dict (dict): Configuration dictionary.
        input_file (str): Type of input file (e.g., "bb" or "bb_inner").

    Returns:
        pd.DataFrame: Merged DataFrame.
    """

    # define an empty list where to store all the csv counters
    df_list = []
    # get sigma vis data
    df_sigma_vis = pd.read_csv("../../../" + config_dict["folder_name"] + "/output_data/sigma_vis.csv")
    df_sigma_vis.set_index("colName",inplace=True)
    calib_colname = list(df_sigma_vis.index)
    # get list of counters from configuration file
    for counter in config_dict["counters_name"]:
        
        # create a list with the bxtype counters for a module (in counters list you find bb, eb, be ee for a single counter)
        counters_list = create_file_list(config_dict, input_file, counter)
        # if the list is empty (no data for counters were found) skip loop
        if not counters_list:
            continue

        # merge the different type of counters in a unique timestamp
        df_merged_bb = reduce(
            lambda left, right: pd.merge_asof(
                left,
                right,
                on="TS",
                direction="nearest",
                tolerance=pd.Timedelta("3s"),
            ),
            counters_list,
        )
        # define a list with the existing counters
        var_list = [col for col in df_merged_bb.columns if col.startswith("M")]
        
        for el in var_list:
            if input_file == "bb_inner":
                inner_calib_colname = [col for col in calib_colname if col.endswith("inner")]
                inner_calib_colname = [x.replace('_inner', '') for x in inner_calib_colname]
                if el[:-2] not in inner_calib_colname:
                    continue
                col_name = f"{el[:-2]}_inner"
                print(f"Processing {str(col_name)} counters")
                
                # subtract background
                df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                    - df_merged_bb[el[:-2] + "be"]
                    - 16 * df_merged_bb[el[:-2] + "ee"]
                )
                # define error
                new_col_name = f"Delta_{el[:-2]}_inner"
                # function calculated with monte carlo
                df_merged_bb[new_col_name] = 0.000709511 * np.sqrt(
                    0.955167
                    * (
                        df_merged_bb[el[:-2] + "bb"]
                        + df_merged_bb[el[:-2] + "be"]
                        + 16 * df_merged_bb[el[:-2] + "ee"]
                    )
                )
                # define lumi
                lumi_col_name = f"lumi_{col_name}"
                df_merged_bb[lumi_col_name] = (
                    df_merged_bb[col_name]
                    * config_dict["nCollisions"]
                    * config_dict["lhc_frequency"]
                    / df_sigma_vis.loc[col_name]["calibration"]
                )
                # define error lumi
                lumi_err_col_name = f"lumi_err_{col_name}"
                df_merged_bb[lumi_err_col_name] = (
                    np.sqrt(
                        (
                            df_merged_bb[new_col_name]
                            / df_sigma_vis.loc[col_name]["calibration"]
                        )
                        ** 2
                        + (
                            df_merged_bb[col_name]
                            * df_sigma_vis.loc[col_name]["error_calibration"]
                            / (df_sigma_vis.loc[col_name]["calibration"] ** 2)
                        )
                        ** 2
                    )
                    * config_dict["nCollisions"]
                    * config_dict["lhc_frequency"]
                )

                
            else:
                inner_calib_colname = [col for col in calib_colname if col.endswith("outer")]
                inner_calib_colname = [x.replace('_outer', '') for x in inner_calib_colname]
                if el[:-2] not in inner_calib_colname:
                    continue
                col_name = f"{el[:-2]}_outer"
                print(f"Processing {str(col_name)} counters")
                # subtract background
                df_merged_bb[col_name] = (
                    df_merged_bb[el[:-2] + "bb"]
                    - df_merged_bb[el[:-2] + "be"]
                    - df_merged_bb[el[:-2] + "eb"]
                    + df_merged_bb[el[:-2] + "ee"]
                )
                # define error
                new_col_name = f"Delta_{el[:-2]}_outer"
                df_merged_bb[new_col_name] = 0.000709511 * np.sqrt(
                    0.955167
                    * (
                        df_merged_bb[el[:-2] + "bb"]
                        + df_merged_bb[el[:-2] + "be"]
                        + df_merged_bb[el[:-2] + "eb"]
                        + df_merged_bb[el[:-2] + "ee"]
                    )
                )# define lumi
                lumi_col_name = f"lumi_{col_name}"
                df_merged_bb[lumi_col_name] = (
                    df_merged_bb[col_name]
                    * config_dict["nCollisions"]
                    * config_dict["lhc_frequency"]
                    / df_sigma_vis.loc[col_name]["calibration"]
                )
                # define error lumi
                lumi_err_col_name = f"lumi_err_{col_name}"
                df_merged_bb[lumi_err_col_name] = (
                    np.sqrt(
                        (
                            df_merged_bb[new_col_name]
                            / df_sigma_vis.loc[col_name]["calibration"]
                        )
                        ** 2
                        + (
                            df_merged_bb[col_name]
                            * df_sigma_vis.loc[col_name]["error_calibration"]
                            / (df_sigma_vis.loc[col_name]["calibration"] ** 2)
                        )
                        ** 2
                    )
                    * config_dict["nCollisions"]
                    * config_dict["lhc_frequency"]
                )


        # define columns to remove: every type counters
        col_to_remove = [
            col
            for col in df_merged_bb.columns
            if (
                col.endswith("bb")
                or col.endswith("be")
                or col.endswith("eb")
                or col.endswith("ee")
            )
        ]
        # drop the columns defined above
        df_merged_bb.drop(columns=col_to_remove, inplace=True)

        df_list.append(df_merged_bb)

        # df_merged_bb.to_csv("merged_counters/merged_" + counter + ".csv")
    # merge all counters in a single csv
    df_merged_unique = reduce(
        lambda left, right: pd.merge_asof(
            left,
            right,
            on="TS",
            direction="nearest",
            tolerance=pd.Timedelta("3s"),
        ),
        df_list,
    )
    df_merged_unique.set_index("TS", inplace=True)
    return df_merged_unique


def plot_figure(counters, mean, stdev,fill):
    """
    Create a figure to display counters.

    Args:
        df (pd.DataFrame): DataFrame with counters data.
        year (str): Year of vdm (e.g., "2022" or "2023").
    """

    _, axes = plt.subplots(nrows=3, ncols=1, sharex=True, figsize=(15, 10))

    counters.plot(legend=False,
                              linestyle='',
                              marker='.',
                              markersize=1,
                              title=f"Luminosity for Fill {fill}",
                              ylabel="Luminosity counters [Hz/$\mu barn$]",
                              ax=axes[0]
                             )

    mean.plot(
            marker=".",
            markersize=1,

            xlabel="Time Stamp (D H:m)",
            ylabel="Lumi estimation (trim) [Hz/$\mu barn$]",
            ax=axes[1],
        )


    stdev[stdev<0.1].plot(
        linestyle='',
        marker='.',
        markersize=1,
        xlabel="Time Stamp (D H:m)",
        ylabel="Relative Std dev",
        ax=axes[2],
    )
    #ax0.text(dt.datetime(2023, 9, 20,2,0), 5, '99 counters (VELO C only)', fontsize = 10) 

    plt.show()

def plot_hist(df,timestamp='2023-09-20 03:33:00'):
    var=timestamp
    f, ax = plt.subplots()
    plt.hist(df.T[var], bins=20, edgecolor='k')

    # Calcola le statistiche
    mean_value = df.T[var].mean()
    std_deviation = df.T[var].std()
    min_value = df.T[var].min()
    max_value = df.T[var].max()
    num_entries = len(df.T)

    # Aggiungi le statistiche come testo sulla canvas dell'istogramma
    plt.text(1., 0.8, f'Mean: {mean_value:.5f}', fontsize=12, horizontalalignment='right',
         verticalalignment='center',
         transform = ax.transAxes)
    plt.text(1., 0.75, f'StDev: {std_deviation:.5f}', fontsize=12, horizontalalignment='right',
         verticalalignment='center',
         transform = ax.transAxes)
    #plt.text(.00045, 6, f'Min: {min_value:.2f}', fontsize=1, color='red')
    #plt.text(.00045, 50, f'Max: {max_value:.2f}', fontsize=1, color='red')
    plt.text(1., 0.7, f'Entries: {num_entries}', fontsize=12, horizontalalignment='right',
         verticalalignment='center',
         transform = ax.transAxes)

    plt.title('Lumi measurement of all working counters at ' + var)
    plt.xlabel('Estimated lumi [Hz/$\mu$barn]')
    plt.ylabel('Counts')

    #plt.show()

def main():
    """
    Main body of the script.
    """
    # open and read configuration file
    stream = open("config.yaml", "r", encoding="utf-8")
    config_dictionary = yaml.safe_load(stream)
    # set options according to configuration file
    display_fig = config_dictionary["displayFig"]
    # input_file = "bb"
    folder_name = config_dictionary["folder_name"]

    # if the dataset is not already create, merge all the counters in a csv file
    if not exists("../" + folder_name + "/input_data/data_merged.csv"):
        df_merged_bb = merge_dataset(config_dictionary, "bb")
        df_merged_bb_inner = merge_dataset(config_dictionary, "bb_inner")
        df_merged_unique = pd.merge_asof(
            df_merged_bb,
            df_merged_bb_inner,
            left_index=True,
            right_index=True,
            direction="nearest",
            tolerance=pd.Timedelta("3s"),
        )
        mask = (df_merged_unique.index > config_dictionary["StartOfAnalysis"]) & (
            df_merged_unique.index <= config_dictionary["EndOfAnalysis"]
        )
        df_merged_unique = df_merged_unique.loc[mask]

        df_merged_unique.to_csv("../" + folder_name + "/input_data/data_merged.csv")
    else:
        df_merged_unique = pd.read_csv(
            "../" + folder_name + "/input_data/data_merged.csv"
        )
        df_merged_unique["TS"] = pd.to_datetime(
            df_merged_unique["TS"], format="%Y-%m-%d %H:%M:%S"
        )
        df_merged_unique = df_merged_unique.set_index("TS")

    selected_columns = df_merged_unique.filter(regex='^lumi_M')
    cleaned_9168 = selected_columns[selected_columns>=0]
    std_per_timestamp = cleaned_9168.std(axis=1)

    # Identifica e rimuovi gli outliers (valori al di fuori di 5 deviazioni standard dal valore medio)
    mean_per_timestamp = cleaned_9168.mean(axis=1)
    threshold =  5*std_per_timestamp
    outliers = (cleaned_9168.T - mean_per_timestamp).abs() > threshold
    cleaned_df = cleaned_9168.T.mask(outliers)
    # Calcola la deviazione standard dei dati "puliti" per ciascun intervallo di tempo
    std_per_timestamp_cleaned = cleaned_df.std(axis=0)
    cleaned_9168_wo_outliers = cleaned_df.T
    fill_9168_wo_0=cleaned_9168_wo_outliers.loc[:, (cleaned_9168_wo_outliers**2).sum() != 0]*1000
    std_per_timestamp_cleaned = fill_9168_wo_0.std(axis=1)
    np_array = fill_9168_wo_0.T.to_numpy()
    truncated_means = trim_mean(np_array, 0.3)
    truncated_means_cleaned = pd.Series(truncated_means, index=std_per_timestamp_cleaned.index)
    rel_std_cleaned = std_per_timestamp_cleaned/truncated_means_cleaned

    if display_fig:
        plot_figure(fill_9168_wo_0,truncated_means_cleaned, rel_std_cleaned, config_dictionary["Fill"])
        plot_hist(fill_9168_wo_0)
    
    
#

if __name__ == "__main__":
    main()

import pandas as pd

import matplotlib.pyplot as plt
import yaml
import mplhep as hep

# Load the mplhep style
hep.style.use("LHCb2")

# Load the configuration file
stream = open("config.yaml", "r", encoding="utf-8")
config_dictionary = yaml.safe_load(stream)
#set options according to configuration file
display_fig = config_dictionary["displayFig"]
calib_year = config_dictionary["calib_year"]
folder_name = config_dictionary["folder_name"]
StartOfStableTime = config_dictionary["StartOfStableTime"]
EndOfStableTime = config_dictionary["EndOfStableTime"]


# Load the luminosity data
lumi_df = pd.read_csv("../../../data_pvss/" + folder_name + "/output_data/lumi_new.csv")
#set the timestamp as index
lumi_df['TS'] = pd.to_datetime(lumi_df['TS'])
lumi_df.set_index('TS', inplace=True)

# filter dataframe according to the stable time
lumi_df = lumi_df.loc[StartOfStableTime:EndOfStableTime]

# take all the data in the first entry of the stable time and make an histogram
#plt.figure(figsize=(10, 6))
#lumi_df.iloc[0].hist(bins=50)
#plt.title('Histogram of All Values in First Entry of Stable Time')
#plt.xlabel('Values')
#plt.ylabel('Frequency')
#plt.grid(False)
#print mean and standard deviation on the canvas
#plt.text(0.9, 0.9, 'Mean: {:.2f}'.format(lumi_df.iloc[0].mean()), ha='center', va='center', transform=plt.gca().transAxes)
#plt.text(0.9, 0.85, 'Standard Deviation: {:.2f}'.format(lumi_df.iloc[0].std()), ha='center', va='center', transform=plt.gca().transAxes)
#plt.show()


# Load the trimmed luminosity data without offset
lumi_trim_wo_offset_df = pd.read_csv("../../../data_pvss/" + folder_name + "/output_data/lumi_trim_wo_offset.csv")
lumi_trim_wo_offset_df['TS'] = pd.to_datetime(lumi_trim_wo_offset_df['TS'])
lumi_trim_wo_offset_df.set_index('TS', inplace=True)
lumi_trim_wo_offset_df = lumi_trim_wo_offset_df.loc[StartOfStableTime:EndOfStableTime]

# Display histogram of all values in the trimmed dataframe
#plt.figure(figsize=(10, 12))
lumi_trim_wo_offset_df.hist(bins=20, histtype='stepfilled', lw=3, ec='green', color='green',figsize=(9, 7.47), alpha=0.3)
plt.title('')
plt.xlabel('Luminosity (trimmed mean) [Hz/µbarn]')
plt.ylabel('Counts')
plt.grid(False)
#set linewi
#print mean and standard deviation on the canvas
plt.text(0.25, 0.87, '$\mu=${:.3f}'.format(lumi_trim_wo_offset_df.mean().mean()), ha='center', va='center', transform=plt.gca().transAxes, fontsize=32)
plt.text(0.25, 0.73, '$\sigma=$ {:.3f}'.format(lumi_trim_wo_offset_df.std().mean()), ha='center', va='center', transform=plt.gca().transAxes, fontsize=32)
#plt.text(0.15, 0.74, 'N= {}'.format(lumi_trim_wo_offset_df.count().sum()), ha='center', va='center', transform=plt.gca().transAxes, fontsize=32)
plt.tight_layout()
plt.savefig("../" + config_dictionary['folder_figure'] + "/histo_trim-2.pdf")
plt.show()
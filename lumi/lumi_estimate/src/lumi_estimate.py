"""
Script for merging data from DPS of TEll40

VADAQTELL40:Velo_TELL40_Mij_*_k.luminosity_rates.inst_lumi_bb

The script is called via

python3 merge_data.py

"""
#c
import os
from os.path import exists
import sys
from functools import reduce
from pathlib import Path
import numpy as np
import pandas as pd
from pandas.errors import EmptyDataError
import matplotlib.pyplot as plt
import yaml
import seaborn as sns
from scipy.stats import trim_mean
import matplotlib.dates as mdates
import mplhep as hep



def plot_figure(df, mean,df_postion,config_dictionary):
    hep.style.use("LHCb2")
    fig, axes = plt.subplots(nrows=3, ncols=1, sharex=True, figsize=(23, 11.5))
    #plt.figure(figsize=(15, 10))
    df_x1_position = df_postion["Nominal_Displacement_B1_xingPlane"]
    df_x2_position = df_postion["Nominal_Displacement_B2_xingPlane"]
    df_y1_position = df_postion["Nominal_Displacement_B1_sepPlane"]
    df_y2_position = df_postion["Nominal_Displacement_B2_sepPlane"]
    title_tmp = ""
    if config_dictionary["VdM"]:
        title_tmp = "Van der Meer scan - "
    date = config_dictionary["StartOfAnalysis"]
    str_date = f"{date.day}/{date.month}/{date.year}"
    
    # Sample points in ax0
    sample_rate = 4  # Adjust this value to control the sampling rate
    df_sampled = df.iloc[::sample_rate, :]
    
    # Select alternating columns (one every 5 columns)
    df_sampled = df_sampled.iloc[:, ::3]
    
    ax0 = df_sampled.plot(legend=False,
                          linestyle='',
                          marker='.',
                          markersize=3,
                          #linewidth=1,
                          #title=f"{title_tmp} Fill {config_dictionary['Fill']} - {str_date}",
                          ylabel=""+r"$\mu_{vis}$",
                          ax=axes[0]
                         )
    
    ax1 = mean.plot(
        marker=".",
        markersize=3,
        linewidth=3,
        xlabel="Time (HH:mm)",
        ylabel="Luminosity\n"+r"[Hz/µbarn]",
        color="red",
        ax=axes[1],
    )
    ax2 = df_x1_position.plot(
        ax=axes[2],
        marker=".",
        markersize=3,
        linewidth=3,
        linestyle="solid",
        #ylabel=r"$\it{x}$ position"+"\n [mm]",
        color="tab:blue",
        label='x B1'
    )

    ax4 = df_x2_position.plot(
        ax=axes[2],
        marker=".",
        markersize=3,
        linewidth=3,
        linestyle="solid",
        #ylabel=r"$\it{x}$ position"+"\n [mm]",
        color="tab:orange",
        label='x B2'
    )

    ax3 = df_y1_position.plot(
        ax=axes[2],
        marker=".",
        markersize=1,
        linewidth=3,
        linestyle="dashed",
        xlabel="Time (HH:mm)",
        color="tab:green",
        label='y B1'
    )

    ax5 = df_y2_position.plot(
        ax=axes[2],
        marker=".",
        markersize=1,
        linewidth=3,
        linestyle="dashed",
        xlabel="Time (HH:mm)",
        ylabel="Set position of\n beams [mm]",
        color="tab:brown",
        label='y B2'
    )

    # Align y-labels
    for ax in axes:
        ax.yaxis.set_label_coords(-0.06, .9)
    ax0.yaxis.set_label_coords(-0.06, 0.5)
    #hep.lhcb.text("Preliminary",loc=0,ax=axes[0])
    #plt.tight_layout()
    #ax0.legend(prop={"size": 10}, ncols=10)
    
    ax2.legend(prop={"size": 18})
    ax3.legend(prop={"size": 18})
    ax4.legend(prop={"size": 18})
    ax5.legend(prop={"size": 18})

    #ax0.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    #ax1.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    #ax2.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    plt.savefig("../" + config_dictionary['folder_figure'] + "/trace_plot_compressed.pdf")

    '''
    ax2 = rel_std[rel_std<0.1].plot(
    linestyle='',
    marker='.',
    markersize=1,
    xlabel="Time Stamp (D H:m)",
    ylabel="Relative Std dev",
    ax=axes[2],
    )
    '''
    plt.show()

def remove_outliers(df_lumi, threshold=5):
    """
    Remove outliers from the dataframe.

    Args:
    df (pd.DataFrame): Dataframe to remove outliers from.
    threshold (int): Threshold for removing outliers.

    Returns:
    pd.DataFrame: Dataframe without outliers.
    """

    df_only_lumi = df_lumi.filter(regex='^lumi_M')
    cleaned_lumi = df_only_lumi[df_only_lumi>=-10]
    std_per_timestamp = cleaned_lumi.std(axis=1)

    # Identifica e rimuovi gli outliers (valori al di fuori di 5 deviazioni standard dal valore medio)
    mean_per_timestamp = cleaned_lumi.mean(axis=1)
    threshold =  5*std_per_timestamp
    outliers = (cleaned_lumi.T - mean_per_timestamp).abs() > threshold
    cleaned_df = cleaned_lumi.T.mask(outliers)
    # Calcola la deviazione standard dei dati "puliti" per ciascun intervallo di tempo

    cleaned_lumi_wo_outliers = cleaned_df.T
    fill_wo_0=cleaned_lumi.loc[:, (cleaned_lumi**2).sum() != 0]#*6.4000/config_dictionary["nCollisions"]

    #std_per_timestamp_cleaned = fill_wo_0.std(axis=1)
    #mean_per_timestamp_cleaned = fill_wo_0.mean(axis=1)
    median_per_timestamp_cleaned = fill_wo_0.median(axis=1)
    np_array = fill_wo_0.T.to_numpy()
    truncated_means = trim_mean(np_array, 0.15)
    # Calcola i percentili 15 e 85 per ogni colonna
    percentile_15 = fill_wo_0.T.quantile(0.15)
    percentile_85 = fill_wo_0.T.quantile(0.85)
    # Seleziona solo i valori compresi tra i percentili 15 e 85 per ogni colonna
    df_trimmed = fill_wo_0.T.apply(lambda col: col[(col >= percentile_15[col.name]) & (col <= percentile_85[col.name])])

    # Calcola la deviazione standard su questo nuovo set di dati
    std_dev_trimmed = df_trimmed.std()
    #truncated_std = trimmed_std(np_array, 0.3)
    truncated_means_cleaned = pd.Series(truncated_means, index=median_per_timestamp_cleaned.index)
    #truncated_std_cleaned = pd.Series(truncated_std, index=median_per_timestamp_cleaned.index)
    return truncated_means_cleaned, std_dev_trimmed



def main():
    """
    Main body of the script.
    """
    #open and read configuration file. It takes informations such as the year of the calibration, 
    #the start and end of the calibration, the display of the figure, the counters to be used,
    #the bunch population, the frequency of lhc, the number of colliding bunches
    stream = open("config.yaml", "r", encoding="utf-8")
    config_dictionary = yaml.safe_load(stream)
    #set options according to configuration file
    display_fig = config_dictionary["displayFig"]
    calib_year = config_dictionary["calib_year"]
    folder_name = config_dictionary["folder_name"]
    magnitude = config_dictionary["magnitude"]
    #get year of calibration from config file, search for data in the right folder
    smog = config_dictionary["smog"]
    #get sigma vis from configuration file
    df_sigma_vis = pd.read_csv("../../../data_pvss/" + folder_name + "/output_data/sigma_vis"+smog+".csv")
    df_sigma_vis.set_index("colName",inplace=True)
    df_sigma_vis.dropna(inplace=True)
    #new_df = df_sigma_vis
    #new_df = new_df[new_df["calibration"]>0]
    #new_df = new_df[new_df["calibration"]<10]
    #new_df = new_df[new_df["error_calibration"]<0.2]
    #df_sigma_vis = new_df
    calib_colname = list(df_sigma_vis.index)
    #if the dataset is already created, load it
    df_merged_unique = pd.read_csv(
            "../../../data_pvss/" + folder_name + "/input_data/data_merged.csv"#"/output_data/df_counters_wo_offset.csv"
        )
    df_merged_unique["TS"] = pd.to_datetime(
            df_merged_unique["TS"], format="%Y-%m-%d %H:%M:%S"
            )
    df_merged_unique = df_merged_unique.set_index("TS")
    mask = (df_merged_unique.index> config_dictionary["StartOfAnalysis"]) & (df_merged_unique.index <= config_dictionary["EndOfAnalysis"])
    df_merged_unique = df_merged_unique.loc[mask]

    #countertype = ["outer", "inner"]
    var_list = [col for col in df_merged_unique.columns if col.startswith("M")]
    list_to_loop = list(set(var_list) & set(calib_colname))
    #print(list_to_loop)
    #initialize df_lumi
    df_lumi = pd.DataFrame()
    df_lumi_A = pd.DataFrame()
    df_lumi_C = pd.DataFrame()
    lumi_data = {}
    lumi_data_A = {}
    lumi_data_C = {}
    for el in list_to_loop:
        #compute only for outer counters. skip elements that have "_inner" in the name
        #if "inner" in el:
        #    continue
        #print(el)
        lumi_data['lumi_'+el] = df_merged_unique[el]*config_dictionary['nCollisions']*config_dictionary['lhc_frequency']*magnitude/df_sigma_vis.loc[el]["calibration"]
        #seleziona il numero in el
        n = int(el[1:3])
        #print(n)
        if n%2 == 0:
            lumi_data_A['lumi_'+el] = df_merged_unique[el]*config_dictionary['nCollisions']*config_dictionary['lhc_frequency']*magnitude/df_sigma_vis.loc[el]["calibration"]
        else:
            lumi_data_C['lumi_'+el] = df_merged_unique[el]*config_dictionary['nCollisions']*config_dictionary['lhc_frequency']*magnitude/df_sigma_vis.loc[el]["calibration"]
        '''
        lumi_err_col_name = f"err_lumi_{el}"
        df_lumi[lumi_err_col_name] = (
                    np.sqrt(
                        (
                            df_merged_unique['Delta_'+el]
                            / df_sigma_vis.loc[el]["calibration"]
                        )
                        ** 2
                        + (
                            df_merged_unique[el]
                            * df_sigma_vis.loc[el]["error_calibration"]
                            / (df_sigma_vis.loc[el]["calibration"] ** 2)
                        )
                        ** 2
                    )
                    * config_dictionary["nCollisions"]
                    * config_dictionary["lhc_frequency"]
                )
        '''
    
    df_lumi = pd.concat(lumi_data, axis=1)
    df_lumi_A = pd.concat(lumi_data_A, axis=1)
    df_lumi_C = pd.concat(lumi_data_C, axis=1)
    df_lumi.to_csv("../../../data_pvss/" + folder_name + "/output_data/lumi_new.csv")
    truncated_means_cleaned, std_dev_trimmed = remove_outliers(df_lumi,threshold=5)
    truncated_means_cleanedA, std_dev_trimmedA = remove_outliers(df_lumi_A,threshold=5)
    truncated_means_cleanedC, std_dev_trimmedC = remove_outliers(df_lumi_C,threshold=5)
    rel_std_cleaned = std_dev_trimmed/truncated_means_cleaned
    df_counters = df_merged_unique.filter(regex='^M')
    df_position = pd.DataFrame(index=df_merged_unique.index)
    #print(df_merged_unique)
    df_position["Nominal_Displacement_B1_sepPlane"]=df_merged_unique["Nominal_Displacement_B1_sepPlane"]
    df_position["Nominal_Displacement_B2_sepPlane"]=df_merged_unique["Nominal_Displacement_B2_sepPlane"]
    df_position["Nominal_Displacement_B1_xingPlane"]=df_merged_unique["Nominal_Displacement_B1_xingPlane"]
    df_position["Nominal_Displacement_B2_xingPlane"]=df_merged_unique["Nominal_Displacement_B2_xingPlane"]

    #plot the figure with the counters and the position of the beam
    df_general = pd.concat([df_counters,df_position],axis=1)
    df_general['lumi'] = truncated_means_cleaned
    #df_general.to_csv("../../../data_pvss/" + folder_name + "/output_data/df_general.csv")
    if display_fig:
        plot_figure(df_counters, truncated_means_cleaned,df_position,config_dictionary) #df_trimmed.T
#   
    truncated_means_cleaned.to_csv("../../../data_pvss/" + folder_name + "/output_data/lumi_trim_wo_offset.csv")
    #truncated_means_cleanedA.to_csv("../../../data_pvss/" + folder_name + "/output_data/lumi_trim_wo_offsetA.csv")
    #truncated_means_cleanedC.to_csv("../../../data_pvss/" + folder_name + "/output_data/lumi_trim_wo_offsetC.csv")

if __name__ == "__main__":
    main()
